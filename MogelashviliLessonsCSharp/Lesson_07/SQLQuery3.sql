USE MogelashviliLessonsCSharp
SELECT
	me.Amount, me.EntryDate, me.Description, cat.Name
FROM
	MoneyEntries me
	LEFT JOIN Categories cat
	ON me.Category = cat.Id
WHERE cat.Id = 1
	AND me.Description <> ''