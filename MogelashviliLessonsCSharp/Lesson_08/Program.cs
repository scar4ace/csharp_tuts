﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;//библиотека для работы с SQL

namespace Lesson_08
{
    class Program
    {
        static void Main(string[] args)
        {
            //инициализация соединения
            SqlConnection sqlConn = new SqlConnection(@"Data Source=DNKPLAYTIKA55\SQLEXPRESS;Initial Catalog=MogelashviliLessonsCSharp;Integrated Security=True");
            //создание запроса
            SqlCommand sqlCmd = sqlConn.CreateCommand();
            //формирование запроса
            sqlCmd.CommandText = @"SELECT
                                    me.Id, me.Amount, me.EntryDate, me.Description, cat.Name
                                   FROM 
                                     MoneyEntries me
                                   LEFT JOIN Categories cat
                                   ON cat.Id = me.Category";
            //открыть соединение с базой
            sqlConn.Open();
            //инициализация чтения данных на основе сформированного запроса
            using (SqlDataReader reader = sqlCmd.ExecuteReader())
            {
                //чтение строк из таблицы
                while(reader.Read())
                {
                    Console.WriteLine(string.Format("{0}: {1} from {2}; {3}; {4}",
                        reader.GetInt32(0).ToString(),
                        reader.GetFloat(1).ToString(),
                        reader.GetDateTime(2).ToShortDateString(),
                        //проверка содержимого ячейки на DBNull
                        reader.IsDBNull(4) ? "<no category>" : reader.GetString(4),//условие ? return if it's true : return if left is false
                        reader.IsDBNull(3) ? "<no description>" : reader.GetString(3)
                        ));
                }
            }
            //закрыть соединение с базой
            sqlConn.Close();

            Console.ReadKey();
        }
    }
}
