﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_03
{
    // Абстрактный класс
    public abstract class Car
    {
        protected int _speed;
        protected string _type;

        public Car()
        {
            _speed = 0;
            _type = "noname";
        }
        // Переопределение стандартного метода
        public override string ToString()
        {
            return string.Format("Model: {0}; Speed: {1}", _type, _speed);
        }
        // Абстрактный метод
        public abstract void Drive();
    }

    public class Vaz:Car
    {
        public Vaz()
            :base()
        {
            _type = "VAZ 2107";
        }
        // Переопределение метода
        public override void Drive()
        {
            _speed = 60;
        }

    }
    public class Lamborgini : Car, ITurbo
    {
        public Lamborgini()
            :base()
        {
            _type = "Gallardo";
        }
        // Переопределение метода
        public override void Drive()
        {
            Turbo();
            _speed = 260;
        }
        // Реализация методов интерфейса
        public void Turbo()
        {

        }
    }
    public class Ferrari : Car, IExtendedEngine
    {
        public Ferrari()
            :base()
        {
            _type = "LaFerrari";
        }
        public override void Drive()
        {
            ExtendedEngine();
            _speed = 240;
        }
        // Реализация методов интерфейса
        public void ExtendedEngine()
        {

        }
    }
    // Интерфейсы
    interface ITurbo
    {
        void Turbo();
    }
    interface IExtendedEngine
    {
        void ExtendedEngine();
    }
}
