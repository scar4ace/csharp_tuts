﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_03
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter car type");
            Console.WriteLine("Vaz - to select Vaz 2107");
            Console.WriteLine("Lambo - to select Lamborgini");
            Console.WriteLine("Ferrari - to select LaFerrari");

            string type = Console.ReadLine();

            Car chosenCar = GetCarByType(type);

            Console.WriteLine();
            Console.WriteLine("You choosed:");
            Console.WriteLine(chosenCar.ToString());

            Console.WriteLine("GO!");
            chosenCar.Drive();
            Console.WriteLine(chosenCar.ToString());

            // Проверка на соответствие типу
            if(chosenCar is ITurbo)
            {
                Console.WriteLine("Your car is turbochared!");

                // Приведение к типу/интерейсу, дающее доступ к методам реализованного интерфейса
                ((Lamborgini)chosenCar).Turbo();
            }

            Console.ReadKey();
        }
        static Car GetCarByType(string type)
        {
            switch(type)
            {
                case "Vaz":
                    return new Vaz();
                case "Lambo":
                    return new Lamborgini();
                case "Ferrari":
                    return new Ferrari();
                default:
                    return null;
            }
        }
    }
}