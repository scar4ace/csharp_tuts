﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CSMoney;

namespace Lesson_06
{
    public partial class Form1 : Form
    {
        //private double _balance;
        public Form1()
        {
            InitializeComponent();
            //_balance = 0;
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            MoneyEntry me = new MoneyEntry();

            double income;
            double.TryParse(textBoxAmount.Text, out income);

            me.Amount = income;
            me.EntryDate = dtpDate.Value;
            me.Description = textBoxDescription.Text;
            me.Category = textBoxCategory.Text;

            AddEntry(me);
            UpdateBalance();
            ClearFields();
        }
        private void UpdateBalance()
        {
            double balance = 0;
            foreach(DataGridViewRow row in dataGridViewEntries.Rows)
            {
                double income;
                double.TryParse((row.Cells[1].Value ?? "0").ToString(), out income);
                balance += income;
            }
            textBoxBalance.Text = balance.ToString();
        }
        private void AddEntry(MoneyEntry me)
        {
            dataGridViewEntries.Rows.Add(me.IsDebit ? "Доход" : "Расход",//условный оператор ?: (condition ? first_expression : second_expression;)
                me.Amount,
                me.EntryDate,
                me.Description,
                me.Category
                );
        }
        private void ClearFields()
        {
            dtpDate.Value = DateTime.Now;
            textBoxAmount.Text = "";
            textBoxDescription.Text = "";
            textBoxCategory.Text = "";
        }
        private void textBoxAmount_TextChanged(object sender, EventArgs e)
        {

        }

        private void dataGridViewEntries_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if(e.ColumnIndex==1&&dataGridViewEntries.Rows.Count>0)
            {
                double income;
                double.TryParse((dataGridViewEntries[e.ColumnIndex,e.RowIndex].Value ?? "0").ToString(), out income);//условный оператор ?? (value1 ?? value2 если value1!=null возвращать value1, в противном случае value2)
                
                dataGridViewEntries[0, e.RowIndex].Value = (income > 0) ? "Доход" : "Расход";
                UpdateBalance();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            MessageBox.Show("Привет! Начни учет своих финансов!");
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            MessageBox.Show(string.Format("Ваш баланс: {0}", textBoxBalance.Text));
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }
    }
}
