﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSMoney
{
    public class MoneyEntry
    {
        /// <summary>
        /// Fields
        /// </summary>
        private double _amount;
        /// <summary>
        /// Constructors
        /// </summary>
        public MoneyEntry()
        {
            _amount = 0;
        }
        public MoneyEntry(double amount, DateTime date)
        {
            _amount = amount;
            EntryDate = date;
        }
        /// <summary>
        /// Methods
        /// </summary>
        /// <param name="amount">Amount of money</param>
        /// <param name="date">Date of entry</param>
        public void InitWithString(string amount, string date)
        {
            // Type coversion
            Double.TryParse(amount, out _amount);

            DateTime dt;
            // Type coversion
            DateTime.TryParse(date, out dt);
            EntryDate = dt;
        }
        // Override
        public override string ToString()
        {
            return string.Format("{0} from {1}", _amount, EntryDate.Date);
        }
        // Свойства
        public bool IsDebit
        {
            get { return _amount >= 0; }
            set
            {
                if (value && _amount < 0)
                    _amount = -_amount;
            }
        }
        public double Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }
        // Автореализуемое свойство
        public DateTime EntryDate { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }

    }
}
