﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CSMogelashviliLesson_01
{
    class Program
    {
        static void Main(string[] args)
        {
            DirectoryInfo di = new DirectoryInfo("Data");
            if (di.Exists)
                di.Delete(true);
            di.Create();
            StreamWriter sw = File.CreateText("Data\\money.txt");

            Console.WriteLine("Hello!");
            Console.WriteLine();

            int debitSum = 0;
            for (int i = 0; i < 3; i++)
            {
                string debit = "";
                Console.Write("Input your debit: ");
                debit = Console.ReadLine();

                int debitInt = 0;
                int.TryParse(debit, out debitInt);
                debitSum += debitInt;
            }
            sw.WriteLine("Your debit: " + debitSum);
            sw.WriteLine();
            Console.WriteLine();

            int creditSum = 0;
            for (int i = 0; i < 3; i++)
            {
                string credit = "";
                Console.Write("input your credit: ");
                credit = Console.ReadLine();

                int creditInt = 0;
                int.TryParse(credit, out creditInt);
                creditSum += creditInt;
            }
            sw.WriteLine("Your credit: " + creditSum);
            sw.WriteLine();

            int balance = debitSum - creditSum;
            sw.WriteLine("Your balance: " + balance.ToString());

            sw.WriteLine();
            if (balance > 0)
            {
                sw.WriteLine("You have enough money");
            }
            else
            {
                sw.WriteLine("You need more money");
            }

            sw.Close();
            Console.WriteLine();
            Console.WriteLine("Press any key for closing this application");
            Console.ReadKey();
        }
    }
}
