﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Lesson_04
{
    class ContactManager
    {
        private List<ContactEntry> _entries;
        private string _filePath;
        private string _log;
        public ContactManager()
        {
            _entries = new List<ContactEntry>();
            _filePath = "";
        }
        public ContactManager(string filePath)
        {
            _entries = new List<ContactEntry>();

            ReadFromFile(filePath);

        }
        public bool ReadFromFile(string filePath)
        {
            _filePath = filePath;
            bool retVal = true;

            if (!File.Exists(filePath))
            {
                _log += string.Format("{0}\n", "File not found!");
                return false;
            }
            //чтение из файла
            StreamReader sr = File.OpenText(filePath);

            while(!sr.EndOfStream)
            {
                bool result = this.AddEntryFromFileLine(sr.ReadLine());
                if (!result)
                    retVal = false;
            }

            return retVal;
        }
        public void AddEntry(ContactEntry contact)
        {
            _entries.Add(contact);
        }
        //перегрузка метода AddEntry()
        public void AddEntry(ContactEntry contact, bool forceCommit)
        {
            AddEntry(contact);
            if(forceCommit)
            {
                Commit();
            }
        }

        public void Commit()
        {
            //создание файла и запись
            StreamWriter sw = File.CreateText(_filePath);

            foreach(ContactEntry ce in _entries)
            {
                sw.WriteLine(string.Format("{0}|{1}|{2}|{3}",
                    ce.LastName,
                    ce.FirstName,
                    ce.PhoneNumber,
                    ce.Email));               
            }
            //закрыть поток
            sw.Close();
        }
        public IEnumerable<ContactEntry>Entries
        {
            get { return _entries.AsReadOnly(); }
        }
        public string Log
        {
            get { return _log; }
        }
        public ContactEntry SearchForContact(string lastName,string firstName)
        {
            foreach (ContactEntry ce in _entries)
                if (ce.LastName == lastName && ce.FirstName == firstName)
                    return ce;
            return null;
        }
        public void RemoveEntry(ContactEntry contact)
        {
            _entries.Remove(contact);
        }
        private bool AddEntryFromFileLine(string line)
        {
            string[] fields = line.Split('|');
            //безопасный код
            try
            {
            ContactEntry ce = new ContactEntry();
            ce.LastName = fields[0];
            ce.FirstName = fields[1];
            ce.PhoneNumber = fields[2];
            ce.Email = fields[3];

            _entries.Add(ce);
            }
            //обработка исключения
            catch(IndexOutOfRangeException ex)
            {
                BusinessLogicException blex = new BusinessLogicException();
                blex.BusinessLogicMessage = "Index out of range";
                throw blex;
                //_log += string.Format("{0}\n",ex.Message);
                //return false;
            }
            //обязательный код
            //finally
            //{
            //}
            return true;
        }
    }
    //пользовательское исключение
    public class BusinessLogicException:Exception
    {
        public string BusinessLogicMessage { get; set; }
    }
}
