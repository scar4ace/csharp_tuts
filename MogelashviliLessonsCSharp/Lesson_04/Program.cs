﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_04
{
    class Program
    {
        private static string testString = "test string";
        static void Main(string[] args)
        {
            Console.Write("Enter file path: ");
            string filePath = Console.ReadLine();
            //string filePath = "contacts.txt";
            ContactManager cm = new ContactManager();
            try
            {
                cm.ReadFromFile(filePath);
            }
            catch(BusinessLogicException ex)
            {
                Console.WriteLine(ex.BusinessLogicMessage);
            }
            Console.WriteLine("\nEntries: ");

            foreach(ContactEntry ce in cm.Entries)
            {
                Console.WriteLine(ce.ToString());
            }
            Console.WriteLine();
            //лог на консоль
            Console.WriteLine(cm.Log);
            Console.ReadKey();
        }
    }
}
