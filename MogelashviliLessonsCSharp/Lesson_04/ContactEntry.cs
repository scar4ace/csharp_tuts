﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_04
{
    class ContactEntry
    {
        public ContactEntry()
        {
            FirstName = "---";
            LastName = "---";
            Email = "---";
            PhoneNumber = "---";
        }
        public ContactEntry(string firstName,string lastName,string email,string phoneNumber)
        {
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            PhoneNumber = phoneNumber;
        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }

        public override string ToString()
        {
            return string.Format("First Name: {0}, Last Name: {1}, Phone number: {2}, E-mail: {3}",
                FirstName,
                LastName,
                PhoneNumber,
                Email);
        }
    }
}
