﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CharsAndStrings
{
	public class RegExp
	{
		public RegExp()
		{
			string stringNormal = "Some characters have special meanings in a regular expression. The question mark, for example, says that the preceding expression (the character \"a\" in this case) may or may not be present.";
			string[] stringArr = { "Some", "characters", "have", "special", "meanings", "in", "a", "regular", "expression" };
			string stringWithoutSpaces = "\"The question mark,for example,says (that)the preceding expression. The question mark, for example.\"";

			//WordLength(stringNormal);
			//ReplaceSymbol(stringArr);
			//InsertSymbol(stringWithoutSpaces);
			//RemoveDuplicatedSymbols(stringNormal);
			//WordCount(stringNormal);
			//RemoveSubstring(stringNormal);
			//ReversString(stringNormal);
			//RemoveWord(stringNormal);
		}

		public void WordLength(string stringNormal)
		{
			string[] wordsArr = stringNormal.Split(new String[] { " ", ",", ".", "-", ")", "(", "\"", "/", "“", "”" }, StringSplitOptions.RemoveEmptyEntries);
			int shortestWordLength = wordsArr[0].Length;

			for (int i = 0; i < wordsArr.Length; i++)
			{
				if (i > 0 && wordsArr[i].Length > shortestWordLength)
				{
					shortestWordLength = wordsArr[i].Length;
				}
			}
			Console.Write(shortestWordLength);
		}

		public void ReplaceSymbol(string[] stringArr)
		{
			for (int i = 0; i < stringArr.Length; i++)
			{
				if (stringArr[i].Length == 10)
				{
					stringArr[i] = stringArr[i].Substring(0, 7) + "$";
				}
			}
		}

		public void InsertSymbol(string stringWithoutSpaces)
		{
			String pattern = "[0-9A-Za-z][.,!?:;%)\"][0-9A-Za-z]";
			Regex regex = new Regex(pattern);
			int i = 2;// start shift
			foreach (Match match in regex.Matches(stringWithoutSpaces))
			{
				stringWithoutSpaces = stringWithoutSpaces.Insert(match.Index + i, " ");
				i++;//shift index
			}
			Console.WriteLine(stringWithoutSpaces);
		}

		public void RemoveDuplicatedSymbols(string stringNormal)
		{
			HashSet<String> symbolsHashSet = new HashSet<String>();
			stringNormal = Regex.Replace(stringNormal, @"[\w\W]", m => symbolsHashSet.Add(m.Value) ? m.Value : String.Empty);
			Console.WriteLine(stringNormal);
		}

		public void WordCount(string stringNormal)
		{
			string[] wordsArr = stringNormal.Split(new String[] { " ", ",", ".", "-", ")", "(", "\"", "/", "“", "”" }, StringSplitOptions.RemoveEmptyEntries);
			Console.Write(wordsArr.Length);
		}

		public void RemoveSubstring(string stringNormal)
		{
			stringNormal = stringNormal.Remove(0, 5);
			Console.WriteLine(stringNormal);
		}

		public void ReversString(string stringNormal)
		{
			char[] charArr = stringNormal.ToCharArray();
			Array.Reverse(charArr);
			stringNormal = new String(charArr);
			Console.WriteLine(stringNormal);
		}

		public void RemoveWord(string stringNormal)
		{
			stringNormal = stringNormal.Remove(stringNormal.LastIndexOf(" "));
			Console.Write(stringNormal);
		}
	}
}
