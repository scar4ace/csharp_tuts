﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharsAndStrings
{
	class Chars
	{
		public Chars()
		{
			//Task1 A-Z to console
			for (int i = 'A'; i <= 'Z'; i++)
			{
				Console.Write((char)i);
			}

			//Task2 z-a to console
			for (int i = 'z'; i >= 'a'; i--)
			{
				Console.Write((char)i);
			}

			//Task3 а-я to console
			for (int i = 'а'; i <= 'я'; i++)
			{
				Console.Write((char)i/*+" "+i*/);
			}

			//Task4 0-9 to console
			for (int i = 0; i <= 9; i++)
			{
				Console.Write(i);
			}

			//Task5 CodeTable ASCII to console
			//Console.OutputEncoding = Encoding.ASCII;
			Console.OutputEncoding = Encoding.Unicode;
			for (int i = 0; i <= 255; i++)
			{
				Console.WriteLine((char)i);
			}
		}
	}
}
