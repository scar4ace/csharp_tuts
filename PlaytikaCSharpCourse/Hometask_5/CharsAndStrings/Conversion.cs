﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharsAndStrings
{
	class Conversion
	{
		public Conversion()
		{
			int numInt = 123;
			double numDouble = 1.23;
			string numStringInt = "123";
			string numStringDouble = "1.23";

			IntToString(numInt);
			DoubleToString(numDouble);
			StringToInt(numStringInt);
			StringToDouble(numStringDouble);
		}
		//Task 1 Int to String
		private void IntToString(int numInt)
		{
			string converted = numInt.ToString();
		}
		//Task 2 Double to String
		private void DoubleToString(double numDouble)
		{
			string converted = numDouble.ToString();
		}
		//Task 3 String to Int
		private void StringToInt(string numString)
		{
			int converted = Int32.Parse(numString);
		}
		//Task 4 String to Double
		private void StringToDouble(string numStringDouble)
		{
			double converted = Double.Parse(numStringDouble);
		}
	}
}
