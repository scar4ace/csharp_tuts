﻿using System;

namespace ArraysOneDimensional
{
	public class SumElementsWithOddIndexes
    {
        public SumElementsWithOddIndexes()
        {
            Task();
        }
        private void Task()
        {
			int[] _arr = { 9, -7, 3, 5, 1, 6, 0 };
			Console.WriteLine(TaskSolution(_arr));
		}
		public int TaskSolution(int[] arr)
		{
			int _sumElWithOddIndexes=0;

			for(int i=0;i<arr.Length;i++)
			{
				if(i%2!=0)
				{
					_sumElWithOddIndexes += arr[i];
				}
			}

			return _sumElWithOddIndexes;
		}
    }
}
