﻿using System;

namespace ArraysOneDimensional
{
	public class MaxElementOfArray
    {
        public MaxElementOfArray()
        {
            Task();
        }
		private void Task()
		{
			int[] _arr = { 9, -7, 3, 5, 1, 6, 0 };
			Console.WriteLine(TaskSolution(_arr));
		}
		public int TaskSolution(int[] arr)
		{
			int _maxElementIndex;
			_maxElementIndex = 0;
			for (int i = 1; i < arr.Length; i++)
			{
				if (arr[_maxElementIndex] < arr[i])
				{
					_maxElementIndex = i;
				}
			}

			return arr[_maxElementIndex];
		}
	}
}
