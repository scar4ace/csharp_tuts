﻿using System;
using System.Linq;

namespace ArraysOneDimensional
{
	public class SortArrayQuickMergeShellHeap
	{
		public SortArrayQuickMergeShellHeap()
		{
			Task();
		}
		private void Task()
		{
			int[] _arr = { 2, -1, 2, 5, -8, 9, 3, 0, 6 };

			//QuickSort(_arr, 0, _arr.Length - 1);
			//MergeSort(_arr, 0, _arr.Length - 1);
			//ShellSort(_arr,_arr.Length);
			//HeapSort(_arr);
			foreach (int element in _arr)
			{
				Console.Write(element +" ");
			}
		}
		public void QuickSort(int[] arr, int l, int r)
		{
			int _temp;
			int _pivot = arr[l + (r - l) / 2];
			//int _pivot = arr[(r + l) / 2];
			int i = l;
			int j = r;

			while (i <= j)
			{
				while (arr[i] < _pivot) i++;
				while (arr[j] > _pivot) j--;

				if (i <= j)
				{
					_temp = arr[i];
					arr[i] = arr[j];
					arr[j] = _temp;
					i++;
					j--;
				}
			}
			if (i < r)
			{
				QuickSort(arr, i, r);
			}
			if (l < j)
			{
				QuickSort(arr, l, j);
			}
		}
		public void MergeSort(int[] numbers, int left, int right)
		{
			int mid;

			if (right > left)
			{
				mid = (right + left) / 2;
				MergeSort(numbers, left, mid);
				MergeSort(numbers, (mid + 1), right);

				Merge(numbers, left, (mid + 1), right);
			}
		}
		public void Merge(int[] numbers, int left, int mid, int right)
		{
			int[] temp = new int[25];
			int i, eol, num, pos;

			eol = (mid - 1);
			pos = left;
			num = (right - left + 1);

			while ((left <= eol) && (mid <= right))
			{
				if (numbers[left] <= numbers[mid])
					temp[pos++] = numbers[left++];
				else
					temp[pos++] = numbers[mid++];
			}

			while (left <= eol)
				temp[pos++] = numbers[left++];

			while (mid <= right)
				temp[pos++] = numbers[mid++];

			for (i = 0; i < num; i++)
			{
				numbers[right] = temp[right];
				right--;
			}
		}
		public void ShellSort(int[] arr, int array_size)
		{
			int i, j, inc, temp;
			inc = 3;
			while (inc > 0)
			{
				for (i = 0; i < array_size; i++)
				{
					j = i;
					temp = arr[i];
					while ((j >= inc) && (arr[j - inc] > temp))
					{
						arr[j] = arr[j - inc];
						j = j - inc;
					}
					arr[j] = temp;
				}
				if (inc / 2 != 0)
					inc = inc / 2;
				else if (inc == 1)
					inc = 0;
				else
					inc = 1;
			}
		}
		public void HeapSort(int[] input)
		{
			//Build-Max-Heap
			int heapSize = input.Length;
			for (int p = (heapSize - 1) / 2; p >= 0; p--)
				MaxHeapify(input, heapSize, p);

			for (int i = input.Length - 1; i > 0; i--)
			{
				//Swap
				int temp = input[i];
				input[i] = input[0];
				input[0] = temp;

				heapSize--;
				MaxHeapify(input, heapSize, 0);
			}
		}
		public void MaxHeapify(int[] input, int heapSize, int index)
		{
			int left = (index + 1) * 2 - 1;
			int right = (index + 1) * 2;
			int largest = 0;

			if (left < heapSize && input[left] > input[index])
				largest = left;
			else
				largest = index;

			if (right < heapSize && input[right] > input[largest])
				largest = right;

			if (largest != index)
			{
				int temp = input[index];
				input[index] = input[largest];
				input[largest] = temp;

				MaxHeapify(input, heapSize, largest);
			}
		}
	}
}

