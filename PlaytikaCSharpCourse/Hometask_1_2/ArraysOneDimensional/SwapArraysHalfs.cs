﻿using System;

namespace ArraysOneDimensional
{
	public class SwapArraysHalfs
	{
		public SwapArraysHalfs()
		{
			Task();
		}
		private void Task()
		{
			int[] _arr = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
			TaskSolution(_arr);
			for (int i = 0; i < _arr.Length; i++)
			{
				Console.Write(_arr[i]);
			}
		}
		public void TaskSolution(int[] arr)
		{
			int _temp;
			int _tempIndex;
			int _arrHalf;

			if (arr.Length % 2 == 0)
			{
				_arrHalf = arr.Length / 2;
				_tempIndex = _arrHalf;
			}
			else
			{
				_arrHalf = (arr.Length - 1) / 2;
				_tempIndex = _arrHalf + 1;
			}

			for (int i = 0; i < _arrHalf; i++)
			{
				_temp = arr[i];
				arr[i] = arr[_tempIndex];
				arr[_tempIndex] = _temp;
				_tempIndex++;
			}
		}
	}
}
