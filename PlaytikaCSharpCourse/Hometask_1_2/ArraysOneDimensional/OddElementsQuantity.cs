﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArraysOneDimensional
{
    public class OddElementsQuantity
    {
        public OddElementsQuantity()
        {
            Task();
        }
		private void Task()
		{
			int[] _arr = { 9, -7, 3, 5, 1, 6, 0 };
			Console.WriteLine(TaskSolution(_arr));
		}
		public int TaskSolution(int[] arr)
		{
			int _oddElementsQuantity = 0;

			for(int i=0;i<arr.Length;i++)
			{
				if(arr[i]!=0 && arr[i]%2!=0)
				{
					_oddElementsQuantity++;
				}
			}

			return _oddElementsQuantity;
		}
    }
}
