﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArraysOneDimensional
{
	public class MinElementOfArray
	{
		public MinElementOfArray()
		{
			Task();
		}
		private void Task()
		{
			int[] _arr = { 9, -7, 3, 5, 1, 6, 0 };
			Console.WriteLine(TaskSolution(_arr));
		}
		public int TaskSolution(int[] arr)
		{
			int _minElementIndex;
			_minElementIndex = 0;
			for (int i = 1; i < arr.Length; i++)
			{
				if (arr[_minElementIndex] > arr[i])
				{
					_minElementIndex = i;
				}
			}

			return arr[_minElementIndex];
		}
	}
}
