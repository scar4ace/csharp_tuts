﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArraysOneDimensional
{
    class Program
    {
        static void Main(string[] args)
        {
            //Task1
            //MinElementOfArray task1 = new MinElementOfArray();
            //Task2
            //MaxElementOfArray task2 = new MaxElementOfArray();
            //Task3
            //IndexOfMinElement task3 = new IndexOfMinElement();
            //Task4
            //IndexOfMaxElement task4 = new IndexOfMaxElement();
            //Task5
            //SumElementsWithOddIndexes task5 = new SumElementsWithOddIndexes();
            //Task6
            //ReversArray task6 = new ReversArray();
            //Task7
            //OddElementsQuantity task7 = new OddElementsQuantity();
            //Task8
            //SwapArraysHalfs task8 = new SwapArraysHalfs();
            //Task9
            //SortArrayBubbleSelectInsert task9 = new SortArrayBubbleSelectInsert();
            //Task10
            SortArrayQuickMergeShellHeap task10 = new SortArrayQuickMergeShellHeap();

            Console.ReadKey();
        }
    }
}
