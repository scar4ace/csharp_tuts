﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArraysOneDimensional
{
	public class ReversArray
	{
		public ReversArray()
		{
			Task();
		}
		private void Task()
		{
			int[] _arr = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
			TaskSolution(_arr);
			for (int i = 0; i < _arr.Length; i++)
			{
				Console.Write(_arr[i]);
			}
		}
		public void TaskSolution(int[] arr)
		{
			int _temp;
			int _tempIndex = arr.Length - 1;
			int _arrHalf;
			if (arr.Length % 2 == 0)
				_arrHalf = arr.Length / 2;
			else
				_arrHalf = (arr.Length - 1) / 2;

			for (int i = 0; i < _arrHalf; i++)
			{
				_temp = arr[i];
				arr[i] = arr[_tempIndex];
				arr[_tempIndex] = _temp;
				_tempIndex--;
			}
		}
	}
}
