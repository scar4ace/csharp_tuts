﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArraysOneDimensional
{
    public class SortArrayBubbleSelectInsert
    {
        public SortArrayBubbleSelectInsert()
        {
            Task();
        }
        private void Task()
        {
            int[] _arr = { 2, -1, 2, 5, -8, 9, 3, 0, 6 };
            BubbleSort(_arr);
            //SelectSort(_arr);
            //InsertSort(_arr);
            for (int i = 0; i < _arr.Length; i++)
            {
                Console.Write(_arr[i]);
            }
        }
        public void BubbleSort(int[] arr)
        {
            int _temp;

            for (int i = arr.Length; i > 2; i--)
            {
                for (int y = 0; y < arr.Length - 1; y++)
                {
                    if (arr[y] > arr[y + 1])//swap the elements by the priority
                    {
                        _temp = arr[y];
                        arr[y] = arr[y + 1];
                        arr[y + 1] = _temp;
                    }
                }
            }
        }
        public void SelectSort(int[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                int _minElement = arr[i];
                for (int j = i + 1; j < arr.Length; j++)//to find the min element in the unsorted tail
                {
                    if (_minElement > arr[j])//swap the elements by the priority
                    {
                        _minElement = arr[j];
                        arr[j] = arr[i];
                        arr[i] = _minElement;
                    }
                }
            }
        }
        public void InsertSort(int[] arr)
        {
            for (int i = 1; i < arr.Length; i++)//i next unsorted element
            {
                int _temp = arr[i];
                int j = i - 1;
                while (j >= 0 && arr[j] > _temp)
                {
                    arr[j + 1] = arr[j];//shift of a bigger element one step right
                    j--;//step to [0] of the sorted part
                }
                arr[j + 1] = _temp;
            }
        }
    }
}
