﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loops
{
    class FindFactorialN
    {
        public FindFactorialN()
        {
            Task();
        }
        //IN
        private void Task()
        {
            uint _number=6;

            Output("Factorial of "+_number.ToString()+" is: "+(TaskSolution(_number)).ToString());
        }
        //PROCESSING
        private uint TaskSolution(uint number)
        {
            uint _result = 1;
            if (number == 0)
            {
                _result = 1;
            } else {
                for (uint i = 1; i<=number; i++)
                {
                    _result = _result * i;
                }
            }
            return _result;
        }
        //OUT
        private void Output(string message)
        {
            Console.WriteLine(message);
        }
    }
}
