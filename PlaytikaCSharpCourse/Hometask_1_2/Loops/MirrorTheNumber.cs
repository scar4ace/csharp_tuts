﻿using System;

namespace Loops
{
	public class MirrorTheNumber
    {
        public MirrorTheNumber()
        {
            Task();
        }
        private void Task()
        {
			string _number = "-123";
			Console.Write(TaskSolution(_number));
		}
		public string TaskSolution(string number)
        {
			string _mirror = "";
			for (int i = number.Length-1; i >= 0; i--)
			{
				_mirror += number[i];
			}
			return _mirror;
        }
    }
}
