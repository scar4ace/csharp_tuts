﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loops
{
	public class CountRoot
	{
		public CountRoot()
		{
			Task();
		}
		void Task()
		{
			//int _number = 81;//9
			//int _number = 152000;//389.87
			int _number = 1520000;//1224.7 fail

			Console.Write(TaskSolution(_number));
		}
		public int TaskSolution(int number)
		{
			int _squareRoot = number / 2;

			while ((_squareRoot * _squareRoot) > number)
			{
				_squareRoot = _squareRoot / 2;
			}
			for (int i = _squareRoot; (i*i) <= number; i++)
			{
				_squareRoot = i;
			}
			return _squareRoot;
		}
	}
}
