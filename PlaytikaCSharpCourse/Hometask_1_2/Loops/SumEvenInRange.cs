﻿using System;

namespace Loops
{
    class SumEvenInRange
    {
        public SumEvenInRange()
        {
            Task();
        }
        //IN
        private void Task()
        {
            int _rangeMin = 1;
            int _rangeMax = 99;

            TaskSolution(_rangeMin, _rangeMax);
        }
        //PROCESSING
        private void TaskSolution(int rangeMin, int rangeMax)
        {
            int _sumEvenNumbers = 0;
            int _quantityEvenNumbers = 0;

            for(int i = rangeMin; i <= rangeMax; i++)
            {
                if (i % 2 == 0)
                {
                    _sumEvenNumbers += i;
                    _quantityEvenNumbers ++;
                }
            }

            Output(String.Format("Range: from {0} to {1}\nSum of the even numbers: {2}\nQuantity of the even numbers: {3}",rangeMin,rangeMax,_sumEvenNumbers,_quantityEvenNumbers));
        }
        //OUT
        private void Output(string message)
        {
            Console.WriteLine(message);
        }
    }
}
