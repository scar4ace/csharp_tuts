﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loops
{
    class DigitSum
    {
        public DigitSum()
        {
            Task();
        }
        //IN
        private void Task()
        {
			int _number = 0;
			Console.WriteLine("Enter a number: ");
			int.TryParse(Console.ReadLine(), out _number);
			//VALIDATION
			if (_number >= 0)
				TaskSolution(_number);
			else
				Output("You entered an incorrect number!");
        }
        //PROCESSING
        private void TaskSolution(int number)
        {
			int _sum = 0;
			int _delta = 0;

			while (number>10)
			{
				_delta = number % 10;
				_sum += _delta;
				number = (number-_delta)/10;
			}
			if (number != 0)
			{
				_sum += number;
			}
			Output("The sum of the digits: "+_sum.ToString());
        }
        //OUT
        private void Output(string message)
        {
            Console.WriteLine(message);
        }
    }
}
