﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loops
{
    class CheckPrimeNumber
    {
        public CheckPrimeNumber()
        {
            Task();
        }
        //IN
        private void Task()
        {
            uint _number = 11;

            TaskSolution(_number);
        }
        //PROCESSING
        private void TaskSolution(uint number)
        {
            bool isPrime = true;

            //Solution 1
            for (uint i = 2; i < number; i++)
            {
                if (number % i == 0)
                {
                    isPrime = false;
                    break;
                }
            }

            //Solution 2
            //if(number % 2==0 || number % 3 == 0 || number % 5 == 0 || number % 7 == 0)
            //{
            //    isPrime = false;
            //}

            if (isPrime)
            {
                Output(number + " is a prime number");
            }else
            {
                Output(number + " is not a prime number");
            }
        }
        //OUT
        private void Output(string message)
        {
            Console.WriteLine(message);
        }
    }
}
