﻿using System;

namespace Conditions
{
	public static class AssociateTwoValues
    {
        public static string TaskSolution(int value)
        {
            string _grade = "";

            if (value >= 0 && value <= 19)
                _grade = "F";
            else if (value >= 20 && value <= 39)
                _grade = "E";
            else if (value >= 40 && value <= 59)
                _grade = "D";
            else if (value >= 60 && value <= 74)
                _grade = "C";
            else if (value >= 75 && value <= 89)
                _grade = "B";
            else if (value >= 90 && value <= 100)
                _grade = "A";
            else
				throw new ArgumentException();

			if (_grade == "")
				throw new ArgumentException();
			else
				return _grade;
        }
    }
}
