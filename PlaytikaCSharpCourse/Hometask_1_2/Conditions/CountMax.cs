﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Conditions
{
    class CountMax
    {
        public CountMax()
        {
            Task();
        }
        //IN
        private void Task()
        {
            int _a = 2;
            int _b = 3;
            int _c = 4;
            int _delta = 3;

            TaskSolution(_a, _b, _c, _delta);
        }
        //PROCESSING
        private void TaskSolution(int a, int b, int c, int delta)
        {
            Output((ReturnMax(a+b+c, a*b*c) + delta).ToString());
        }
        private int ReturnMax(int first, int second)
        {
            if (first > second)
                return first;
            else
                return second;
        }
        //OUT
        private void Output(string message)
        {
            Console.WriteLine(message);
        }
    }
}
