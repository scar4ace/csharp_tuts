﻿using System;

namespace Conditions
{
	public class IdentifyQuarter
	{
		public IdentifyQuarter()
		{
			Task();
		}
		private void Task()
		{
			double _xCoord = 5.7;
			double _yCoord = -3;

			Console.Write(TaskSolution(_xCoord, _yCoord));
		}
		public int TaskSolution(double xCoord, double yCoord)
		{
			int _quarter = 0;

			if (xCoord > 0 && yCoord > 0)
				_quarter = 1;//++
			else if (xCoord < 0 && yCoord > 0)
				_quarter = 2;//-+
			else if (xCoord < 0 && yCoord < 0)
				_quarter = 3;//--
			else if (xCoord > 0 && yCoord < 0)
				_quarter = 4;//+-
			else if (xCoord == 0 && yCoord > 0)
				throw new ArgumentException();
			else if (xCoord == 0 && yCoord < 0)
				throw new ArgumentException();
			else if (xCoord > 0 && yCoord == 0)
				throw new ArgumentException();
			else if (xCoord < 0 && yCoord == 0)
				throw new ArgumentException();
			else { }

			return _quarter;
		}
	}
}
