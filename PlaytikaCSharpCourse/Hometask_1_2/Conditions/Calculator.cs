﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Conditions
{
    class Calculator
    {
        public Calculator()
        {
            Task();
        }
        //IN
        private void Task()
        {
            int _a = 0;
            int _b = 0;

            Console.WriteLine("Enter a: ");
            Int32.TryParse(Console.ReadLine(), out _a);
            Console.WriteLine("Enter b: ");
            Int32.TryParse(Console.ReadLine(), out _b);

            TaskSolution(_a, _b);
        }
        //PROCESSING
        private void TaskSolution(int a, int b)
        {
            int _result = 0;
            if (a % 2 == 0)
                _result = Multiply(a, b);
            else
                _result = Add(a, b);

            Output("Result: " + _result);
        }
        private int Add(int a, int b)
        {
            int _result = 0;
            _result = a + b;

            return _result;
        }
        private int Multiply(int a, int b)
        {
            int _result = 0;
            _result = a * b;
            return _result;
        }
        //OUT
        private void Output(string message)
        {
            Console.WriteLine(message);
        }
    }
}
