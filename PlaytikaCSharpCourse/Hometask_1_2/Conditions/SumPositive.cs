﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Conditions
{
    class SumPositive
    {
        public SumPositive()
        {
            Task();
        }
        //IN
        private void Task()
        {
            int _a = 0;
            int _b = 0;
            int _c = 0;

            Console.WriteLine("A: ");
            int.TryParse(Console.ReadLine(), out _a);
            Console.WriteLine("B: ");
            int.TryParse(Console.ReadLine(), out _b);
            Console.WriteLine("C: ");
            int.TryParse(Console.ReadLine(), out _c);

            TaskSolution(_a, _b, _c);
        }
        //PROCESSING
        private void TaskSolution(int a, int b, int c)
        {
            int _sum = 0;

            if (a >= 0)
                _sum += a;
            if (b >= 0)
                _sum += b;
            if (c >= 0)
                _sum += c;

            Output("Sum of the positive numbers is: " + _sum);
        }
        //OUT
        private void Output(string message)
        {
            Console.WriteLine(message);
        }
    }
}
