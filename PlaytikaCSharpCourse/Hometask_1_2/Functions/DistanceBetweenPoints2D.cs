﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Functions
{
	class DistanceBetweenPoints2D
	{
		public DistanceBetweenPoints2D()
		{
			Task();
		}
		//IN
		private void Task()
		{
			double _aX = 5;
			double _aY = -2;
			double _bX = -1;
			double _bY = 1;

			Console.WriteLine(TaskSolution(_aX, _aY, _bX, _bY));
		}
		//PROCESSING
		private double TaskSolution(double aX, double aY, double bX, double bY)
		{
			double _distance = Math.Round(Math.Sqrt(Math.Pow((aX - bX), 2) + Math.Pow((aY - bY), 2)), 1);

			return _distance;
		}
	}
}
