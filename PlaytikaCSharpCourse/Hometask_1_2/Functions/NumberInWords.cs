﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Functions
{
	class NumberInWords
	{
		public NumberInWords()
		{
			Task();
		}
		private void Task()
		{
			//IN
			int _number = 0;
			//OUT
			Console.WriteLine(TaskSolution(_number));
		}
		//PROCESSING
		private string TaskSolution(int number)
		{
			string _wordLine = "";
			string _wordLineBuffer = "";
			int _delta;
			try
			{
				_delta = number % 100;
				if (number % 100 >= 0 && number % 100 <= 20)
				{
					_wordLine = NumberToWord(_delta);
					_wordLineBuffer = _wordLine;
					number -= _delta;
					if (number > 0)
					{
						_wordLine = NumberToWord(number) + " " + _wordLineBuffer;
					}
				}
				else
				{
					_delta = number % 10;
					if (_delta != 0)
					{
						_wordLine = NumberToWord(_delta);
						_wordLineBuffer = _wordLine;
						number -= _delta;
					}
					_delta = number % 100;
					_wordLine = NumberToWord(_delta) + " " + _wordLineBuffer;
					_wordLineBuffer = _wordLine;
					number -= _delta;

					if (number != 0)
					{
						_wordLine = NumberToWord(number) + " " + _wordLineBuffer;
					}
				}
			}
			catch (ArgumentException ex)
			{
				_wordLine = "";
				Console.WriteLine("Exeption: " + ex.Message);
			}
			finally
			{
				Console.WriteLine("Завершение работы");
			}
			return _wordLine;
		}
		private string NumberToWord(int number)
		{
			string _word = "";
			switch (number)
			{
				case 0:
					_word = "Ноль";
					break;
				case 1:
					_word = "Один";
					break;
				case 2:
					_word = "Два";
					break;
				case 3:
					_word = "Три";
					break;
				case 4:
					_word = "Четыре";
					break;
				case 5:
					_word = "Пять";
					break;
				case 6:
					_word = "Шесть";
					break;
				case 7:
					_word = "Семь";
					break;
				case 8:
					_word = "Восемь";
					break;
				case 9:
					_word = "Девять";
					break;
				case 10:
					_word = "Десять";
					break;
				case 11:
					_word = "Одинадцать";
					break;
				case 12:
					_word = "Двенадцать";
					break;
				case 13:
					_word = "Тринадцать";
					break;
				case 14:
					_word = "Четырнадцать";
					break;
				case 15:
					_word = "Пятнадцать";
					break;
				case 16:
					_word = "Шестнадцать";
					break;
				case 17:
					_word = "Семнадцать";
					break;
				case 18:
					_word = "Восемнадцать";
					break;
				case 19:
					_word = "Девятнадцать";
					break;
				case 20:
					_word = "Двадцать";
					break;
				case 30:
					_word = "Тридцать";
					break;
				case 40:
					_word = "Сорок";
					break;
				case 50:
					_word = "Пятьдесят";
					break;
				case 60:
					_word = "Шестьдесят";
					break;
				case 70:
					_word = "Семьдесят";
					break;
				case 80:
					_word = "Восемьдесят";
					break;
				case 90:
					_word = "Девяносто";
					break;
				case 100:
					_word = "Сто";
					break;
				case 200:
					_word = "Двести";
					break;
				case 300:
					_word = "Триста";
					break;
				case 400:
					_word = "Четыреста";
					break;
				case 500:
					_word = "Пятьсот";
					break;
				case 600:
					_word = "Шестьсот";
					break;
				case 700:
					_word = "Семьсот";
					break;
				case 800:
					_word = "Восемьсот";
					break;
				case 900:
					_word = "Девятьсот";
					break;
				default:
					throw new ArgumentException("There is no match for this number");
			}

			return _word;
		}
	}
}
