﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Functions
{
    class StringByNumber
    {
        public StringByNumber()
        {
            Task();
        }
        //IN
        private void Task()
        {
			int _numberOfTheDay = 0;
			Console.WriteLine("Enter a number of the day from 1 to 7: ");
			int.TryParse(Console.ReadLine(), out _numberOfTheDay);
			//VALIDATION
			if (_numberOfTheDay>=1 && _numberOfTheDay<=7)
			{
				TaskSolution(_numberOfTheDay.ToString());
			}
        }
        //PROCESSING
        private void TaskSolution(string numberOfTheDay)
        {
			string _dayName = "";
			switch (numberOfTheDay)
			{
				case "1":
					_dayName = "Sunday";
					break;
				case "2":
					_dayName = "Monday";
					break;
				case "3":
					_dayName = "Tuesday";
					break;
				case "4":
					_dayName = "Wednesday";
					break;
				case "5":
					_dayName = "Thursday";
					break;
				case "6":
					_dayName = "Friday";
					break;
				case "7":
					_dayName = "Saturday";
					break;
				default:
					break;
			}
			Output("It's "+_dayName+"!");
		}
        //OUT
        private void Output(string message)
        {
            Console.WriteLine(message);
        }
    }
}
