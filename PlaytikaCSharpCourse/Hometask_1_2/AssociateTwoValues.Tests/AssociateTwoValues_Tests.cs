﻿using NUnit.Framework;
using System;

namespace Conditions.Tests
{
	[TestFixture]
    public class AssociateTwoValues_Tests
    {
		// F
		[Test]
		public void TaskSolution_0Input_FReturned()
		{
			Assert.AreEqual("F", AssociateTwoValues.TaskSolution(0));
		}
		[Test]
		public void TaskSolution_19Input_FReturned()
		{
			Assert.AreEqual("F", AssociateTwoValues.TaskSolution(19));
		}
		// E
		[Test]
		public void TaskSolution_20Input_EReturned()
		{
			Assert.AreEqual("E", AssociateTwoValues.TaskSolution(20));
		}
		[Test]
		public void TaskSolution_39Input_EReturned()
		{
			Assert.AreEqual("E", AssociateTwoValues.TaskSolution(39));
		}
		// D
		[Test]
		public void TaskSolution_40Input_DReturned()
		{
			Assert.AreEqual("D", AssociateTwoValues.TaskSolution(40));
		}
		[Test]
		public void TaskSolution_59Input_DReturned()
		{
			Assert.AreEqual("D", AssociateTwoValues.TaskSolution(59));
		}
		// C
		[Test]
		public void TaskSolution_60Input_CReturned()
		{
			Assert.AreEqual("C", AssociateTwoValues.TaskSolution(60));
		}
		[Test]
		public void TaskSolution_74Input_CReturned()
		{
			Assert.AreEqual("C", AssociateTwoValues.TaskSolution(74));
		}
		// B
		[Test]
		public void TaskSolution_75Input_BReturned()
		{
			Assert.AreEqual("B", AssociateTwoValues.TaskSolution(75));
		}
		[Test]
		public void TaskSolution_89Input_BReturned()
		{
			Assert.AreEqual("B", AssociateTwoValues.TaskSolution(89));
		}
		// A
		[Test]
		public void TaskSolution_90Input_AReturned()
		{
			Assert.AreEqual("A", AssociateTwoValues.TaskSolution(90));
		}
		[Test]
		public void TaskSolution_100Input_AReturned()
		{
			Assert.AreEqual("A", AssociateTwoValues.TaskSolution(100));
		}
		// Exceptions
		[Test]
		public void TaskSolution_101Input_ArgumentException()
		{
			Assert.Throws<ArgumentException>(() => AssociateTwoValues.TaskSolution(101));
		}
		[Test]
		public void TaskSolution_Negative1Input_ArgumentException()
		{
			Assert.Throws<ArgumentException>(() => AssociateTwoValues.TaskSolution(-1));
		}
	}
}
