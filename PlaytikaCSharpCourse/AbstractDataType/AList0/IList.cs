﻿namespace List
{
	public interface IList
	{
		void Init(int[] arr);
		void Clear();
		int Size();
		int Get(int pos);
		void Set(int pos, int val);
		void AddStart(int val);
		void AddEnd(int val);
		void AddPos(int pos, int val);
		int DelStart();
		int DelEnd();
		int DelPos(int pos);
		void HalfRevers();
		int[] ToArray();
		void Sort();
		void Revers();
		int Min();
		int Max();
		int MinIndex();
		int MaxIndex();
	}
}

