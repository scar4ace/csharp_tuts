﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace List
{
	public class LList1 : IList
	{
		class Node
		{
			public int val;
			public Node next;
			public Node(int val)
			{
				this.val = val;
			}
		}
		Node root = null;
		public void Init(int[] arr)
		{
			root = null;
			if (arr == null)
				arr = new int[0];

			for (int i = arr.Length - 1; i >= 0; i--)
			{
				AddStart(arr[i]);
			}
		}
		public override string ToString()
		{
			Node r = root;
			String ret = "";
			while (r != null)
			{
				ret += r.val + (r.next == null ? "" : ", ");
				r = r.next;
			}
			return ret;
		}
		public int[] ToArray()
		{
			int[] ret = new int[Size()];
			int i = 0;
			Node r = root;
			while (r != null)
			{
				ret[i++] = r.val;
				r = r.next;
			}
			return ret;
		}
		public void Clear()
		{
			root = null;
		}
		public int Size()
		{
			int i = 0;
			Node r = root;
			while (r != null)
			{
				i++;
				r = r.next;
			}
			return i;
		}
		public int Get(int pos)
		{
			if (root == null || pos < 0 || pos > Size() - 1)
				throw new IndexOutOfRangeException();
			if (pos == 0)
				return root.val;
			return GetNodeByPos(pos).val;
			//Node r = root;
			//int i = 0;
			//while (i < pos)
			//{
			//	r = r.next;
			//	i++;
			//}
			//return r.val;
		}
		public void Set(int pos, int val)
		{
			if (root == null || pos < 0 || pos > Size() - 1)
				throw new IndexOutOfRangeException();
			if (pos == 0)
				root.val = val;
			GetNodeByPos(pos).val = val;
			//int i = 0;
			//Node r = root;
			//while (i < pos)
			//{
			//	r = r.next;
			//	i++;
			//}
			//r.val = val;
		}
		public void AddStart(int val)
		{
			Node tmp = new Node(val);
			tmp.next = root;
			root = tmp;
		}
		public void AddEnd(int val)
		{
			Node tmp = new Node(val);
			if (root == null)
			{
				root = tmp;
				return;
			}
			GetNodeByPos(Size() - 1).next = tmp;
			//Node r = root;
			//while (r.next != null)
			//{
			//	r = r.next;
			//}
			//r.next = tmp;
		}
		public void AddPos(int pos, int val)
		{
			if (root == null || pos < 0 || pos > Size() - 1)
				throw new IndexOutOfRangeException();
			Node tmp = new Node(val);
			if (pos == 0)
			{
				tmp.next = root.next;
				tmp.val = root.val;
				root.next = tmp;
				root.val = val;
			}
			else
			{
				tmp.next = GetNodeByPos(pos);
				GetNodeByPos(pos - 1).next = tmp;
				//Node r = root;
				//Node prev = null;
				//int i = 0;
				//while (i < pos)
				//{
				//	prev = r;
				//	r = r.next;
				//	i++;
				//}
				//prev.next = tmp;
				//tmp.next = r;
			}
		}
		public int DelStart()
		{
			if (root == null)
				throw new IndexOutOfRangeException();
			int ret = root.val;
			root = root.next;
			return ret;
		}
		public int DelEnd()
		{
			if (root == null)
				throw new IndexOutOfRangeException();
			int ret = GetNodeByPos(Size() - 1).val;
			if (GetNodeByPos(Size() - 2) != null && Size() - 2 >= 0)
				GetNodeByPos(Size() - 2).next = null;
			else
				root = null;
			return ret;
			//Node r = root;
			//Node prev = null;
			//while (r.next != null)
			//{
			//	prev = r;
			//	r = r.next;
			//}
			//int ret = r.val;
			//if (prev != null)
			//	prev.next = null;
			//else
			//	root = null;
			//return ret;
		}
		public int DelPos(int pos)
		{
			if (root == null || pos < 0 || pos > Size() - 1)
				throw new IndexOutOfRangeException();
			int ret = 0;
			//Node r = root;
			//Node prev = null;
			//int i = 0;
			if (pos == 0)
			{
				ret = root.val;
				root = root.next;
			}
			else
			{
				//while (i < pos)
				//{
				//	prev = r;
				//	r = r.next;
				//	i++;
				//}
				//ret = r.val;
				//prev.next = r.next;
				//r = null;
				ret = GetNodeByPos(pos).val;
				GetNodeByPos(pos - 1).next = GetNodeByPos(pos).next;
				Node r = GetNodeByPos(pos);
				r = null;
			}
			return ret;
		}
		public void HalfRevers()
		{
			if (root == null)
				return;
			int d = Size() % 2 == 0 ? 0 : 1;
			for (int i = 0; i < Size() / 2; i++)
			{
				int tmp = GetNodeByPos(i).val;
				GetNodeByPos(i).val = GetNodeByPos(Size() / 2 + i + d).val;
				GetNodeByPos(Size() / 2 + i + d).val = tmp;
			}
		}
		public void Sort()// Insert sort
		{
			if (Size() <= 1)
				return;
			Node s = root;// Sorted ref
			Node sPrev = null;// Sorted Previous ref
			Node u = s.next;// Unsorted ref
			Node uPrev = s;// Unsorted Previous ref
			while (u != null)
			{
				if (s.val >= u.val)
				{
					uPrev.next = u.next;
					u.next = s;
					if (s == root)
						root = u;
					else
						sPrev.next = u;

					u = uPrev.next;
					s = root;
					sPrev = null;
				}
				else
				{
					if (s.next == u)
					{
						uPrev = u;

						u = uPrev.next;
						s = root;
						sPrev = null;
					}
					else
					{
						sPrev = s;
						s = s.next;
					}
				}
			}
		}
		public void Revers()
		{
			if (root == null)
				return;
			int i = 0;
			int tmpVal;
			while (i < Size() / 2)
			{
				tmpVal = GetNodeByPos(i).val;
				GetNodeByPos(i).val = GetNodeByPos(Size() - 1 - i).val;
				GetNodeByPos(Size() - 1 - i).val = tmpVal;
				i++;
			}
		}
		public int Min()
		{
			if (root == null)
				throw new InvalidOperationException();
			Node r = root;
			int ret = r.val;
			while (r.next != null)
			{
				r = r.next;
				if (ret > r.val)
					ret = r.val;
			}
			return ret;
		}
		public int Max()
		{
			if (root == null)
				throw new InvalidOperationException();
			Node r = root;
			int ret = r.val;
			while (r.next != null)
			{
				r = r.next;
				if (ret < r.val)
					ret = r.val;
			}
			return ret;
		}
		public int MinIndex()
		{
			if (root == null)
				throw new InvalidOperationException();
			Node r = root;
			int i = 0;
			while (r.next != null)
			{
				if (r.val > r.next.val)
					i++;
				r = r.next;
			}
			return i;
		}
		public int MaxIndex()
		{
			if (root == null)
				throw new InvalidOperationException();
			Node r = root;
			int i = 0;
			while (r.next != null)
			{
				if (r.val < r.next.val)
					i++;
				r = r.next;
			}
			return i;
		}
		// --------------------------------------------------------- LList extra methods ---------------------------------------------------------
		private Node GetNodeByPos(int pos)
		{
			Node r = root;
			int i = 0;
			while (i < pos)
			{
				r = r.next;
				i++;
			}
			return r;
		}
	}
}
