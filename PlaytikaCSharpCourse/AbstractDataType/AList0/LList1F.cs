﻿using System;

namespace List
{
	public class LList1F : IList
	{
		class Node
		{
			public int val;
			public Node next;
			public Node(int val)
			{
				this.val = val;
			}
		}
		Node root = null;
		public void Init(int[] arr)
		{
			root = null;
			if (arr == null)
				arr = new int[0];

			for (int i = arr.Length - 1; i >= 0; i--)
			{
				AddStart(arr[i]);
			}
		}
		public override string ToString()
		{
			return NodeToString(root);
		}
		private string NodeToString(Node p)
		{
			if (p == null)
				return "";
			string ret = p.val + (p.next == null ? "" : ", ");
			ret += NodeToString(p.next);
			return ret;
		}
		public int Size()
		{
			return NodeSize(root);
		}
		private int NodeSize(Node p)
		{
			if (p == null)
				return 0;
			return 1 + NodeSize(p.next);
		}
		public int[] ToArray()
		{
			int[] ret = new int[Size()];
			NodeToArray(root, ret, 0);
			return ret;
		}
		private void NodeToArray(Node p, int[] ret, int i)
		{
			if (p == null)
				return;
			ret[i++] = p.val;
			NodeToArray(p.next, ret, i);
		}
		public void AddEnd(int val)
		{
			NodeAddEnd(ref root, val);
		}
		public void Clear()
		{
			root = null;
		}
		public int DelStart()
		{
			if (root == null)
				throw new IndexOutOfRangeException();
			int ret = root.val;
			if (root.next == null)
				root = null;
			else
			{
				root.val = root.next.val;
				root.next = root.next.next;
			}
			return ret;
		}
		private void NodeAddEnd(ref Node p, int val)
		{
			if (p == null)
				p = new Node(val);
			else
				NodeAddEnd(ref p.next, val);
		}
		public void AddPos(int pos, int val)// implemented by me
		{
			NodeAddPos(root, pos, val, 0, null);
		}
		private void NodeAddPos(Node p, int pos, int val, int i, Node prev)
		{
			if (p == null || pos < 0 || pos >= Size())
				throw new IndexOutOfRangeException();
			Node tmpNode = new Node(val);
			if (pos == 0)
			{
				tmpNode.next = p.next;
				tmpNode.val = p.val;
				p.next = tmpNode;
				p.val = val;
				return;
			}
			if (pos == i)
			{
				prev.next = tmpNode;
				tmpNode.next = p;
				return;
			}
			else
			{
				if (p.next != null)
					NodeAddPos(p.next, pos, val, ++i, p);
			}
		}
		public void AddStart(int val)
		{
			Node tmp = new Node(val);
			tmp.next = root;
			root = tmp;
		}
		public int DelEnd()// implemented by me
		{
			return NodeDelEnd(root, null);
		}
		private int NodeDelEnd(Node p, Node prev)
		{
			if (p == null)
				throw new IndexOutOfRangeException();
			if (p.next == null)
			{
				int ret = p.val;
				if (prev == null)
					root = null;
				else
					prev.next = null;
				return ret;
			}
			return NodeDelEnd(p.next, p);
		}
		public int DelPos(int pos)// implemented by me
		{
			return NodeDelPos(root, pos, 0, null);
		}
		private int NodeDelPos(Node p, int pos, int i, Node prev)
		{
			if (p == null || pos < 0 || pos >= Size())
				throw new IndexOutOfRangeException();
			if (pos == i)
			{
				if (i == 0)
					return DelStart();
				if (i == Size() - 1)
					return DelEnd();
				int ret = p.val;
				prev.next = p.next;
				return ret;
			}
			return NodeDelPos(p.next, pos, ++i, p);
		}
		public int Get(int pos)
		{
			return NodeGet(root, pos, 0);
		}
		private int NodeGet(Node p, int pos, int i)
		{
			if (p == null || pos < i || pos >= Size())
				throw new IndexOutOfRangeException();
			if (pos == i)
				return p.val;
			return NodeGet(p.next, pos, ++i);
		}
		public void Set(int pos, int val)// implemented by me
		{
			NodeSet(root, pos, 0, val);
		}
		private void NodeSet(Node p, int pos, int i, int val)
		{
			if (p == null || pos < 0 || pos >= Size())
				throw new IndexOutOfRangeException();
			if (pos == i)
				p.val = val;
			else
				NodeSet(p.next, pos, ++i, val);
		}
		public int Min()// implemented by me
		{
			if (root == null)
				throw new InvalidOperationException();
			return NodeMin(root, root.val);
		}
		private int NodeMin(Node p, int val)
		{
			if (p.next != null)
			{
				if (val > p.next.val)
					return NodeMin(p.next, p.next.val);
				else
					return NodeMin(p.next, val);
			}
			return val;
		}
		public int Max()// implemented by me
		{
			if (root == null)
				throw new InvalidOperationException();
			return NodeMax(root, root.val);
		}
		private int NodeMax(Node p, int val)
		{
			if (p.next != null)
			{
				if (val < p.next.val)
					return NodeMax(p.next, p.next.val);
				else
					return NodeMax(p.next, val);
			}
			return val;
		}
		public int MinIndex()// implemented by me
		{
			return NodeMinIndex(root, 0);
		}
		private int NodeMinIndex(Node p, int i)
		{
			if (root == null)
				throw new InvalidOperationException();
			if (Min() != p.val)
				return NodeMinIndex(p.next, ++i);
			return i;
		}
		public int MaxIndex()// implemented by me
		{
			return NodeMaxIndex(root, 0);
		}
		private int NodeMaxIndex(Node p, int i)
		{
			if (root == null)
				throw new InvalidOperationException();
			if (Max() != p.val)
				return NodeMaxIndex(p.next, ++i);
			return i;
		}
		public void HalfRevers()// implemented by me
		{
			int d = Size() % 2;
			NodeHalfRevers(root, root, 0, Size() / 2 + d);
		}
		private void NodeHalfRevers(Node p, Node p2, int i, int halfStart)
		{
			if (p == null)
				return;
			if (i == halfStart)
			{
				int tmp = p.val;
				p.val = p2.val;
				p2.val = tmp;
				if (p2.next != null)
					NodeHalfRevers(p.next, p2.next, i, halfStart);
			}
			else
			{
				if (p2.next != null)
					NodeHalfRevers(p, p2.next, ++i, halfStart);
			}
		}
		public void Revers()// implemented by me
		{
			NodeRevers(root, root, Size() - 1, 0);
		}
		private void NodeRevers(Node p, Node p2, int q, int i)
		{
			if (p == null)
				return;
			if (q > i)
			{
				NodeRevers(p, p2.next, q, ++i);
			}
			else
			{
				int tmp = p.val;
				p.val = p2.val;
				p2.val = tmp;
				NodeRevers(p.next, p.next, q - 2, 0);
			}
		}
		public void Sort()// implemented by me
		{
			if (root == null)
				return;
			NodeInsertSort(root, null, root.next, root);
		}
		private void NodeInsertSort(Node s, Node sPrev, Node u, Node uPrev)
		{
			if (u == null)
				return;
			if (s.val >= u.val)
			{
				uPrev.next = u.next;
				u.next = s;
				if (s == root)
					root = u;
				else
					sPrev.next = u;

				u = uPrev.next;
				s = root;
				sPrev = null;
			}
			else
			{
				if (s.next == u)
				{
					uPrev = u;

					u = uPrev.next;
					s = root;
					sPrev = null;
				}
				else
				{
					sPrev = s;
					s = s.next;
				}
			}
			NodeInsertSort(s, sPrev, u, uPrev);
		}
	}
}
