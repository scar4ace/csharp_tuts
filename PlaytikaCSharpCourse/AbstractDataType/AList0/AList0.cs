﻿using System;

namespace List
{
	public class AList0 : IList
	{
		private int[] arr = { };
		public AList0()
		{

		}
		public AList0(int[] arrIn)
		{
			Init(arrIn);
		}

		public void Init(int[] arrIn)
		{
			if (arrIn == null)
				arrIn = new int[0];
			arr = new int[arrIn.Length];
			for (int i = 0; i < arrIn.Length; i++)
			{
				arr[i] = arrIn[i];
			}
		}
		public void Clear()
		{
			arr = new int[0];
		}
		public int Size()
		{
			return arr.Length;
		}
		public int Get(int posIn)
		{
			if (arr.Length == 0 || posIn < 0 || posIn > arr.Length - 1)
				throw new IndexOutOfRangeException();
			return arr[posIn];
		}
		public void Set(int posIn, int valIn)
		{
			if (arr.Length == 0 || posIn < 0 || posIn > arr.Length - 1)
				throw new IndexOutOfRangeException();
			arr[posIn] = valIn;
		}
		public void AddStart(int valIn)
		{
			int[] _tempArr = new int[arr.Length + 1];
			_tempArr[0] = valIn;
			for (int i = 1; i < _tempArr.Length; i++)
			{
				_tempArr[i] = arr[i - 1];
			}
			arr = _tempArr;
		}
		public void AddEnd(int valIn)
		{
			int[] _tempArr = new int[arr.Length + 1];
			_tempArr[_tempArr.Length - 1] = valIn;
			for (int i = 0; i < arr.Length; i++)
			{
				_tempArr[i] = arr[i];
			}
			arr = _tempArr;
		}
		public void AddPos(int posIn, int valIn)
		{
			if (posIn < 0 || posIn > arr.Length - 1)
				throw new IndexOutOfRangeException();
			int[] _tempArr = new int[arr.Length + 1];
			_tempArr[posIn] = valIn;
			for (int i = 0; i < _tempArr.Length; i++)
			{
				if (i < posIn)
					_tempArr[i] = arr[i];
				else if (i > posIn)
					_tempArr[i] = arr[i - 1];
			}
			arr = _tempArr;
		}
		public int DelStart()
		{
			if (arr.Length == 0)
				throw new IndexOutOfRangeException();
			int _deleted = arr[0];
			int[] _tempArr = new int[arr.Length - 1];
			for (int i = 0; i < _tempArr.Length; i++)
			{
				_tempArr[i] = arr[i + 1];
			}
			arr = _tempArr;
			return _deleted;
		}
		public int DelEnd()
		{
			if (arr.Length == 0)
				throw new IndexOutOfRangeException();
			int _deleted = arr[arr.Length - 1];
			int[] _tempArr = new int[arr.Length - 1];
			for (int i = 0; i < _tempArr.Length; i++)
			{
				_tempArr[i] = arr[i];
			}
			arr = _tempArr;
			return _deleted;
		}
		public int DelPos(int posIn)
		{
			if (posIn < 0 || posIn > arr.Length - 1)
				throw new IndexOutOfRangeException();
			int _deleted = arr[posIn];
			int[] _tempArr = new int[arr.Length - 1];
			for (int i = 0; i < _tempArr.Length; i++)
			{
				if (i < posIn)
					_tempArr[i] = arr[i];
				else if (i >= posIn)
					_tempArr[i] = arr[i + 1];
			}
			arr = _tempArr;
			return _deleted;
		}
		public void HalfRevers()
		{
			int d = (arr.Length % 2 == 0) ? 0 : 1;
			for (int i = 0; i < arr.Length / 2; i++)
			{
				int tmp = arr[i];
				arr[i] = arr[arr.Length / 2 + i + d];
				arr[arr.Length / 2 + i + d] = tmp;
			}
		}
		public int[] ToArray()
		{
			int[] _tempArr = new int[arr.Length];
			for (int i = 0; i < _tempArr.Length; i++)
			{
				_tempArr[i] = arr[i];
			}
			return _tempArr;
		}
		public override string ToString()
		{
			string _str = "";
			for (int i = 0; i < arr.Length; i++)
			{
				_str += arr[i] + (i != arr.Length - 1 ? ", " : "");
			}
			return _str;
		}
		public void Sort()
		{
			if (arr.Length > 1)
				QuickSort(arr, 0, arr.Length - 1);
		}
		private void QuickSort(int[] _arr, int l, int r)
		{
			int _temp;
			int _pivot = _arr[l + (r - l) / 2];
			int i = l;
			int j = r;

			while (i <= j)
			{
				while (_arr[i] < _pivot) i++;
				while (_arr[j] > _pivot) j--;

				if (i <= j)
				{
					_temp = _arr[i];
					_arr[i] = _arr[j];
					_arr[j] = _temp;
					i++;
					j--;
				}
			}
			if (i < r)
			{
				QuickSort(_arr, i, r);
			}
			if (l < j)
			{
				QuickSort(_arr, l, j);
			}
		}
		public void Revers()
		{
			if (arr.Length > 0)
			{
				int temp;
				int i = 0;
				int j = arr.Length - 1;
				while (i < j)
				{
					temp = arr[i];
					arr[i] = arr[j];
					arr[j] = temp;
					i++;
					j--;
				}
			}
		}
		public int Min()
		{
			if (arr.Length == 0)
				throw new InvalidOperationException();
			int _min = arr[0];
			for (int i = 1; i < arr.Length; i++)
			{
				if (_min > arr[i])
				{
					_min = arr[i];
				}
			}
			return _min;
		}
		public int Max()
		{
			if (arr.Length == 0)
				throw new InvalidOperationException();
			int _max = arr[0];
			for (int i = 1; i < arr.Length; i++)
			{
				if (_max < arr[i])
				{
					_max = arr[i];
				}
			}
			return _max;
		}
		public int MinIndex()
		{
			if (arr.Length == 0)
				throw new InvalidOperationException();
			return Array.IndexOf(arr, Min());
		}
		public int MaxIndex()
		{
			if (arr.Length == 0)
				throw new InvalidOperationException();
			return Array.IndexOf(arr, Max());
		}
	}
}
