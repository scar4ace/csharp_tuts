﻿using System;

namespace List
{
	public class AList1 : IList
	{
		private int[] arr = { };
		private int index = 0;
		public AList1()
		{

		}
		public AList1(int[] arrIn)
		{
			Init(arrIn);
		}
		public void Init(int[] arrIn)
		{
			if (arrIn == null || arrIn.Length == 0)
				arrIn = new int[0];
			arr = new int[arrIn.Length];
			for (int i = 0; i < arrIn.Length; i++)
			{
				arr[i] = arrIn[i];
			}
			index = arrIn.Length;
		}
		public override string ToString()
		{
			string _str = "";
			for (int i = 0; i < index; i++)
			{
				_str += arr[i] + (i + 1 != index ? ", " : "");
			}
			return _str;
		}
		public void Set(int posIn, int valIn)
		{
			if (posIn < 0 || posIn >= index)
				throw new IndexOutOfRangeException();
			arr[posIn] = valIn;
		}
		public int Get(int posIn)
		{
			if (posIn < 0 || posIn >= index)
				throw new IndexOutOfRangeException();
			return arr[posIn];
		}
		public void Clear()
		{
			for (int i = 0; i < index; i++)
			{
				arr[i] = 0;
			}
			index = 0;
		}
		public int Size()
		{
			return index;
		}
		public void AddStart(int valIn)
		{
			CheckSize();
			for (int i = index - 1; i > 0; i--)
			{
				arr[i] = arr[i - 1];
			}
			arr[0] = valIn;
			index++;
		}
		private void CheckSize()
		{
			if (index == arr.Length)
			{
				int[] _tempArr;
				if (arr.Length >= 3)
					_tempArr = new int[arr.Length + arr.Length / 3];
				else
					_tempArr = new int[arr.Length + 1];
				for (int i = 1; i < arr.Length; i++)
				{
					_tempArr[i] = arr[i];
				}
				arr = _tempArr;
			}
		}
		public void AddEnd(int valIn)
		{
			CheckSize();
			arr[index] = valIn;
			index++;
		}
		public void AddPos(int posIn, int valIn)
		{
			if (posIn < 0 || posIn >= index)
				throw new IndexOutOfRangeException();
			CheckSize();
			int[] tempArr = new int[index + 1];
			tempArr[posIn] = valIn;
			for (int i = 0; i < tempArr.Length; i++)
			{
				if (i < posIn)
					tempArr[i] = arr[i];
				else if (i > posIn)
					tempArr[i] = arr[i - 1];
			}
			arr = tempArr;
			index++;
		}
		public int DelStart()
		{
			if (index <= 0)
				throw new IndexOutOfRangeException();
			int _deleted = arr[0];
			int[] _tempArr = new int[arr.Length - 1];
			for (int i = 0; i < _tempArr.Length; i++)
			{
				_tempArr[i] = arr[i + 1];
			}
			arr = _tempArr;
			index--;
			return _deleted;
		}
		public int DelEnd()
		{
			if (index <= 0)
				throw new IndexOutOfRangeException();
			int _deleted = arr[index - 1];
			int[] _tempArr = new int[arr.Length - 1];
			for (int i = 0; i < _tempArr.Length; i++)
			{
				_tempArr[i] = arr[i];
			}
			arr = _tempArr;
			index--;
			return _deleted;
		}
		public int DelPos(int posIn)
		{
			if (posIn < 0 || posIn >= index)
				throw new IndexOutOfRangeException();
			int _deleted = arr[posIn];
			int[] _tempArr = new int[arr.Length - 1];
			for (int i = 0; i < _tempArr.Length; i++)
			{
				if (i < posIn)
				{
					_tempArr[i] = arr[i];
				}
				else if (i >= posIn)
				{
					_tempArr[i] = arr[i + 1];
				}
			}
			arr = _tempArr;
			index--;
			return _deleted;
		}
		public void HalfRevers()
		{
			if (index > 1)
			{
				int d = (index % 2 == 0) ? 0 : 1;
				for (int i = 0; i < index / 2; i++)
				{
					int tmp = arr[i];
					arr[i] = arr[index / 2 + i + d];
					arr[index / 2 + i + d] = tmp;
				}
			}
		}
		public int[] ToArray()
		{
			int[] tempArr = new int[index];
			for (int i = 0; i < tempArr.Length; i++)
			{
				tempArr[i] = arr[i];
			}
			return tempArr;
		}
		public void Sort()
		{
			if (index > 1)
				QuickSort(arr, 0, index - 1);
		}
		private void QuickSort(int[] _arr, int l, int r)
		{
			int _temp;
			int _pivot = _arr[l + (r - l) / 2];
			int i = l;
			int j = r;
			while (i <= j)
			{
				while (_arr[i] < _pivot) i++;
				while (_arr[j] > _pivot) j--;

				if (i <= j)
				{
					_temp = _arr[i];
					_arr[i] = _arr[j];
					_arr[j] = _temp;
					i++;
					j--;
				}
			}
			if (i < r)
			{
				QuickSort(_arr, i, r);
			}
			if (l < j)
			{
				QuickSort(_arr, l, j);
			}
		}
		public void Revers()
		{
			if (index > 1)
			{
				int temp;
				int i = 0;
				int j = index - 1;
				while (i < j)
				{
					temp = arr[i];
					arr[i] = arr[j];
					arr[j] = temp;
					i++;
					j--;
				}
			}
		}
		public int Min()
		{
			if (index == 0)
				throw new InvalidOperationException();
			int _min = arr[0];
			for (int i = 1; i < index; i++)
			{
				if (_min > arr[i])
					_min = arr[i];
			}
			return _min;
		}
		public int Max()
		{
			if (index == 0)
				throw new InvalidOperationException();
			int _max = arr[0];
			for (int i = 1; i < index; i++)
			{
				if (_max < arr[i])
					_max = arr[i];
			}
			return _max;
		}
		public int MinIndex()
		{
			if (index == 0)
				throw new InvalidOperationException();
			return Array.IndexOf(arr, Min());
		}
		public int MaxIndex()
		{
			if (index == 0)
				throw new InvalidOperationException();
			return Array.IndexOf(arr, Max());
		}
	}
}
