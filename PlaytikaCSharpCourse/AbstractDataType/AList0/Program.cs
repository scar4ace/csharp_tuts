﻿using System;

namespace List
{
	class Program
	{
		static void Main(string[] args)
		{
			//int[] arr = { 2, 1, 0, 3, 1 };
			//int[] arr = { 1, 2/*, 3, 4, 5*/ };
			int[] arr = { -5, 4, 3, 2, 1 };

			//int[] arr = new int[1];
			//int[] arr = { };
			//AList0 list = new AList0();
			//AList1 list = new AList1();
			//AList2 list = new AList2();
			LList1F list = new LList1F();
			//LList1 list = new LList1();
			list.Init(arr);
			//list.AddPos(0, 7);
			//list.AddPos(1, 7);
			//list.Size();
			//list.Set(0, 1);
			//list.Clear();
			//list.AddStart(-20);
			//list.AddStart(5);
			//list.AddEnd(7);
			//list.DelStart();
			//list.DelEnd();
			//list.DelPos(0);
			list.Sort();
			//list.Revers();
			//list.HalfRevers();

			//int[] arrFromList = { };
			//arrFromList = list.ToArray();
			//for (int i = 0; i < arrFromList.Length; i++)
			//{
			//	Console.Write(arrFromList[i]+" ");
			//}
			//Console.WriteLine(list.ToString());
			//Console.WriteLine(list.Size());
			//Console.WriteLine(list.DelEnd());

			Console.WriteLine(list.ToString());
			//Console.WriteLine(list.Get(list.Size() - 1));

			//Console.WriteLine(list.Get(2));
			//Console.WriteLine(list.DelStart());
			//Console.WriteLine(list.Min());
			//Console.WriteLine(list.Min() + " " + list.MinIndex());
			//Console.WriteLine(list.Max() + " " + list.MaxIndex());
			//Console.WriteLine(list.DelPos(6));
			//Console.WriteLine(/*"\n"+*/list);
			Console.ReadKey();
		}
	}
}
