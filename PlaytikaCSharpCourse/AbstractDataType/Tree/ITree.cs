﻿namespace Tree
{
	interface ITree
	{
		void Init(int[] arr);
		int Size();
		int[] ToArray();
	}
}
