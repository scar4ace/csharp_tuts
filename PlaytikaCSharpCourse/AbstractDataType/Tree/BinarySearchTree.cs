﻿using System;

namespace Tree
{
	public class BinarySearchTree : ITree
	{
		public class Node
		{
			public int data;
			public Node left;
			public Node right;

			public Node(int data)
			{
				this.data = data;
			}
		}
		public Node root = null;
		public void Init(int[] arr)
		{
			if (arr.Length <= 0)
				return;
			// Root init
			Node tmpNode = new Node(arr[0]);
			root = tmpNode;
			for (int i = 1; i < arr.Length; i++)
			{
				InitNode(root, new Node(arr[i]));
			}
		}
		public int Size()
		{
			int size = 0;
			return TreeSize(root, ref size);
		}
		private int TreeSize(Node r, ref int size)
		{
			if (r != null)
			{
				TreeSize(r.left, ref size);
				size++;
				TreeSize(r.right, ref size);
			}
			return size;
		}
		public int[] ToArray()
		{
			int[] retArr = new int[0];
			return InOrderTraversal(root, ref retArr);
		}
		public int[] InOrderTraversal(Node r, ref int[] retArr)
		{
			if (r != null)
			{
				InOrderTraversal(r.left, ref retArr);
				Array.Resize(ref retArr, retArr.Length + 1);
				retArr[retArr.Length - 1] = r.data;
				InOrderTraversal(r.right, ref retArr);
			}
			return retArr;
		}

		private void InitNode(Node r, Node x)
		{
			if (x.data < r.data)
			{
				if (r.left != null)
					InitNode(r.left, x);
				else
					r.left = x;
			}
			else
			{
				if (r.right != null)
					InitNode(r.right, x);
				else
					r.right = x;
			}
		}
	}
}
