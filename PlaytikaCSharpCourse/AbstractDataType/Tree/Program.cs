﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tree
{
	class Program
	{
		static void Main(string[] args)
		{
			int[] arr = { 50, 25, 11, 77, 4, 21, 30, 99, 106, 64 };
			BinarySearchTree bsTree = new BinarySearchTree();
			bsTree.Init(arr);

			Console.WriteLine(bsTree.Size()+" elements");

			int[] testArr = bsTree.ToArray();
			for (int i = 0; i < testArr.Length; i++)
			{
				Console.Write(testArr[i].ToString() + " ");
			}

			Console.ReadKey();
		}

		private static void InOrderTraversalOfBSTree(BinarySearchTree.Node root)
		{
			if (root != null)
			{
				InOrderTraversalOfBSTree(root.left);
				Console.Write(root.data + " ");
				InOrderTraversalOfBSTree(root.right);
			}
		}
	}
}
