﻿using NUnit.Framework;
using List;
using System;

namespace List_Tests
{
	//[TestFixture(typeof(AList0))]
	//[TestFixture(typeof(AList1))]
	//[TestFixture(typeof(AList2))]
	//[TestFixture(typeof(LList1))]
	//[TestFixture (typeof(LList1F))]
	public class IList_Tests<TList> where TList : IList, new()
	{
		TList list;
		[SetUp]
		public void CreateList()
		{
			list = new TList();
		}
		//Init(), ToString() Tests
		[TestCase(null, "")]
		[TestCase(new int[] { }, "")]
		[TestCase(new int[] { 1 }, "1")]
		[TestCase(new int[] { 2, 1 }, "2, 1")]
		[TestCase(new int[] { 5, 4, 3, 2, 1 }, "5, 4, 3, 2, 1")]
		public void ToString_Test(int[] ini, String exp)
		{
			list.Init(ini);
			Assert.AreEqual(exp, list.ToString());
		}
		// ToArray() Tests
		[TestCase(null, new int[0])]
		[TestCase(new int[] { }, new int[0])]
		[TestCase(new int[] { 1 }, new int[] { 1 })]
		[TestCase(new int[] { 2, 1 }, new int[] { 2, 1 })]
		[TestCase(new int[] { 5, 4, 3, 2, 1 }, new int[] { 5, 4, 3, 2, 1 })]
		public void ToArray_Test(int[] ini, int[] exp)
		{
			list.Init(ini);
			Assert.AreEqual(exp, list.ToArray());
		}
		// Clear() Tests
		[TestCase(null)]
		[TestCase(new int[] { })]
		[TestCase(new int[] { 1 })]
		[TestCase(new int[] { 2, 1 })]
		[TestCase(new int[] { 5, 4, 3, 2, 1 })]
		public void Clear_Test(int[] ini)
		{
			list.Init(ini);
			list.Clear();
			Assert.AreEqual(new int[0], list.ToArray());
		}
		// Size() Tests
		[TestCase(null, 0)]
		[TestCase(new int[] { }, 0)]
		[TestCase(new int[] { 1 }, 1)]
		[TestCase(new int[] { 2, 1 }, 2)]
		[TestCase(new int[] { 5, 4, 3, 2, 1 }, 5)]
		public void Size_Test(int[] ini, int exp)
		{
			list.Init(ini);
			Assert.AreEqual(exp, list.Size());
		}
		// Get() Tests
		// *exceptions
		[TestCase(null, -1)]
		[TestCase(null, 0)]
		[TestCase(new int[] { }, -1)]
		[TestCase(new int[] { }, 0)]
		[TestCase(new int[] { 1 }, -1)]
		[TestCase(new int[] { 1 }, 1)]
		[TestCase(new int[] { 2, 1 }, -1)]
		[TestCase(new int[] { 2, 1 }, 2)]
		[TestCase(new int[] { 5, 4, 3, 2, 1 }, -1)]
		[TestCase(new int[] { 5, 4, 3, 2, 1 }, 5)]
		public void Get_Test_IndexOutOfRangeException(int[] ini, int i)
		{
			list.Init(ini);
			Assert.Throws<IndexOutOfRangeException>(() => list.Get(i));
		}
		// *regular
		[TestCase(new int[] { 1 }, 0, ExpectedResult = 1)]
		[TestCase(new int[] { 2, 1 }, 0, ExpectedResult = 2)]
		[TestCase(new int[] { 2, 1 }, 1, ExpectedResult = 1)]
		[TestCase(new int[] { 5, 4, 3, 2, 1 }, 0, ExpectedResult = 5)]
		[TestCase(new int[] { 5, 4, 3, 2, 1 }, 2, ExpectedResult = 3)]
		[TestCase(new int[] { 5, 4, 3, 2, 1 }, 4, ExpectedResult = 1)]
		public int Get_Test(int[] ini, int i)
		{
			list.Init(ini);
			return list.Get(i);
		}
		// Set() Tests
		// *exceptions
		[TestCase(null)]
		[TestCase(new int[] { })]
		[TestCase(new int[] { 1 })]
		[TestCase(new int[] { 2, 1 })]
		[TestCase(new int[] { 5, 4, 3, 2, 1 })]
		public void Set_Test_IndexLeftOut_IndexOutOfRangeException(int[] ini)
		{
			list.Init(ini);
			int i = -1;
			Assert.Throws<IndexOutOfRangeException>(() => list.Set(i, 0));
		}
		[TestCase(null)]
		[TestCase(new int[] { })]
		[TestCase(new int[] { 1 })]
		[TestCase(new int[] { 2, 1 })]
		[TestCase(new int[] { 5, 4, 3, 2, 1 })]
		public void Set_Test_IndexRightOut_IndexOutOfRangeException(int[] ini)
		{
			list.Init(ini);
			int i = list.Size();
			Assert.Throws<IndexOutOfRangeException>(() => list.Set(i, 0));
		}
		[TestCase(null)]
		[TestCase(new int[] { })]
		public void Set_Test_IndexLeft_IndexOutOfRangeException(int[] ini)
		{
			list.Init(ini);
			int i = 0;
			Assert.Throws<IndexOutOfRangeException>(() => list.Set(i, 0));
		}
		[TestCase(null)]
		[TestCase(new int[] { })]
		public void Set_Test_IndexMedium_IndexOutOfRangeException(int[] ini)
		{
			list.Init(ini);
			int i = list.Size() / 2;
			Assert.Throws<IndexOutOfRangeException>(() => list.Set(i, 0));
		}
		[TestCase(null)]
		[TestCase(new int[] { })]
		public void Set_Test_IndexRight_IndexOutOfRangeException(int[] ini)
		{
			list.Init(ini);
			int i = list.Size() - 1;
			Assert.Throws<IndexOutOfRangeException>(() => list.Set(i, 0));
		}
		// *regular
		[TestCase(new int[] { 1 })]
		[TestCase(new int[] { 2, 1 })]
		[TestCase(new int[] { 5, 4, 3, 2, 1 })]
		public void Set_Test_IndexLeft(int[] ini)
		{
			list.Init(ini);
			int i = 0;
			list.Set(i, 0);
			Assert.AreEqual(0, list.Get(i));
		}
		[TestCase(new int[] { 1 })]
		[TestCase(new int[] { 2, 1 })]
		[TestCase(new int[] { 5, 4, 3, 2, 1 })]
		public void Set_Test_IndexMedium(int[] ini)
		{
			list.Init(ini);
			int i = list.Size() / 2;
			list.Set(i, 0);
			Assert.AreEqual(0, list.Get(i));
		}
		[TestCase(new int[] { 1 })]
		[TestCase(new int[] { 2, 1 })]
		[TestCase(new int[] { 5, 4, 3, 2, 1 })]
		public void Set_Test_IndexRight(int[] ini)
		{
			list.Init(ini);
			int i = list.Size() - 1;
			list.Set(i, 0);
			Assert.AreEqual(0, list.Get(i));
		}
		// AddEnd() Tests
		[TestCase(null)]
		[TestCase(new int[] { })]
		[TestCase(new int[] { 1 })]
		[TestCase(new int[] { 2, 1 })]
		[TestCase(new int[] { 5, 4, 3, 2, 1 })]
		public void AddEnd_Test(int[] ini)
		{
			list.Init(ini);
			list.AddEnd(0);
			Assert.AreEqual(0, list.Get(list.Size() - 1));
		}
		// AddStart() Tests
		[TestCase(null)]
		[TestCase(new int[] { })]
		[TestCase(new int[] { 1 })]
		[TestCase(new int[] { 2, 1 })]
		[TestCase(new int[] { 5, 4, 3, 2, 1 })]
		public void AddStart_Test(int[] ini)
		{
			list.Init(ini);
			list.AddStart(0);
			Assert.AreEqual(0, list.Get(0));
		}
		// AddPos() Tests
		// *exceptions
		[TestCase(null)]
		[TestCase(new int[] { })]
		[TestCase(new int[] { 1 })]
		[TestCase(new int[] { 2, 1 })]
		[TestCase(new int[] { 5, 4, 3, 2, 1 })]
		public void AddPos_Test_IndexLeftOut_IndexOutOfRangeException(int[] ini)
		{
			list.Init(ini);
			int i = -1;
			Assert.Throws<IndexOutOfRangeException>(() => list.AddPos(i, 0));
		}
		[TestCase(null)]
		[TestCase(new int[] { })]
		[TestCase(new int[] { 1 })]
		[TestCase(new int[] { 2, 1 })]
		[TestCase(new int[] { 5, 4, 3, 2, 1 })]
		public void AddPos_Test_IndexRightOut_IndexOutOfRangeException(int[] ini)
		{
			list.Init(ini);
			int i = list.Size();
			Assert.Throws<IndexOutOfRangeException>(() => list.AddPos(i, 0));
		}
		[TestCase(null)]
		[TestCase(new int[] { })]
		public void AddPos_Test_IndexLeft_IndexOutOfRangeException(int[] ini)
		{
			list.Init(ini);
			int i = 0;
			Assert.Throws<IndexOutOfRangeException>(() => list.AddPos(i, 0));
		}
		[TestCase(null)]
		[TestCase(new int[] { })]
		public void AddPos_Test_IndexMiddle_IndexOutOfRangeException(int[] ini)
		{
			list.Init(ini);
			int i = list.Size() / 2;
			Assert.Throws<IndexOutOfRangeException>(() => list.AddPos(i, 0));
		}
		[TestCase(null)]
		[TestCase(new int[] { })]
		public void AddPos_Test_IndexRight_IndexOutOfRangeException(int[] ini)
		{
			list.Init(ini);
			int i = list.Size() - 1;
			Assert.Throws<IndexOutOfRangeException>(() => list.AddPos(i, 0));
		}
		// *regular
		[TestCase(new int[] { 1 })]
		[TestCase(new int[] { 2, 1 })]
		[TestCase(new int[] { 5, 4, 3, 2, 1 })]
		public void AddPos_IndexLeft(int[] ini)
		{
			list.Init(ini);
			int i = 0;
			list.AddPos(i, 0);
			Assert.AreEqual(0, list.Get(i));
		}
		[TestCase(new int[] { 1 })]
		[TestCase(new int[] { 2, 1 })]
		[TestCase(new int[] { 5, 4, 3, 2, 1 })]
		public void AddPos_IndexMedium(int[] ini)
		{
			list.Init(ini);
			int i = list.Size() / 2;
			list.AddPos(i, 0);
			Assert.AreEqual(0, list.Get(i));
		}
		[TestCase(new int[] { 1 })]
		[TestCase(new int[] { 2, 1 })]
		[TestCase(new int[] { 5, 4, 3, 2, 1 })]
		public void AddPos_IndexRight(int[] ini)
		{
			list.Init(ini);
			int i = list.Size() - 1;
			list.AddPos(i, 0);
			Assert.AreEqual(0, list.Get(i));
		}
		// DelStart() Tests
		// *exceptions
		[TestCase(null)]
		[TestCase(new int[] { })]
		public void DelStart_Test_IndexOutOfRangeException(int[] ini)
		{
			list.Init(ini);
			Assert.Throws<IndexOutOfRangeException>(() => list.DelStart());
		}
		// *regular
		[TestCase(new int[] { 1 }, 1)]
		[TestCase(new int[] { 2, 1 }, 2)]
		[TestCase(new int[] { 5, 4, 3, 2, 1 }, 5)]
		public void DelStart_Test_ValReturned(int[] ini, int exp)
		{
			list.Init(ini);
			Assert.AreEqual(exp, list.DelStart());
		}
		[TestCase(new int[] { 1 })]
		[TestCase(new int[] { 2, 1 })]
		[TestCase(new int[] { 5, 4, 3, 2, 1 })]
		public void DelStart_Test_DecrementedLengthReturned(int[] ini)
		{
			list.Init(ini);
			int l = list.Size();
			list.DelStart();
			Assert.AreEqual(l - 1, list.Size());
		}
		// DelEnd() Tests
		// *exceptions
		[TestCase(null)]
		[TestCase(new int[] { })]
		public void DelEnd_Test_IndexOutOfRangeException(int[] ini)
		{
			list.Init(ini);
			Assert.Throws<IndexOutOfRangeException>(() => list.DelEnd());
		}
		// *regular
		//[TestCase(new int[] { 1 })]
		[TestCase(new int[] { 2, 1 })]
		[TestCase(new int[] { 5, 4, 3, 2, 1 })]
		public void DelEnd_Test_ValReturned(int[] ini)
		{
			list.Init(ini);
			Assert.AreEqual(1, list.DelEnd());
		}
		[TestCase(new int[] { 1 })]
		[TestCase(new int[] { 2, 1 })]
		[TestCase(new int[] { 5, 4, 3, 2, 1 })]
		public void DelEnd_Test_DecrementedLengthReturned(int[] ini)
		{
			list.Init(ini);
			int l = list.Size();
			list.DelEnd();
			Assert.AreEqual(l - 1, list.Size());
		}
		// DelPos() Tests
		// *exceptions
		[TestCase(null)]
		[TestCase(new int[] { })]
		[TestCase(new int[] { 1 })]
		[TestCase(new int[] { 2, 1 })]
		[TestCase(new int[] { 5, 4, 3, 2, 1 })]
		public void DelPos_IndexLeftOut_IndexOutOfRangeException(int[] ini)
		{
			list.Init(ini);
			int i = -1;
			Assert.Throws<IndexOutOfRangeException>(() => list.DelPos(i));
		}
		[TestCase(null)]
		[TestCase(new int[] { })]
		[TestCase(new int[] { 1 })]
		[TestCase(new int[] { 2, 1 })]
		[TestCase(new int[] { 5, 4, 3, 2, 1 })]
		public void DelPos_IndexRightOut_IndexOutOfRangeException(int[] ini)
		{
			list.Init(ini);
			int i = list.Size();
			Assert.Throws<IndexOutOfRangeException>(() => list.DelPos(i));
		}
		[TestCase(null)]
		[TestCase(new int[] { })]
		public void DelPos_IndexLeft_IndexOutOfRangeException(int[] ini)
		{
			list.Init(ini);
			int i = 0;
			Assert.Throws<IndexOutOfRangeException>(() => list.DelPos(i));
		}
		[TestCase(null)]
		[TestCase(new int[] { })]
		public void DelPos_IndexMiddle_IndexOutOfRangeException(int[] ini)
		{
			list.Init(ini);
			int i = list.Size() / 2;
			Assert.Throws<IndexOutOfRangeException>(() => list.DelPos(i));
		}
		[TestCase(null)]
		[TestCase(new int[] { })]
		public void DelPos_IndexRight_IndexOutOfRangeException(int[] ini)
		{
			list.Init(ini);
			int i = list.Size() - 1;
			Assert.Throws<IndexOutOfRangeException>(() => list.DelPos(i));
		}
		// *regular
		[TestCase(new int[] { 1 }, 1)]
		[TestCase(new int[] { 2, 1 }, 2)]
		[TestCase(new int[] { 5, 4, 3, 2, 1 }, 5)]
		public void DelPos_Test_IndexLeft_ValReturned(int[] ini, int exp)
		{
			list.Init(ini);
			int i = 0;
			Assert.AreEqual(exp, list.DelPos(i));
		}
		[TestCase(new int[] { 1 }, 1)]
		[TestCase(new int[] { 2, 1 }, 1)]
		[TestCase(new int[] { 5, 4, 3, 2, 1 }, 3)]
		public void DelPos_Test_IndexMiddle_ValReturned(int[] ini, int exp)
		{
			list.Init(ini);
			int i = list.Size() / 2;
			Assert.AreEqual(exp, list.DelPos(i));
		}
		[TestCase(new int[] { 1 })]
		[TestCase(new int[] { 2, 1 })]
		[TestCase(new int[] { 5, 4, 3, 2, 1 })]
		public void DelPos_Test_IndexRight_ValReturned(int[] ini)
		{
			list.Init(ini);
			int i = list.Size() - 1;
			Assert.AreEqual(1, list.DelPos(i));
		}
		[TestCase(new int[] { 1 })]
		[TestCase(new int[] { 2, 1 })]
		[TestCase(new int[] { 5, 4, 3, 2, 1 })]
		public void DelPos_Test_IndexLeft_DecrementedLengthReturned(int[] ini)
		{
			list.Init(ini);
			int l = list.Size();
			list.DelPos(0);
			Assert.AreEqual(l - 1, list.Size());
		}
		[TestCase(new int[] { 1 })]
		[TestCase(new int[] { 2, 1 })]
		[TestCase(new int[] { 5, 4, 3, 2, 1 })]
		public void DelPos_Test_IndexMiddle_DecrementedLengthReturned(int[] ini)
		{
			list.Init(ini);
			int l = list.Size();
			list.DelPos(list.Size() / 2);
			Assert.AreEqual(l - 1, list.Size());
		}
		[TestCase(new int[] { 1 })]
		[TestCase(new int[] { 2, 1 })]
		[TestCase(new int[] { 5, 4, 3, 2, 1 })]
		public void DelPos_Test_IndexRight_DecrementedLengthReturned(int[] ini)
		{
			list.Init(ini);
			int l = list.Size();
			list.DelPos(list.Size() - 1);
			Assert.AreEqual(l - 1, list.Size());
		}
		// Sort() Tests
		[TestCase(null, "")]
		[TestCase(new int[] { }, "")]
		[TestCase(new int[] { 1 }, "1")]
		[TestCase(new int[] { 2, 1 }, "1, 2")]
		[TestCase(new int[] { 5, 4, 3, 2, 1 }, "1, 2, 3, 4, 5")]
		public void Sort_Test(int[] ini, String exp)
		{
			list.Init(ini);
			list.Sort();
			Assert.AreEqual(exp, list.ToString());
		}
		// Revers() Tests
		[TestCase(null, "")]
		[TestCase(new int[] { }, "")]
		[TestCase(new int[] { 1 }, "1")]
		[TestCase(new int[] { 2, 1 }, "1, 2")]
		[TestCase(new int[] { 5, 4, 3, 2, 1 }, "1, 2, 3, 4, 5")]
		public void Revers_Test(int[] ini, String exp)
		{
			list.Init(ini);
			list.Revers();
			Assert.AreEqual(exp, list.ToString());
		}
		// HalfRevers() Tests
		[TestCase(null, "")]
		[TestCase(new int[] { }, "")]
		[TestCase(new int[] { 1 }, "1")]
		[TestCase(new int[] { 2, 1 }, "1, 2")]
		[TestCase(new int[] { 5, 4, 3, 2, 1 }, "2, 1, 3, 5, 4")]
		public void HalfRevers_Test(int[] ini, String exp)
		{
			list.Init(ini);
			list.HalfRevers();
			Assert.AreEqual(exp, list.ToString());
		}
		// Min() Tests
		[TestCase(null)]
		[TestCase(new int[] { })]
		public void Min_Test_InvalidOperationException(int[] ini)
		{
			list.Init(ini);
			Assert.Throws<InvalidOperationException>(() => list.Min());
		}
		[TestCase(new int[] { 1 })]
		[TestCase(new int[] { 2, 1 })]
		[TestCase(new int[] { 5, 4, 3, 2, 1 })]
		public void Min_Test(int[] ini)
		{
			list.Init(ini);
			Assert.AreEqual(1, list.Min());
		}

		// Max() Tests
		[TestCase(null)]
		[TestCase(new int[] { })]
		public void Max_Test_InvalidOperationException(int[] ini)
		{
			list.Init(ini);
			Assert.Throws<InvalidOperationException>(() => list.Max());
		}
		[TestCase(new int[] { 1 }, 1)]
		[TestCase(new int[] { 2, 1 }, 2)]
		[TestCase(new int[] { 5, 4, 3, 2, 1 }, 5)]
		public void Max_Test(int[] ini, int exp)
		{
			list.Init(ini);
			Assert.AreEqual(exp, list.Max());
		}

		// MinIndex() Tests
		[TestCase(null)]
		[TestCase(new int[] { })]
		public void MinIndex_Test_InvalidOperationException(int[] ini)
		{
			list.Init(ini);
			Assert.Throws<InvalidOperationException>(() => list.MinIndex());
		}
		[TestCase(new int[] { 1 }, 0)]
		[TestCase(new int[] { 2, 1 }, 1)]
		[TestCase(new int[] { 5, 4, 3, 2, 1 }, 4)]
		public void MinIndex_Test(int[] ini, int exp)
		{
			list.Init(ini);
			Assert.AreEqual(exp, list.MinIndex());
		}

		// MaxIndex() Tests
		[TestCase(null)]
		[TestCase(new int[] { })]
		public void MaxIndex_Test_InvalidOperationException(int[] ini)
		{
			list.Init(ini);
			Assert.Throws<InvalidOperationException>(() => list.MaxIndex());
		}
		[TestCase(new int[] { 1 })]
		[TestCase(new int[] { 2, 1 })]
		[TestCase(new int[] { 5, 4, 3, 2, 1 })]
		public void MaxIndex_Test(int[] ini)
		{
			list.Init(ini);
			Assert.AreEqual(0, list.MaxIndex());
		}
	}
}
