﻿using NUnit.Framework;
using List;
using System;

namespace List_Tests
{
	[TestFixture]
	public class AList0_Tests
	{
		AList0 list = new AList0();

		// Collections Test Cases
		int[] arrNull;// null
		int[] arr0 = { };// empty
		int[] arr1 = { 1 };// 1 value
		int[] arr2 = { 2, 1 };// 2 values
		int[] arrMany = { 5, 4, 3, 2, 1 };// many values from 5 to 7)
		//int[,] arrTwoDim = { { 1 } };// Multi dimensional [1,1]

		//Init(), ToString() Tests
		[Test]
		public void Init_arrNull_NullReferenceException()
		{
			Assert.Throws<NullReferenceException>(() => new AList0(arrNull));
		}
		[Test]
		public void Init_arr0_emptyString()
		{
			list.Init(arr0);
			Assert.AreEqual("", list.ToString());
		}
		[Test]
		public void Init_arr1_1String()
		{
			list.Init(arr1);
			Assert.AreEqual("1", list.ToString());
		}
		[Test]
		public void Init_arr2_12String()
		{
			list.Init(arr2);
			Assert.AreEqual("2, 1", list.ToString());
		}
		[Test]
		public void Init_arrMany_7654321String()
		{
			list.Init(arrMany);
			Assert.AreEqual("5, 4, 3, 2, 1", list.ToString());
		}
		// ToArray() Tests
		[Test]
		public void ToArray_arr0_ArrayReturned()
		{
			list.Init(arr0);
			Assert.AreEqual(arr0, list.ToArray());
		}
		[Test]
		public void ToArray_arr1_ArrayReturned()
		{
			list.Init(arr1);
			Assert.AreEqual(arr1, list.ToArray());
		}
		[Test]
		public void ToArray_arr2_ArrayReturned()
		{
			list.Init(arr2);
			Assert.AreEqual(arr2, list.ToArray());
		}
		[Test]
		public void ToArray_arrMany_ArrayReturned()
		{
			list.Init(arrMany);
			Assert.AreEqual(arrMany, list.ToArray());
		}

		// Clear() Tests
		[Test]
		public void Clear_arr0_EmptyArr()
		{
			list.Init(arr0);
			list.Clear();
			Assert.AreEqual(arr0, list.ToArray());
		}
		[Test]
		public void Clear_arr1_EmptyArr()
		{
			list.Init(arr1);
			list.Clear();
			Assert.AreEqual(arr0, list.ToArray());
		}
		[Test]
		public void Clear_arr2_EmptyArr()
		{
			list.Init(arr2);
			list.Clear();
			Assert.AreEqual(arr0, list.ToArray());
		}
		[Test]
		public void Clear_arrMany_EmptyArr()
		{
			list.Init(arrMany);
			list.Clear();
			Assert.AreEqual(arr0, list.ToArray());
		}
		// Size() Tests
		[Test]
		public void Size_arr0_0Returned()
		{
			list.Init(arr0);
			Assert.AreEqual(0, list.Size());
		}
		[Test]
		public void Size_arr1_1Returned()
		{
			list.Init(arr1);
			Assert.AreEqual(1, list.Size());
		}
		[Test]
		public void Size_arr2_2Returned()
		{
			list.Init(arr2);
			Assert.AreEqual(2, list.Size());
		}
		[Test]
		public void Size_arrMany_ManyReturned()
		{
			list.Init(arrMany);
			Assert.AreEqual(5, list.Size());
		}

		// Get() Tests
		[Test]
		public void Get_arr0IndexLeftOut_IndexOutOfRangeException()
		{
			list.Init(arr0);
			int i = -1;
			Assert.Throws<IndexOutOfRangeException>(() => list.Get(i));
		}
		[Test]
		public void Get_arr0IndexRightOut_IndexOutOfRangeException()
		{
			list.Init(arr0);
			int i = list.Size();
			Assert.Throws<IndexOutOfRangeException>(() => list.Get(i));
		}
		[Test]
		public void Get_arr0IndexLeft_IndexOutOfRangeException()
		{
			list.Init(arr0);
			int i = 0;
			Assert.Throws<IndexOutOfRangeException>(() => list.Get(i));
		}
		[Test]
		public void Get_arr0IndexMedium_IndexOutOfRangeException()
		{
			list.Init(arr0);
			int i = list.Size() / 2;
			Assert.Throws<IndexOutOfRangeException>(() => list.Get(i));
		}
		[Test]
		public void Get_arr0IndexRight_IndexOutOfRangeException()
		{
			list.Init(arr0);
			int i = list.Size() - 1;
			Assert.Throws<IndexOutOfRangeException>(() => list.Get(i));
		}
		[Test]
		public void Get_arr1IndexLeft_1Returned()
		{
			list.Init(arr1);
			int i = 0;
			Assert.AreEqual(1, list.Get(i));
		}
		[Test]
		public void Get_arr1IndexMedium_1Returned()
		{
			list.Init(arr1);
			int i = list.Size() / 2;
			Assert.AreEqual(1, list.Get(i));
		}
		[Test]
		public void Get_arr1IndexRight_1Returned()
		{
			list.Init(arr1);
			int i = list.Size() - 1;
			Assert.AreEqual(1, list.Get(i));
		}
		[Test]
		public void Get_arr2IndexLeft_1Returned()
		{
			list.Init(arr2);
			int i = 0;
			Assert.AreEqual(2, list.Get(i));
		}
		[Test]
		public void Get_arr2IndexMedium_1Returned()
		{
			list.Init(arr2);
			int i = list.Size() / 2;
			Assert.AreEqual(1, list.Get(i));
		}
		[Test]
		public void Get_arr2IndexRight_1Returned()
		{
			list.Init(arr2);
			int i = list.Size() - 1;
			Assert.AreEqual(1, list.Get(i));
		}
		[Test]
		public void Get_arrManyIndexLeft_1Returned()
		{
			list.Init(arrMany);
			int i = 0;
			Assert.AreEqual(5, list.Get(i));
		}
		[Test]
		public void Get_arrManyIndexMedium_1Returned()
		{
			list.Init(arrMany);
			int i = list.Size() / 2;
			Assert.AreEqual(3, list.Get(i));
		}
		[Test]
		public void Get_arrManyIndexRight_1Returned()
		{
			list.Init(arrMany);
			int i = list.Size() - 1;
			Assert.AreEqual(1, list.Get(i));
		}

		// Set() Tests
		[Test]
		public void Set_arr0IndexLeftOut0_IndexOutOfRangeException()
		{
			list.Init(arr0);
			int i = -1;
			Assert.Throws<IndexOutOfRangeException>(() => list.Set(i, 0));
		}
		[Test]
		public void Set_arr0IndexRightOut0_IndexOutOfRangeException()
		{
			list.Init(arr0);
			int i = list.Size();
			Assert.Throws<IndexOutOfRangeException>(() => list.Set(i, 0));
		}
		[Test]
		public void Set_arr0IndexLeftVal0_IndexOutOfRangeException()
		{
			list.Init(arr0);
			int i = 0;
			Assert.Throws<IndexOutOfRangeException>(() => list.Set(i, 0));
		}
		[Test]
		public void Set_arr0IndexMediumtVal0_IndexOutOfRangeException()
		{
			list.Init(arr0);
			int i = list.Size() / 2;
			Assert.Throws<IndexOutOfRangeException>(() => list.Set(i, 0));
		}
		[Test]
		public void Set_arr0IndexRightVal0_IndexOutOfRangeException()
		{
			list.Init(arr0);
			int i = list.Size() - 1;
			Assert.Throws<IndexOutOfRangeException>(() => list.Set(i, 0));
		}
		[Test]
		public void Set_arr1IndexLeftVal0_0()
		{
			list.Init(arr1);
			int i = 0;
			list.Set(i, 0);
			Assert.AreEqual(0, list.Get(i)); ;
		}
		[Test]
		public void Set_arr1IndexMediumtVal0_0()
		{
			list.Init(arr1);
			int i = list.Size() / 2;
			list.Set(i, 0);
			Assert.AreEqual(0, list.Get(i)); ;
		}
		[Test]
		public void Set_arr1IndexRightVal0_0()
		{
			list.Init(arr1);
			int i = list.Size() - 1;
			list.Set(i, 0);
			Assert.AreEqual(0, list.Get(i)); ;
		}
		[Test]
		public void Set_arr2IndexLeftVal0_0()
		{
			list.Init(arr2);
			int i = 0;
			list.Set(i, 0);
			Assert.AreEqual(0, list.Get(i)); ;
		}
		[Test]
		public void Set_arr2IndexMediumtVal0_0()
		{
			list.Init(arr2);
			int i = list.Size() / 2;
			list.Set(i, 0);
			Assert.AreEqual(0, list.Get(i)); ;
		}
		[Test]
		public void Set_arr2IndexRightVal0_0()
		{
			list.Init(arr2);
			int i = list.Size() - 1;
			list.Set(i, 0);
			Assert.AreEqual(0, list.Get(i)); ;
		}
		[Test]
		public void Set_arrManyIndexLeftVal0_0()
		{
			list.Init(arrMany);
			int i = 0;
			list.Set(i, 0);
			Assert.AreEqual(0, list.Get(i)); ;
		}
		[Test]
		public void Set_arrManyIndexMediumtVal0_0()
		{
			list.Init(arrMany);
			int i = list.Size() / 2;
			list.Set(i, 0);
			Assert.AreEqual(0, list.Get(i)); ;
		}
		[Test]
		public void Set_arrManyIndexRightVal0_0()
		{
			list.Init(arrMany);
			int i = list.Size() - 1;
			list.Set(i, 0);
			Assert.AreEqual(0, list.Get(i)); ;
		}
		// AddEnd() Tests
		[Test]
		public void AddEnd_arr0Val0_0()
		{
			list.Init(arr0);
			list.AddEnd(0);
			Assert.AreEqual(0, list.Get(list.Size() - 1)); ;
		}
		[Test]
		public void AddEnd_arr1Val0_0()
		{
			list.Init(arr1);
			list.AddEnd(0);
			Assert.AreEqual(0, list.Get(list.Size() - 1)); ;
		}
		[Test]
		public void AddEnd_arr2Val0_0()
		{
			list.Init(arr2);
			list.AddEnd(0);
			Assert.AreEqual(0, list.Get(list.Size() - 1)); ;
		}
		[Test]
		public void AddEnd_arrManyVal0_0()
		{
			list.Init(arrMany);
			list.AddEnd(0);
			Assert.AreEqual(0, list.Get(list.Size() - 1)); ;
		}
		// AddStart() Tests
		[Test]
		public void AddStart_arr0Val0_0()
		{
			list.Init(arr0);
			list.AddStart(0);
			Assert.AreEqual(0, list.Get(0)); ;
		}
		[Test]
		public void AddStart_arr1Val0_0()
		{
			list.Init(arr1);
			list.AddStart(0);
			Assert.AreEqual(0, list.Get(0)); ;
		}
		[Test]
		public void AddStart_arr2Val0_0()
		{
			list.Init(arr2);
			list.AddStart(0);
			Assert.AreEqual(0, list.Get(0)); ;
		}
		[Test]
		public void AddStart_arrManyVal0_0()
		{
			list.Init(arrMany);
			list.AddStart(0);
			Assert.AreEqual(0, list.Get(0)); ;
		}
		// AddPos() Tests
		[Test]
		public void AddPos_arr0IndexLeftOutVal0_IndexOutOfRangeException()
		{
			list.Init(arr0);
			int i = -1;
			Assert.Throws<IndexOutOfRangeException>(() => list.AddPos(i, 0));
		}
		[Test]
		public void AddPos_arr0IndexRighttOutVal0_IndexOutOfRangeException()
		{
			list.Init(arr0);
			int i = list.Size();
			Assert.Throws<IndexOutOfRangeException>(() => list.AddPos(i, 0));
		}
		[Test]
		public void AddPos_arr0IndexLeftVal0_IndexOutOfRangeException()
		{
			list.Init(arr0);
			int i = 0;
			Assert.Throws<IndexOutOfRangeException>(() => list.AddPos(i, 0));
		}
		[Test]
		public void AddPos_arr0IndexMiddleVal0_IndexOutOfRangeException()
		{
			list.Init(arr0);
			int i = list.Size() / 2;
			Assert.Throws<IndexOutOfRangeException>(() => list.AddPos(i, 0));
		}
		[Test]
		public void AddPos_arr0IndexRightVal0_IndexOutOfRangeException()
		{
			list.Init(arr0);
			int i = list.Size() - 1;
			Assert.Throws<IndexOutOfRangeException>(() => list.AddPos(i, 0));
		}
		[Test]
		public void AddPos_arr1IndexLeftVal0_0()
		{
			list.Init(arr1);
			int i = 0;
			list.AddPos(i, 0);
			Assert.AreEqual(0, list.Get(i));
		}
		[Test]
		public void AddPos_arr1IndexMiddleVal0_0()
		{
			list.Init(arr1);
			int i = list.Size() / 2;
			list.AddPos(i, 0);
			Assert.AreEqual(0, list.Get(i));
		}
		[Test]
		public void AddPos_arr1IndexRightVal0_0()
		{
			list.Init(arr1);
			int i = list.Size() - 1;
			list.AddPos(i, 0);
			Assert.AreEqual(0, list.Get(i));
		}
		[Test]
		public void AddPos_arr2IndexLeftVal0_0()
		{
			list.Init(arr2);
			list.AddPos(0, 0);
			Assert.AreEqual(0, list.Get(0));
		}
		[Test]
		public void AddPos_arr2IndexMiddleVal0_0()
		{
			list.Init(arr2);
			int i = list.Size() / 2;
			list.AddPos(i, 0);
			Assert.AreEqual(0, list.Get(i));
		}
		[Test]
		public void AddPos_arr2IndexRightVal0_0()
		{
			list.Init(arr2);
			int i = list.Size() - 1;
			list.AddPos(i, 0);
			Assert.AreEqual(0, list.Get(i));
		}
		[Test]
		public void AddPos_arrManyIndexLeftVal0_0()
		{
			list.Init(arrMany);
			list.AddPos(0, 0);
			Assert.AreEqual(0, list.Get(0));
		}
		[Test]
		public void AddPos_arrManyIndexMiddleVal0_0()
		{
			list.Init(arrMany);
			int i = list.Size() / 2;
			list.AddPos(i, 0);
			Assert.AreEqual(0, list.Get(i));
		}
		[Test]
		public void AddPos_arrManyIndexRightVal0_0()
		{
			list.Init(arrMany);
			int i = list.Size() - 1;
			list.AddPos(i, 0);
			Assert.AreEqual(0, list.Get(i));
		}
		// DelStart() Tests
		[Test]
		public void DelStart_arr0_IndexOutOfRangeException()
		{
			list.Init(arr0);
			Assert.Throws<IndexOutOfRangeException>(() => list.DelStart());
		}
		[Test]
		public void DelStart_arr1_1Returned()
		{
			list.Init(arr1);
			Assert.AreEqual(1, list.DelStart());
		}
		[Test]
		public void DelStart_arr1_DecrementedLengthReturned()
		{
			list.Init(arr1);
			int l = list.Size();
			list.DelStart();
			Assert.AreEqual(l - 1, list.Size());
		}
		[Test]
		public void DelStart_arr2_2Returned()
		{
			list.Init(arr2);
			Assert.AreEqual(2, list.DelStart());
		}
		[Test]
		public void DelStart_arr2_DecrementedLengthReturned()
		{
			list.Init(arr2);
			int l = list.Size();
			list.DelStart();
			Assert.AreEqual(l - 1, list.Size());
		}
		[Test]
		public void DelStart_arrMany_5Returned()
		{
			list.Init(arrMany);
			Assert.AreEqual(5, list.DelStart());
		}
		[Test]
		public void DelStart_arrMany_DecrementedLengthReturned()
		{
			list.Init(arrMany);
			int l = list.Size();
			list.DelStart();
			Assert.AreEqual(l - 1, list.Size());
		}
		// DelEnd() Tests
		[Test]
		public void DelEnd_arr0_IndexOutOfRangeException()
		{
			list.Init(arr0);
			Assert.Throws<IndexOutOfRangeException>(() => list.DelEnd());
		}
		[Test]
		public void DelEnd_arr1_1Returned()
		{
			list.Init(arr1);
			Assert.AreEqual(1, list.DelEnd());
		}
		[Test]
		public void DelEnd_arr1_DecrementedLengthReturned()
		{
			list.Init(arr1);
			int l = list.Size();
			list.DelEnd();
			Assert.AreEqual(l - 1, list.Size());
		}
		[Test]
		public void DelEnd_arr2_1Returned()
		{
			list.Init(arr2);
			Assert.AreEqual(1, list.DelEnd());
		}
		[Test]
		public void DelEnd_arr2_DecrementedLengthReturned()
		{
			list.Init(arr2);
			int l = list.Size();
			list.DelEnd();
			Assert.AreEqual(l - 1, list.Size());
		}
		[Test]
		public void DelEnd_arrMany_1Returned()
		{
			list.Init(arrMany);
			Assert.AreEqual(1, list.DelEnd());
		}
		[Test]
		public void DelEnd_arrMany_DecrementedLengthReturned()
		{
			list.Init(arrMany);
			int l = list.Size();
			list.DelEnd();
			Assert.AreEqual(l - 1, list.Size());
		}
		// DelPos() Tests
		[Test]
		public void DelPos_arr0IndexLeft_IndexOutOfRangeException()
		{
			list.Init(arr0);
			int i = 0;
			Assert.Throws<IndexOutOfRangeException>(() => list.DelPos(i));
		}
		[Test]
		public void DelPos_arr0IndexMedium_IndexOutOfRangeException()
		{
			list.Init(arr0);
			int i = list.Size() / 2;
			Assert.Throws<IndexOutOfRangeException>(() => list.DelPos(i));
		}
		[Test]
		public void DelPos_arr0IndexRight_IndexOutOfRangeException()
		{
			list.Init(arr0);
			int i = list.Size() - 1;
			Assert.Throws<IndexOutOfRangeException>(() => list.DelPos(i));
		}
		[Test]
		public void DelPos_arr1IndexLeft_1Returned()
		{
			list.Init(arr1);
			int i = 0;
			Assert.AreEqual(1, list.DelPos(i));
		}
		[Test]
		public void DelPos_arr1IndexLeft_DecrementedLengthReturned()
		{
			list.Init(arr1);
			int i = 0;
			int l = list.Size();
			list.DelPos(i);
			Assert.AreEqual(l - 1, list.Size());
		}
		[Test]
		public void DelPos_arr1IndexMedium_1Returned()
		{
			list.Init(arr1);
			int i = list.Size() / 2;
			Assert.AreEqual(1, list.DelPos(i));
		}
		[Test]
		public void DelPos_arr1IndexMedium_DecrementedLengthReturned()
		{
			list.Init(arr1);
			int i = list.Size() / 2;
			int l = list.Size();
			list.DelPos(i);
			Assert.AreEqual(l - 1, list.Size());
		}
		[Test]
		public void DelPos_arr1IndexRight_1Returned()
		{
			list.Init(arr1);
			int i = list.Size() - 1;
			Assert.AreEqual(1, list.DelPos(i));
		}
		[Test]
		public void DelPos_arr1IndexRight_DecrementedLengthReturned()
		{
			list.Init(arr1);
			int i = list.Size() - 1;
			int l = list.Size();
			list.DelPos(i);
			Assert.AreEqual(l - 1, list.Size());
		}
		[Test]
		public void DelPos_arr2IndexLeft_2Returned()
		{
			list.Init(arr2);
			int i = 0;
			Assert.AreEqual(2, list.DelPos(i));
		}
		[Test]
		public void DelPos_arr2IndexLeft_DecrementedLengthReturned()
		{
			list.Init(arr2);
			int i = 0;
			int l = list.Size();
			list.DelPos(i);
			Assert.AreEqual(l - 1, list.Size());
		}
		[Test]
		public void DelPos_arr2IndexMedium_1Returned()
		{
			list.Init(arr2);
			int i = list.Size() / 2;
			Assert.AreEqual(1, list.DelPos(i));
		}
		[Test]
		public void DelPos_arr2IndexMedium_DecrementedLengthReturned()
		{
			list.Init(arr2);
			int i = list.Size() / 2;
			int l = list.Size();
			list.DelPos(i);
			Assert.AreEqual(l - 1, list.Size());
		}
		[Test]
		public void DelPos_arr2IndexRight_1Returned()
		{
			list.Init(arr2);
			int i = list.Size() - 1;
			Assert.AreEqual(1, list.DelPos(i));
		}
		[Test]
		public void DelPos_arr2IndexRight_DecrementedLengthReturned()
		{
			list.Init(arr2);
			int i = list.Size() - 1;
			int l = list.Size();
			list.DelPos(i);
			Assert.AreEqual(l - 1, list.Size());
		}
		[Test]
		public void DelPos_arrManyIndexLeft_5Returned()
		{
			list.Init(arrMany);
			int i = 0;
			Assert.AreEqual(5, list.DelPos(i));
		}
		[Test]
		public void DelPos_arrManyIndexLeft_DecrementedLengthReturned()
		{
			list.Init(arrMany);
			int i = 0;
			int l = list.Size();
			list.DelPos(i);
			Assert.AreEqual(l - 1, list.Size());
		}
		[Test]
		public void DelPos_arrManyIndexMedium_3Returned()
		{
			list.Init(arrMany);
			int i = list.Size() / 2;
			Assert.AreEqual(3, list.DelPos(i));
		}
		[Test]
		public void DelPos_arrManyIndexMedium_DecrementedLengthReturned()
		{
			list.Init(arrMany);
			int i = list.Size() / 2;
			int l = list.Size();
			list.DelPos(i);
			Assert.AreEqual(l - 1, list.Size());
		}
		[Test]
		public void DelPos_arrManyIndexRight_1Returned()
		{
			list.Init(arrMany);
			int i = list.Size() - 1;
			Assert.AreEqual(1, list.DelPos(i));
		}
		[Test]
		public void DelPos_arrManyIndexRight_DecrementedLengthReturned()
		{
			list.Init(arrMany);
			int i = list.Size() - 1;
			int l = list.Size();
			list.DelPos(i);
			Assert.AreEqual(l - 1, list.Size());
		}
		// Sort() Tests
		[Test]
		public void Sort_arr0_EmptyString()
		{
			list.Init(arr0);
			list.Sort();
			Assert.AreEqual("", list.ToString());
		}
		[Test]
		public void Sort_arr1_SortedString()
		{
			list.Init(arr1);
			list.Sort();
			Assert.AreEqual("1", list.ToString());
		}
		[Test]
		public void Sort_arr2_SortedString()
		{
			list.Init(arr2);
			list.Sort();
			Assert.AreEqual("1, 2", list.ToString());
		}
		[Test]
		public void Sort_arrMany_SortedString()
		{
			list.Init(arrMany);
			list.Sort();
			Assert.AreEqual("1, 2, 3, 4, 5", list.ToString());
		}
		// Revers() Tests
		[Test]
		public void Revers_arr0_EmptyString()
		{
			list.Init(arr0);
			list.Revers();
			Assert.AreEqual("", list.ToString());
		}
		[Test]
		public void Revers_arr1_RevertedString()
		{
			list.Init(arr1);
			list.Revers();
			Assert.AreEqual("1", list.ToString());
		}
		[Test]
		public void Revers_arr2_RevertedString()
		{
			list.Init(arr2);
			list.Revers();
			Assert.AreEqual("1, 2", list.ToString());
		}
		[Test]
		public void Revers_arrMany_RevertedString()
		{
			list.Init(arrMany);
			list.Revers();
			Assert.AreEqual("1, 2, 3, 4, 5", list.ToString());
		}
		// HalfRevers() Tests
		[Test]
		public void HalfRevers_arr0_EmptyString()
		{
			list.Init(arr0);
			list.HalfRevers();
			Assert.AreEqual("", list.ToString());
		}
		[Test]
		public void HalfRevers_arr1_HalfRevertedString()
		{
			list.Init(arr1);
			list.HalfRevers();
			Assert.AreEqual("1", list.ToString());
		}
		[Test]
		public void HalfRevers_arr2_HalfRevertedString()
		{
			list.Init(arr2);
			list.HalfRevers();
			Assert.AreEqual("1, 2", list.ToString());
		}
		[Test]
		public void HalfRevers_arrMany_HalfRevertedString()
		{
			list.Init(arrMany);
			list.HalfRevers();
			Assert.AreEqual("2, 1, 3, 5, 4", list.ToString());
		}
		// Min() Tests
		[Test]
		public void Min_arr0_InvalidOperationException()
		{
			list.Init(arr0);
			Assert.Throws<InvalidOperationException>(() => list.Min());
		}
		[Test]
		public void Min_arr1_1Returned()
		{
			list.Init(arr1);
			Assert.AreEqual(1, list.Min());
		}
		[Test]
		public void Min_arr2_1Returned()
		{
			list.Init(arr2);
			Assert.AreEqual(1, list.Min());
		}
		[Test]
		public void Min_arrMany_1Returned()
		{
			list.Init(arrMany);
			Assert.AreEqual(1, list.Min());
		}
		// Max() Tests
		[Test]
		public void Max_arr0_InvalidOperationException()
		{
			list.Init(arr0);
			Assert.Throws<InvalidOperationException>(() => list.Max());
		}
		[Test]
		public void Max_arr1_1Returned()
		{
			list.Init(arr1);
			Assert.AreEqual(1, list.Max());
		}
		[Test]
		public void Max_arr2_2Returned()
		{
			list.Init(arr2);
			Assert.AreEqual(2, list.Max());
		}
		[Test]
		public void Max_arrMany_5Returned()
		{
			list.Init(arrMany);
			Assert.AreEqual(5, list.Max());
		}
		// MinIndex() Tests
		[Test]
		public void MinIndex_arr0_InvalidOperationException()
		{
			list.Init(arr0);
			Assert.Throws<InvalidOperationException>(() => list.MinIndex());
		}
		[Test]
		public void MinIndex_arr1_0Returned()
		{
			list.Init(arr1);
			Assert.AreEqual(0, list.MinIndex());
		}
		[Test]
		public void MinIndex_arr2_1Returned()
		{
			list.Init(arr2);
			Assert.AreEqual(1, list.MinIndex());
		}
		[Test]
		public void MinIndex_arrMany_4Returned()
		{
			list.Init(arrMany);
			Assert.AreEqual(4, list.MinIndex());
		}
		// MaxIndex() Tests
		[Test]
		public void MaxIndex_arr0_InvalidOperationException()
		{
			list.Init(arr0);
			Assert.Throws<InvalidOperationException>(() => list.MaxIndex());
		}
		[Test]
		public void MaxIndex_arr1_0Returned()
		{
			list.Init(arr1);
			Assert.AreEqual(0, list.MaxIndex());
		}
		[Test]
		public void MaxIndex_arr2_0Returned()
		{
			list.Init(arr2);
			Assert.AreEqual(0, list.MaxIndex());
		}
		[Test]
		public void MaxIndex_arrMany_0Returned()
		{
			list.Init(arrMany);
			Assert.AreEqual(0, list.MaxIndex());
		}
	}
}
