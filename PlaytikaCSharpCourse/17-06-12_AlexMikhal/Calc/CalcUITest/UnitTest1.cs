﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;

namespace CalcUITest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            IWebDriver driver = new FirefoxDriver();
            driver.Navigate().GoToUrl("file:///D:/C_sharp/17-06-07/Calc%20HTML/calc_2.html");
            driver.FindElement(By.Id("btn1")).Click();
            driver.FindElement(By.Id("btn5")).Click();
            driver.FindElement(By.Id("btnPlus")).Click();
            driver.FindElement(By.Id("btn2")).Click();
            driver.FindElement(By.Id("btnEq")).Click();
            String res = driver.FindElement(By.Id("txtRes")).GetAttribute("value");
            Assert.AreEqual("17", res);
            driver.Quit();
        }
    }
}
