﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CalcXZ
{
    public partial class Calc : Form
    {
        public Calc()
        {
            InitializeComponent();
        }

        public void Calc_Load(object sender, EventArgs e)
        {
    
        }

        string aNum = "";
        char op;

        private void btnNumClick(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            result.Text = result.Text + btn.Text;
        }

        // функция нажатия кнопок счета
        private void btnOpClick(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            string txt = btn.Text;
            if ( txt.Equals("=") )
            {
                int a = int.Parse(aNum);
                int b = int.Parse(result.Text);
                result.Text = "" + API.CalcX(a, b, op);
            }
            else
            {
                aNum = result.Text;
                result.Text = "";
                op = txt[0];
            }
        }

        // функция самого просчета
   
    }
}
