﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcXZ
{
    public class API
    {
        public static int CalcX(int a, int b, char op)
        {
            int res = 0;
            switch (op)
            {
                case '+': res = a + b; break;
                case '-': res = a - b; break;
                case '/': res = a / b; break;
                case '*': res = a * b; break;
            }
            return res;
        }
    }
}
