﻿using System;

namespace MathLibrary
{
	public class HitTest
	{
		public HitTest()
		{
			Console.Write(IsItPartOf(2, 2));
		}

		public int IsItPartOf(double x, double y)
		{
			if (x >= -2 && x <= 2 && y >= -1 && y <= 2 && (Math.Abs(y + 1)) / (Math.Abs(x) + 1) <= 1 && (Math.Abs(y + 1) + 1.5) / (Math.Abs(x) + 1) >= 1.5)
			{
				return 1;
			}
			else
				return 0;
		}
	}
}
