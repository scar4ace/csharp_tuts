﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathLibrary
{
	public class Distance
	{
		public Distance()
		{
			CalculateDistance(90.5, 50, 5, 5.5);
		}

		public double CalculateDistance(double velocityA, double velocityB, double startDistance, double time)
		{
			return (velocityA * time) + (velocityB * time) + startDistance;
		}
	}
}
