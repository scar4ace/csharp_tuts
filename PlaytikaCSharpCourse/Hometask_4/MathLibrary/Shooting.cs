﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathLibrary
{
	public class Shooting
	{
		public Shooting()
		{
			Console.WriteLine(CalculateDistanceDegrees(15, 150));// Degrees, velocity m/s
			Console.WriteLine(CalculateDistanceRadians(0.261799, 150));// Radians, velocity m/s
		}
		public double CalculateDistanceDegrees(double degrees, double startVelocity)
		{
			double l = 0;
			l = Math.Pow(startVelocity, 2) * Math.Sin(2 * (Math.PI / 180 * degrees)) / 9.8;
			return l;
		}
		public double CalculateDistanceRadians(double radians, double startVelocity)
		{
			double l = 0;
			l = Math.Pow(startVelocity, 2) * Math.Sin(2 * radians) / 9.8;
			return l;
		}
	}
}
