﻿using System;

namespace RandomGenerator
{
	class Program
	{
		static void Main(string[] args)
		{
			Random r = new Random();

			//Task 1 (Random number)
			Console.Write(r.Next());

			////Task 2 (Ten random numbers)
			//for (int i = 0; i < 10; i++)
			//{
			//	Console.Write(r.Next() + " ");
			//}

			////Task 3 (Ten random numbers in range 0-10)
			//for (int i = 0; i < 10; i++)
			//{
			//	Console.Write(r.Next(0, 10) + " ");
			//}

			////Task 4 (Ten random numbers in range 20-50)
			//for (int i = 0; i < 10; i++)
			//{
			//	Console.Write(r.Next(20, 50) + " ");
			//}

			////Task 5 (Ten random numbers in range -10-10)
			//for (int i = 0; i < 10; i++)
			//{
			//	Console.Write(r.Next(-10, 10) + " ");
			//}

			////Task 6 (Random in range 3-15 random numbers in range -10-35)
			//for (int i = 0; i < r.Next(3, 15); i++)
			//{
			//	Console.Write(r.Next(-10, 35) + " ");
			//}

			Console.ReadKey();
		}
	}
}
