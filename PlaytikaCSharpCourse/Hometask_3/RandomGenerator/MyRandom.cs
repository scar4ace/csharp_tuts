﻿using System;

namespace RandomGenerator
{
	class MyRandom
	{
		public int GetRandom(int rangeMin, int rangeMax)
		{
			while (rangeMin < DateTime.Now.Millisecond % (rangeMax + 1))
			{
				rangeMin++;
			}
			return rangeMin;
		}
	}
}
