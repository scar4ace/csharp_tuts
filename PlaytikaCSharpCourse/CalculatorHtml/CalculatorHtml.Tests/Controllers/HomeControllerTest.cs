﻿//using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace CalculatorHtml.Tests.Controllers
{
	[TestFixture]
	public class HomeControllerTest
	{
		IWebDriver driver;
		[SetUp]
		public void Setup()
		{
			driver = new ChromeDriver();
			driver.Navigate().GoToUrl("http://scar4ace.somee.com/");
			driver.Manage().Window.Maximize();
		}
		[TearDown]
		public void Teardown()
		{
			System.Threading.Thread.Sleep(5000);
			driver.Quit();
		}
		[Test]
		public void UICheckTitle()
		{
			StringAssert.Contains("Calculator - Asp.NET MVC - My ASP.NET Application", driver.Title);
		}
		[Test]
		public void UIInputAndSubmit()
		{
			IWebElement inputText = driver.FindElement(By.Name("a"));
			inputText.SendKeys("1234567890");
			inputText = driver.FindElement(By.Name("op"));
			inputText.SendKeys("+");
			inputText = driver.FindElement(By.Name("b"));
			inputText.SendKeys("1");
			inputText.Submit();
			Assert.AreEqual("1234567891", driver.FindElement(By.Name("res")).GetAttribute("value"));
		}
	}
}
