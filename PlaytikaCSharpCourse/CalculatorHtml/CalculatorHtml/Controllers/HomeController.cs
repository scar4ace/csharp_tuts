﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CalculatorHtml.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			return View();
		}
		public ActionResult Count(string a, string b, string op)
		{
			if (a != "" && b != "" && op != "")
			{
				switch (op)
				{
					case "+":
						ViewBag.res = (double.Parse(a) + double.Parse(b)).ToString();
						break;
					case "-":
						ViewBag.res = (double.Parse(a) - double.Parse(b)).ToString();
						break;

					case "/":
						if (b != "0")
							ViewBag.res = (double.Parse(a) / double.Parse(b)).ToString();
						else
							try
							{
								throw new DivideByZeroException("Cannot divide by zero");
							}
							catch (DivideByZeroException ex)
							{
								ViewBag.res = ex.Message;
							}
						break;
					case "*":
						ViewBag.res = (double.Parse(a) * double.Parse(b)).ToString();
						break;
					default:
						break;
				}
			}
			return View("Index");
		}
	}
}