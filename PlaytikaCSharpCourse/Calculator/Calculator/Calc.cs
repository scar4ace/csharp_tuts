﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
	public static class Calc
	{
		public static double Count(double a, double b, string op, bool isError, calc form)
		{
			double _res = 0;
			switch (op)
			{
				case "+":
					_res = a + b;
					break;
				case "-":
					_res = a - b;
					break;
				case "/":
					if (b == 0)
					{
						isError = true;
						throw new DivideByZeroException("Cannot divide by zero");
					}
					else
						_res = a / b;
					break;
				case "x":
					_res = a * b;
					break;
				default:
					isError = true;
					throw new ArgumentException();
			}
			if (form != null)
			{
				form.Reset();
			}
			return Math.Round(_res, 2);
		}
	}
}
