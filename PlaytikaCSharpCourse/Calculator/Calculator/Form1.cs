﻿using System;
using System.Windows.Forms;

namespace Calculator
{
	public partial class calc : Form
	{
		private double a, b;
		private string op = "";
		private string buffer = "";
		private bool isError = false;
		//private Calc calculator;

		public calc()
		{
			InitializeComponent();
			//calculator = new Calc();
		}
		private void button0_Click(object sender, EventArgs e)
		{
			SetOperand("0");
		}
		private void button1_Click(object sender, EventArgs e)
		{
			SetOperand("1");
		}
		private void button2_Click(object sender, EventArgs e)
		{
			SetOperand("2");
		}
		private void button3_Click(object sender, EventArgs e)
		{
			SetOperand("3");
		}
		private void button4_Click(object sender, EventArgs e)
		{
			SetOperand("4");
		}
		private void button5_Click(object sender, EventArgs e)
		{
			SetOperand("5");
		}
		private void button6_Click(object sender, EventArgs e)
		{
			SetOperand("6");
		}
		private void button7_Click(object sender, EventArgs e)
		{
			SetOperand("7");
		}
		private void button8_Click(object sender, EventArgs e)
		{
			SetOperand("8");
		}
		private void button9_Click(object sender, EventArgs e)
		{
			SetOperand("9");
		}
		private void buttonAdd_Click(object sender, EventArgs e)
		{
			SetOperator("+");
		}
		private void buttonMinus_Click(object sender, EventArgs e)
		{
			SetOperator("-");
		}
		private void buttonDivide_Click(object sender, EventArgs e)
		{
			SetOperator("/");
		}
		private void buttonMultiply_Click(object sender, EventArgs e)
		{
			SetOperator("x");
		}
		private void buttonEqual_Click(object sender, EventArgs e)
		{
			if (isError)
			{
				Reset();
				isError = false;
				return;
			}
			try
			{
				if (buffer != "" && op != "")
				{
					b = double.Parse(buffer);
					a = Calc.Count(a, b, op, isError,this);
					resultTextBox.Text = a.ToString();
				}
			}
			catch (DivideByZeroException ex)
			{
				Reset();
				resultTextBox.Text = ex.Message;
				isError = true;
			}
			finally
			{

			}
		}
		private void SetOperand(string _operand)
		{
			if (isError)
			{
				Reset();
				isError = false;
			}
			buffer += _operand;
			resultTextBox.Text += _operand;
		}
		private void SetOperator(string _operator)
		{
			if (isError)
			{
				Reset();
				return;
			}
			if (buffer != "")
			{
				if (op != "")
				{
					b = double.Parse(buffer);
					try
					{
						a = Calc.Count(a, b, op, isError,this);
					}
					catch (DivideByZeroException ex)
					{
						Reset();
						resultTextBox.Text = ex.Message;
						isError = true;
					}
					finally
					{

					}
				}
				else
				{
					a = double.Parse(buffer);
				}
				buffer = "";
			}
			if (!isError)
			{
				op = _operator;
				resultTextBox.Text = a.ToString() + op;
			}
		}
		public void Reset()
		{
			a = 0;
			b = 0;
			resultTextBox.Text = "";
			op = "";
			buffer = "";
		}
	}
}
