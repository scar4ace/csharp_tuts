﻿namespace Calculator
{
	partial class calc
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.resultTextBox = new System.Windows.Forms.TextBox();
			this.button0 = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.button4 = new System.Windows.Forms.Button();
			this.button5 = new System.Windows.Forms.Button();
			this.button6 = new System.Windows.Forms.Button();
			this.button7 = new System.Windows.Forms.Button();
			this.button8 = new System.Windows.Forms.Button();
			this.button9 = new System.Windows.Forms.Button();
			this.buttonAdd = new System.Windows.Forms.Button();
			this.buttonMinus = new System.Windows.Forms.Button();
			this.buttonDivide = new System.Windows.Forms.Button();
			this.buttonMultiply = new System.Windows.Forms.Button();
			this.buttonEqual = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// resultTextBox
			// 
			this.resultTextBox.Location = new System.Drawing.Point(12, 12);
			this.resultTextBox.Name = "resultTextBox";
			this.resultTextBox.Size = new System.Drawing.Size(162, 20);
			this.resultTextBox.TabIndex = 0;
			// 
			// button0
			// 
			this.button0.Location = new System.Drawing.Point(12, 38);
			this.button0.Name = "button0";
			this.button0.Size = new System.Drawing.Size(36, 35);
			this.button0.TabIndex = 1;
			this.button0.Text = "0";
			this.button0.UseVisualStyleBackColor = true;
			this.button0.Click += new System.EventHandler(this.button0_Click);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(54, 38);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(36, 35);
			this.button1.TabIndex = 2;
			this.button1.Text = "1";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(96, 38);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(36, 35);
			this.button2.TabIndex = 3;
			this.button2.Text = "2";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(12, 79);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(36, 35);
			this.button3.TabIndex = 4;
			this.button3.Text = "3";
			this.button3.UseVisualStyleBackColor = true;
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// button4
			// 
			this.button4.Location = new System.Drawing.Point(54, 79);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(36, 35);
			this.button4.TabIndex = 5;
			this.button4.Text = "4";
			this.button4.UseVisualStyleBackColor = true;
			this.button4.Click += new System.EventHandler(this.button4_Click);
			// 
			// button5
			// 
			this.button5.Location = new System.Drawing.Point(96, 79);
			this.button5.Name = "button5";
			this.button5.Size = new System.Drawing.Size(36, 35);
			this.button5.TabIndex = 6;
			this.button5.Text = "5";
			this.button5.UseVisualStyleBackColor = true;
			this.button5.Click += new System.EventHandler(this.button5_Click);
			// 
			// button6
			// 
			this.button6.Location = new System.Drawing.Point(12, 120);
			this.button6.Name = "button6";
			this.button6.Size = new System.Drawing.Size(36, 35);
			this.button6.TabIndex = 7;
			this.button6.Text = "6";
			this.button6.UseVisualStyleBackColor = true;
			this.button6.Click += new System.EventHandler(this.button6_Click);
			// 
			// button7
			// 
			this.button7.Location = new System.Drawing.Point(54, 120);
			this.button7.Name = "button7";
			this.button7.Size = new System.Drawing.Size(36, 35);
			this.button7.TabIndex = 8;
			this.button7.Text = "7";
			this.button7.UseVisualStyleBackColor = true;
			this.button7.Click += new System.EventHandler(this.button7_Click);
			// 
			// button8
			// 
			this.button8.Location = new System.Drawing.Point(96, 120);
			this.button8.Name = "button8";
			this.button8.Size = new System.Drawing.Size(36, 35);
			this.button8.TabIndex = 9;
			this.button8.Text = "8";
			this.button8.UseVisualStyleBackColor = true;
			this.button8.Click += new System.EventHandler(this.button8_Click);
			// 
			// button9
			// 
			this.button9.Location = new System.Drawing.Point(12, 161);
			this.button9.Name = "button9";
			this.button9.Size = new System.Drawing.Size(36, 35);
			this.button9.TabIndex = 10;
			this.button9.Text = "9";
			this.button9.UseVisualStyleBackColor = true;
			this.button9.Click += new System.EventHandler(this.button9_Click);
			// 
			// buttonAdd
			// 
			this.buttonAdd.Location = new System.Drawing.Point(138, 38);
			this.buttonAdd.Name = "buttonAdd";
			this.buttonAdd.Size = new System.Drawing.Size(36, 35);
			this.buttonAdd.TabIndex = 11;
			this.buttonAdd.Text = "+";
			this.buttonAdd.UseVisualStyleBackColor = true;
			this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
			// 
			// buttonMinus
			// 
			this.buttonMinus.Location = new System.Drawing.Point(138, 79);
			this.buttonMinus.Name = "buttonMinus";
			this.buttonMinus.Size = new System.Drawing.Size(36, 35);
			this.buttonMinus.TabIndex = 12;
			this.buttonMinus.Text = "-";
			this.buttonMinus.UseVisualStyleBackColor = true;
			this.buttonMinus.Click += new System.EventHandler(this.buttonMinus_Click);
			// 
			// buttonDivide
			// 
			this.buttonDivide.Location = new System.Drawing.Point(138, 120);
			this.buttonDivide.Name = "buttonDivide";
			this.buttonDivide.Size = new System.Drawing.Size(36, 35);
			this.buttonDivide.TabIndex = 13;
			this.buttonDivide.Text = "/";
			this.buttonDivide.UseVisualStyleBackColor = true;
			this.buttonDivide.Click += new System.EventHandler(this.buttonDivide_Click);
			// 
			// buttonMultiply
			// 
			this.buttonMultiply.Location = new System.Drawing.Point(138, 161);
			this.buttonMultiply.Name = "buttonMultiply";
			this.buttonMultiply.Size = new System.Drawing.Size(36, 35);
			this.buttonMultiply.TabIndex = 14;
			this.buttonMultiply.Text = "x";
			this.buttonMultiply.UseVisualStyleBackColor = true;
			this.buttonMultiply.Click += new System.EventHandler(this.buttonMultiply_Click);
			// 
			// buttonEqual
			// 
			this.buttonEqual.Location = new System.Drawing.Point(54, 161);
			this.buttonEqual.Name = "buttonEqual";
			this.buttonEqual.Size = new System.Drawing.Size(78, 35);
			this.buttonEqual.TabIndex = 15;
			this.buttonEqual.Text = "=";
			this.buttonEqual.UseVisualStyleBackColor = true;
			this.buttonEqual.Click += new System.EventHandler(this.buttonEqual_Click);
			// 
			// calc
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoSize = true;
			this.ClientSize = new System.Drawing.Size(187, 212);
			this.Controls.Add(this.buttonEqual);
			this.Controls.Add(this.buttonMultiply);
			this.Controls.Add(this.buttonDivide);
			this.Controls.Add(this.buttonMinus);
			this.Controls.Add(this.buttonAdd);
			this.Controls.Add(this.button9);
			this.Controls.Add(this.button8);
			this.Controls.Add(this.button7);
			this.Controls.Add(this.button6);
			this.Controls.Add(this.button5);
			this.Controls.Add(this.button4);
			this.Controls.Add(this.button3);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.button0);
			this.Controls.Add(this.resultTextBox);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
			this.MaximizeBox = false;
			this.MaximumSize = new System.Drawing.Size(203, 246);
			this.MinimizeBox = false;
			this.MinimumSize = new System.Drawing.Size(203, 246);
			this.Name = "calc";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Calculator";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox resultTextBox;
		private System.Windows.Forms.Button button0;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.Button button5;
		private System.Windows.Forms.Button button6;
		private System.Windows.Forms.Button button7;
		private System.Windows.Forms.Button button8;
		private System.Windows.Forms.Button button9;
		private System.Windows.Forms.Button buttonAdd;
		private System.Windows.Forms.Button buttonMinus;
		private System.Windows.Forms.Button buttonDivide;
		private System.Windows.Forms.Button buttonMultiply;
		private System.Windows.Forms.Button buttonEqual;
	}
}

