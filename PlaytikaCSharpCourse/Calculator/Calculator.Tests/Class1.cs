﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Calculator.Tests
{
	[TestFixture]
	public class Class1
	{
		//Addition
		[Test]
		public void Count_15Plus2_17Returned()
		{
			Assert.That(Calculator.Calc.Count(15, 2, "+", false, null), Is.EqualTo(17));
		}
		[Test]
		public void Count_Negative15Plus2_Negtive13Returned()
		{
			Assert.That(Calculator.Calc.Count(-15, 2, "+", false, null), Is.EqualTo(-13));
		}
		//Substraction
		[Test]
		public void Count_15Minus2_13Returned()
		{
			Assert.That(Calculator.Calc.Count(15, 2, "-", false, null), Is.EqualTo(13));
		}
		[Test]
		public void Count_Negative15Minus2_Negtive17Returned()
		{
			Assert.That(Calculator.Calc.Count(-15, 2, "-", false, null), Is.EqualTo(-17));
		}
		//Multiplication
		[Test]
		public void Count_3Multiply7_21Returned()
		{
			Assert.That(Calculator.Calc.Count(3, 7, "x", false, null), Is.EqualTo(21));
		}
		[Test]
		public void Count_Negative3Multiply5_Negative17Returned()
		{
			Assert.That(Calculator.Calc.Count(-3, 5, "x", false, null), Is.EqualTo(-15));
		}
		//Division
		[Test]
		public void Count_9Divide3_3Returned()
		{
			Assert.That(Calculator.Calc.Count(9, 3, "/", false, null), Is.EqualTo(3));
		}
		[Test]
		public void Count_3Divide9_0_33Returned()
		{
			Assert.That(Calculator.Calc.Count(3, 9, "/", false, null), Is.EqualTo(0.33));
		}
		[Test]
		public void Count_3Divide5_0_6Returned()
		{
			Assert.That(Calculator.Calc.Count(3, 5, "/", false, null), Is.EqualTo(0.6));
		}
		//Exceptions
		[Test]
		[ExpectedException("System.DivideByZeroException", ExpectedMessage ="Cannot divide by zero")]
		public void Count_DivideByZeroException()
		{
			Calc.Count(3, 0, "/", false, null);
		}

	}
}
