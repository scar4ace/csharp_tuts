﻿using System;

namespace BsTreeProj
{
    public class BsTree
    {
        class Node
        {
            public int val;
            public Node left;
            public Node right;
            public Node( int val )
            {
                this.val = val;
            }
        }

        Node root = null;

        public BsTree()
        {
            //
        }

        public BsTree( int[] ini )
        {
            Init( ini );
        }

        public void Clear()
        {
            root = null;
        }

        public void Init( int[] ini )
        {
            Clear();
            if (ini == null)
                return;
            for (int i = 0; i < ini.Length; i++)
            {
                Add(ini[i]);
            }
        }

        public void Add(int val)
        {
            if( root == null )
            {
                root = new Node(val);
                return;
            }
            AddNode(root, val);
        }

        private void AddNode(Node p, int val)
        {
            if(val < p.val)
            {
                if (p.left == null)
                    p.left = new Node(val);
                else
                    AddNode(p.left, val);
            }
            else
            {
                if (p.right == null)
                    p.right = new Node(val);
                else
                    AddNode(p.right, val);
            }
        }

        public override String ToString()
        {
            return NodeToString(root);
        }

        private string NodeToString(Node p)
        {
            if (p == null)
                return "";

            string str = "";
            str += NodeToString(p.left);
            str += p.val + ", ";
            str += NodeToString(p.right);
            return str;
        }
    }
}