﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BsTreeProj
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] ini = { 50, 25, 11, 77, 4, 21, 30, 99, 106, 64 };
            BsTree tr = new BsTree(ini);
            String str = tr.ToString();
            Console.WriteLine( str );
            Console.ReadKey();
        }
    }
}
