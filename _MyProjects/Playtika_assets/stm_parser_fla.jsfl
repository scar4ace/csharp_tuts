xjsfl.init(this);
clear();
//main vars
//var stm_tree_obj={'state_machines':[]};//final state machines tree object
var stm_tree_json='';
var pathToJSON=xjsfl.uri + 'user/jsfl/snippets/_lib/Playtika_assets/fla_stm_tree.json';
//auxiliary vars
var stm_arr_lib=[];//an array with all state machines from library
var stm_obj_comprising={'stm':[]};//an array of state machines and their children
var stm_obj_temp;
var state_obj_temp;
var state_children_temp=[];
var temp_stm_list=[];
var temp_id_list=[];
var _last_mc_uri='';

function Main()
{
    Find_all_stm();
    for(var i=0;i<stm_arr_lib.length;i++)
    {
        stm_obj_temp = {"id": "","mc_uri": "","states":[],"stm_type": ""};//state machine object
        stm_obj_temp.mc_uri=stm_arr_lib[i].stm_mc_uri;
        stm_obj_temp.stm_type=stm_arr_lib[i].layer_name;
        for(var j=0;j<stm_arr_lib[i].states_mc_uri.length;j++)
        {
            state_obj_temp={"label": "","mc_uri": "","transition": "","children":[]};//state obj
            state_obj_temp.label=stm_arr_lib[i].state_label[j];
            state_obj_temp.mc_uri=stm_arr_lib[i].states_mc_uri[j];
            
            state_obj_temp.transition=stm_arr_lib[i].transition[j];
            
            Parse(stm_arr_lib[i].states_mc_uri[j]);
            state_obj_temp.children=state_children_temp;
            stm_obj_temp.states.push(state_obj_temp);
            //state_obj_temp.children=state_children_temp;
            state_children_temp=[];
        }
        stm_obj_comprising.stm.push(stm_obj_temp);
    }
    Assign_id();
    Add_remove_child('remove extra');
    stm_tree_json=JSON.encode(stm_obj_comprising);//stm_obj_comprising change to stm_tree_obj
    var _fileJson = new File(pathToJSON,'',true);
    _fileJson.write(stm_tree_json);
}
function Assign_id()
{
    for(var i=0;i<temp_id_list.length;i++)
    {
        for(var j=0;j<temp_id_list[i].length;j++)
        {
            Sortof_stm_obj_comprising(temp_id_list[i][j].uri,temp_id_list[i][j].id);
        }
    }
    Add_remove_child('add');
    Add_remove_child('remove');
}
function Add_remove_child(_action_type)
{
    for(var i=0;i<stm_obj_comprising.stm.length;i++)//in 'stm'
    {
        if(_action_type=='remove extra'&&stm_obj_comprising.stm[i].mc_uri.substring(stm_obj_comprising.stm[i].mc_uri.indexOf('.'),stm_obj_comprising.stm[i].mc_uri.length)!='.object')
        {
            stm_obj_comprising.stm[i]='delete';
        }else{
            for(var j=0;j<stm_obj_comprising.stm[i].states.length;j++)//in 'states'
            {
                for(var c=0;c<stm_obj_comprising.stm[i].states[j].children.length;c++)//in 'children'
                {
                    if(stm_obj_comprising.stm[i].states[j].children[0]!=undefined)
                    {
                        switch(_action_type)
                        {
                            case 'add':
                            Add_to_children(stm_obj_comprising.stm[i].states[j].children[c],stm_obj_comprising.stm[i].states[j].children);
                                break;
                            case 'remove':
                                if(typeof stm_obj_comprising.stm[i].states[j].children[c]=='string')
                                {
                                    stm_obj_comprising.stm[i].states[j].children[c]='delete';
                                }
                                break;
                            //case 'remove extra':
                            default:
                                break;
                        }
                    }
                }
                Remove_element(stm_obj_comprising.stm[i].states[j].children,'delete');
            }
        }
    }
    Remove_element(stm_obj_comprising.stm,'delete');
}
function Add_to_children(_mc_uri_to_push,_children_arr)
{
    var _id_str;
    var _obj_to_push;
    var _id_arr=[];
    for(var i=0;i<stm_obj_comprising.stm.length;i++)
    {
        if(stm_obj_comprising.stm[i].id!=''&&_mc_uri_to_push==stm_obj_comprising.stm[i].mc_uri)
        {
            _id_str=stm_obj_comprising.stm[i].id;
            _id_arr=Split_string_to_arr(_id_str,',');
            for(var j=0;j<_id_arr.length;j++)
            {
                _obj_to_push=Clone_object(stm_obj_comprising.stm[i]);
                _obj_to_push.id=_id_arr[j];
                _children_arr.push(_obj_to_push);
            }
        }
    }
}
function Clone_object(_obj)//Common function
{
    var _clone={};
    for(var _key in _obj)
    {
        _clone[_key]=_obj[_key];
    }
    return _clone;
}
function Split_string_to_arr(_str,_divider)//Common function
{
    var _arr=[];
    if(_str.indexOf(_divider)>0)
    {
        while(_str.indexOf(_divider)>0)
        {
            i=0;
            var _divider_index=_str.indexOf(_divider);
            var _element=_str.substring(i,_divider_index);
            i=_divider_index+1;
            _str=_str.substring(i,_str.length);
            _arr.push(_element);
        }
        _element=_str;
        _arr.push(_element);
    }else{
        _arr[0]=_str;
    }
    return _arr;
}
function Sortof_stm_obj_comprising(_uri,_id)
{
    for(var i=0;i<stm_obj_comprising.stm.length;i++)
    {
        if(stm_obj_comprising.stm[i].mc_uri==_uri)
        {
            if(stm_obj_comprising.stm[i].id=="")
            {
                stm_obj_comprising.stm[i].id=_id;
            }else{
                //id list of duplicate mc
                stm_obj_comprising.stm[i].id+=(','+_id);
            }
        }
    }
}
function Find_all_stm()
{
    for(var i=0;i<$library.items.length;i++)
    {
        if($library.items[i].itemType=='movie clip')
        {
            for(var z=0;z<$library.items[i].timeline.layers.length;z++)
            {
                if($library.items[i].timeline.layers[z].layerType=='normal')
                {
                    if($library.items[i].timeline.layers[z].name=='states'||$library.items[i].timeline.layers[z].name=='skins'||$library.items[i].timeline.layers[z].name=='button')
                    {
                        var _stm={'stm_mc_uri':'','states_mc_uri':[],'state_label':[],'transition':[],'layer_name':''};
                        _stm.stm_mc_uri=$library.items[i].name;
                        _stm.layer_name=$library.items[i].timeline.layers[z].name;
                        $library.selectItem($library.items[i].name);
                        var _stm_mc=$library.getSelectedItems()[0];
                        for(var s=0;s<_stm_mc.timeline.layers[z].frames.length;s++)
                        {
                            _stm.state_label.push(_stm_mc.timeline.layers[z].frames[s].name);
                            _stm.transition.push((_stm_mc.timeline.layers[z].frames[s].actionScript.substring(_stm_mc.timeline.layers[z].frames[s].actionScript.indexOf(':')+1,_stm_mc.timeline.layers[z].frames[s].actionScript.lastIndexOf('*'))).replace(/[\n\r]+/g,''));
                            if(_stm_mc.timeline.layers[z].frames[s].elements[0]==undefined)
                            {
                                _stm.states_mc_uri.push('empty state');
                            }else{
                                for(var m=0;m<_stm_mc.timeline.layers[z].frames[s].elements.length;m++)
                                {
                                    _stm.states_mc_uri.push(_stm_mc.timeline.layers[z].frames[s].elements[m].libraryItem.name);
                                }
                            }
                        }
                        stm_arr_lib.push(_stm);
                    }
                }
            }
        }
    }
}
function Parse(_mc_uri)
{
    var all_id_on_level=[];
    var was_added=false;
    var id_duplicate=false;
    $library.selectItem(_mc_uri);
    var _curr_mc=$library.getSelectedItems()[0];
    var _last_parsed_id='';
    if(_curr_mc.timeline!=undefined)
    {
        for(var l=0;l<_curr_mc.timeline.layers.length;l++)
        {
            var _curr_layer=_curr_mc.timeline.layers[l];
            if(_curr_layer.layerType=='normal')
            {
                for(var f=0;f<_curr_mc.timeline.layers[l].frames.length;f++)
                {
                    var _curr_frame=_curr_mc.timeline.layers[l].frames[f];
                    for(var e=0;e<_curr_mc.timeline.layers[l].frames[f].elements.length;e++)
                    {
                        var _curr_element=_curr_mc.timeline.layers[l].frames[f].elements[e];
                        if(_curr_element!=undefined&&(_curr_element.symbolType=='movie clip'||_curr_element.instanceType=='bitmap'))
                        {
                            if(_curr_element.name!='')
                            {
                                var _id_obj={'id':'','uri':''};
                                _id_obj.id=_curr_element.name;
                                _id_obj.uri=_curr_element.libraryItem.name;
                                if(_last_parsed_id!=_id_obj.id)
                                {
                                    _last_parsed_id= _id_obj.id;
                                    all_id_on_level.push(_id_obj);
                                }
                            }
                            if(_curr_layer.name=='states'||_curr_layer.name=='skins'||_curr_layer.name=='button')
                            {
                                for(var i=0;i<temp_stm_list.length;i++)
                                {
                                    if(temp_stm_list[i]==_mc_uri)
                                    {
                                        was_added=true;
                                    }
                                }
                                if(!was_added)
                                {
                                    temp_stm_list.push(_mc_uri);
                                    if(_mc_uri!='empty state')
                                    {
                                        state_children_temp.push(_mc_uri);//���������� ������ ��� ������
                                    }
                                }
                                break;
                            }
                            Parse(_curr_element.libraryItem.name);
                        }
                    }
                }
            }
        }
        if(all_id_on_level[0]!=undefined&&_last_mc_uri!=_mc_uri)
        {
            _last_mc_uri=_mc_uri;
            all_id_on_level=Remove_duplicates(all_id_on_level);
            temp_id_list.push(all_id_on_level);
        }
    }
}
function Remove_duplicates(_arr)
{
    if(_arr.length>=2)
    {
        for(var i=0;i<_arr.length;i++)
        {
            for(var j=i+1;j<_arr.length;j++)
            {
                if(_arr[i]!='duplicate'&&_arr[i].id==_arr[j].id)
                {
                    _arr[j]='duplicate';
                }
            }
        }
        _arr=Remove_element(_arr,'duplicate');
    }
    return _arr;
}
function Remove_element(_arr,_keyword)
{
    for(var i=0;i<_arr.length;i++)
    {
        if(_arr[i]==_keyword)
        {
            _arr.splice(i,1);
            Remove_element(_arr,_keyword);
            break;
        }
    }
    return _arr;
}
Main();