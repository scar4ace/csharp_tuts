xjsfl.init(this);
clear();
function Correct_position(_x,_y)
{
    $dom.selection[0].x=_x;
    $dom.selection[0].y=_y;
}
function Create_control(_control_id,_control_type,_states_arr)
{
    var _control_posX;
    var _control_posY;
    $dom.convertToSymbol('movie clip',_control_id+'_'+_states_arr[0]+'_mc','center');
    _control_posX=$dom.selection[0].x;
    _control_posY=$dom.selection[0].y;
    $dom.deleteSelection();
    $library.addNewItem('movie clip',_control_id);
    $library.addItemToDocument({x:0,y:0},_control_id);
    Correct_position(_control_posX,_control_posY);
    $dom.selection[0].name=_control_id;
    $library.editItem(_control_id);
    if(_control_type=='item')
    {
        $timeline.layers[0].name='states';
    }else{
        $timeline.layers[0].name=_control_type;
    }
    for(var i=0;i<_states_arr.length;i++)
    {
        if(i!=0)
        {
            $timeline.insertBlankKeyframe(i);
            if(_states_arr[i]=='dis')
            {
                $library.addItemToDocument({x:0,y:0},_control_id+'_'+_states_arr[0]+'_mc');
                $timeline.layers[0].frames[i].actionScript='/*property:saturation,-90*/';
            }else{
                $library.duplicateItem(_control_id+'_'+_states_arr[0]+'_mc');
                $library.selectItem(_control_id+'_'+_states_arr[0]+'_mc copy');
                $library.renameItem(_control_id+'_'+_states_arr[i]+'_mc');
                $library.addItemToDocument({x:0,y:0},_control_id+'_'+_states_arr[i]+'_mc');
                if(_control_type=='checkbox'&&_states_arr[i]=='up')
                {
                    $library.editItem(_control_id+'_up_mc');
                    for(var j=0;j<$timeline.layers[0].frames[0].elements.length;j++)
                    {
                        if($timeline.layers[0].frames[0].elements[j].libraryItem.itemName=='check')
                        {
                            $dom.selectNone();
                            $timeline.layers[0].frames[0].elements[j].selected=true;
                            $dom.deleteSelection();
                        }
                    }
                    $library.editItem(_control_id);
                }
            }
        }else{
            $library.addItemToDocument({x:0,y:0},_control_id+'_'+_states_arr[0]+'_mc');
            if(_control_type!='checkbox'&&_states_arr[i]!='up')
            {
                $timeline.layers[0].frames[i].actionScript='/*next:up,0*/';
            }
        }
        $timeline.layers[0].frames[i].name=_states_arr[i];
        if(_control_type=='button'&&_states_arr[i]=='down')
        {
            Correct_position(0,7);
            $timeline.layers[0].frames[i].actionScript='/*'+'\n'+'property:saturation,+50'+'\n'+'property:brightness,+70'+'\n'+'*/';
            $library.editItem(_control_id+'_down_mc');
            $timeline.addNewLayer('sound','guide',true);
            $timeline.layers[$timeline.currentLayer].frames[0].actionScript='/*property:reset,true'+'\n'+'keepplaying:true*/';
            $library.editItem(_control_id);
            
        }else{
            Correct_position(0,0);
        }
    }
    $timeline.addNewLayer('touch','normal',false);
    $timeline.layers[$timeline.currentLayer].frames[0].name='touch';
    $timeline.removeFrames(2,$timeline.layers[$timeline.currentLayer].frames.length);
}
function Main()
{
    xul_assemble = XUL
    .factory('title:Create component,textbox:id,dropdown:type=[button,checkbox,item],textbox:states,button:Create=create')	
    .setColumns([75,250])
    .setButtons('')
    .addEvent('create','click', Create_click_handler)
    .show();
}
function Create_click_handler()
{
    this.close();
    var _control_id;
    var _control_type;
    var _states_arr=[];
    if(this.controls.states.value==null)
    {
        switch(this.controls.type.value)
        {
            case 'button':
                _states_arr=['up','down'];
                break;
            case 'checkbox':
                _states_arr=['down','up'];
                break;
            case 'item':
                _states_arr=['up','down','win','dim'];
                break;
            default:
                break;
        }
    }else{
        var _str=this.controls.states.value;
        var start_index=_str.indexOf(',');
        
        while(start_index<=_str.lastIndexOf(',')&&start_index>0)
        {
            _states_arr.push(_str.substring(0,start_index));
            _str=_str.substring(start_index+1,_str.length);
            if(_str.lastIndexOf(',')<0)
            {
                _states_arr.push(_str);
            }
            start_index=_str.indexOf(',');
        }
    }
    _control_type=this.controls.type.value;
    if(this.controls.id.value==null)
    {
        _control_id='noname';
    }else{
        _control_id=this.controls.id.value;
    }
    Create_control(_control_id,_control_type,_states_arr);
}
Main();