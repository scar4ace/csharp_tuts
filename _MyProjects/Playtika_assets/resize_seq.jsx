﻿#target photoshop

app.bringToFront();
app.preferences.rulerUnits = Units.PIXELS;

#include "json2.js"
   
var script_file = File($.fileName);
var script_file_path = script_file.path;
var file_to_read = File(script_file_path + "/Icons_assembly_data.json");
var my_JSON_object = null;
var content;
var fileRef;
var seq_folder=[];
var resized_folder;
var sequence=[];
var img;
   
function Main(){
   if(file_to_read){
      file_to_read.open('r'); // open it
      content = file_to_read.read(); // read it
      my_JSON_object =  JSON.parse(content);// now evaluate the string from the file
      file_to_read.close(); // always close files after reading
      Open_file ();
      }else{
      alert("alert");
      }    
}
function Open_file()
{
   for(var i=0;i<my_JSON_object.icons.length;i++)
   {
      Set_folder_from_uri(my_JSON_object.icons[i].uri);
      resized_folder=seq_folder+'/resized'
      Folder(resized_folder).create();
      sequence=seq_folder.getFiles("*.png");
      for (var j=0;j<sequence.length;j++)
      {
         fileRef = new File(sequence[j]);
         img = app.open(fileRef);
         app.activeDocument.resizeImage(my_JSON_object.icons[i].width,my_JSON_object.icons[i].height, undefined, ResampleMethod.BICUBICSHARPER);
         var _resized_img=new File(seq_folder+'/'+'resized/'+img.name);
         var _save_options = new ExportOptionsSaveForWeb();
         _save_options.format = SaveDocumentType.PNG;
         _save_options.optimized = true;
         _save_options.PNG8 = false;
         app.activeDocument.exportDocument(File(_resized_img),ExportType.SAVEFORWEB,_save_options);
         app.activeDocument.close(SaveOptions.DONOTSAVECHANGES);
      }
   }
   alert("Resize is done!");
}
function Set_folder_from_uri(_uri)
{
   var cut_file=_uri.lastIndexOf('/');
   seq_folder=_uri.substring(8,cut_file+1);
   seq_folder=new Folder(seq_folder[0]+':'+seq_folder.substring(2));
}
Main();