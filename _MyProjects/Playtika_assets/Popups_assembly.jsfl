xjsfl.init(this);
fl.createDocument("timeline");
$dom.width=3408;
$dom.height=1920;
clear();
//VARS
var psd_uri=fl.browseForFileURL("select", "Select *.psd", "PSD Document (*.psd)", "psd");
var popup_presets_JSON=xjsfl.uri + 'user/jsfl/snippets/_lib/Playtika_assets/Popup_presets.json';
var popup_presets_OBJ={};
var xul_assemble;
var ui_dropdown='';
var module_type='';
var item_for_parse;
var sprites_obj={'sprites':[]};
var main_item_name='';
var to_layer;
var actual_id='none';
var actual_label='none';
//presets
var _states_fs_welcome_arr=['default','freespins','freespins_add'];
//hardcode for full screen
var y_pos_shift= -192;
//Run
function Main()
{
    Decode_json(popup_presets_JSON);
    Construct_dropdown_ui();
}
function Construct_dropdown_ui()
{
    var _module_types_arr=[];
    for(_module_type in popup_presets_OBJ)
    {
        _module_types_arr.push(_module_type.toString());
    }
    ui_dropdown=',dropdown:'+'Module'+'=['+_module_types_arr+']'
    ui_buttons='button:Assembly=assembly';
    xul_assemble = XUL
	.factory('title:Choose module type'+ui_dropdown+','+ui_buttons)	
        .setColumns([65,200])
	.setButtons('')
	.addEvent('assembly','click', ClickAssemblyHandler)
	.show();
}
function ClickAssemblyHandler()
{
    module_type=this.controls.module.value;
    main_item_name=popup_presets_OBJ[module_type][0].object;
    this.close();
    Create_main_object(main_item_name);
    $dom.importFile(psd_uri,true);
    Parse_psd();
    Create_structure();
    Assemble();
    Delete_unused();
    Correct_control_mc();
    $dom.exitEditMode();
}
function Decode_json(_json)
{
    _json=FLfile.read(_json);
    popup_presets_OBJ=JSON.decode(_json);
}
function Create_structure()
{
    $library.editItem(main_item_name);
    //layers structure
    for(var i=item_for_parse.timeline.layers.length-1;i>=0;i--)
    {
        $timeline.addNewLayer(item_for_parse.timeline.layers[i].name);
    }
    $timeline.deleteLayer(item_for_parse.timeline.layers.length);
    //mc structure
    for(var i=0;i<popup_presets_OBJ[module_type].length;++i)
    {
        var _id=popup_presets_OBJ[module_type][i].id;
        var _layer_index=$timeline.findLayerIndex(_id);
        $timeline.currentLayer=_layer_index[0];
        $library.addNewItem('movie clip',_id);
        var _lib_item=$library.items[$library.findItemIndex(_id)];
        if(popup_presets_OBJ[module_type][i].type=='states')
        {            
            _lib_item.timeline.layers[0].name='states';
            _states_arr=item_for_parse.timeline.layers[item_for_parse.timeline.findLayerIndex(_id)].frames[0].elements[0].libraryItem.timeline.layers;
            for(var j=0;j<_states_arr.length;j++)
            {
                if(j!=0)
                {
                    _lib_item.timeline.insertBlankKeyframe(j);
                }
                _lib_item.timeline.layers[0].frames[j].name=_states_arr[j].name;
            }
        }else if(popup_presets_OBJ[module_type][i].type=='button'||popup_presets_OBJ[module_type][i].type=='checkbox'){
            _lib_item.timeline.layers[0].name=popup_presets_OBJ[module_type][i].type;
            _states_arr=popup_presets_OBJ[module_type][i].states;
            Create_control_struct(_lib_item,_states_arr,popup_presets_OBJ[module_type][i].type);
        }
        $library.addItemToDocument({x:0,y:0},_id);
        $timeline.layers[_layer_index].frames[0].elements[0].name=_id;
    }
    Create_txt_id_structure();
}
function Create_txt_id_structure()
{
    var _txt_id_arr=popup_presets_OBJ[module_type][0].txt_id;
    $timeline.currentLayer=0;
    for(var i=0;i<_txt_id_arr.length;i++)
    {
        $timeline.addNewLayer(_txt_id_arr[i]);
        $dom.addNewText({left:0, top:0, right:100, bottom:10});
        $dom.setElementProperty('autoExpand', true);
        $dom.setElementProperty('textType', 'dynamic');
        $dom.setTextString('0123456789kmbKMBXx=.,');
        $dom.setFillColor('#ff0000');
        Convert_txt_to_id(_txt_id_arr[i]);
        $library.editItem(main_item_name);
    }
}
function Convert_txt_to_id(_txt_id)
{
    $timeline.layers[0].frames[0].actionScript=Set_mobile_property('text');
    $timeline.layers[0].frames[0].elements[0].name=_txt_id;
    $timeline.layers[0].frames[0].elements[0].x=0;
    $timeline.setSelectedLayers(0);
    $timeline.cutFrames();
    $library.addNewItem("movie clip",_txt_id+'_mc');
    $library.addItemToDocument({x:0,y:0},_txt_id+'_mc');
    $library.editItem(_txt_id+'_mc');
    $timeline.setSelectedLayers(0);
    $timeline.pasteFrames();
}
function Delete_unused()
{
    _unused_arr=$library.unusedItems;
    for(var i in _unused_arr)
    {
        $library.deleteItem(_unused_arr[i].name);
    }
}
function Assemble()
{
    //reverse assembly for correct draworder
    for(var i=sprites_obj.sprites.length-1;i>=0;i--)
    {
        _id=sprites_obj.sprites[i].id;
        _state=sprites_obj.sprites[i].state;
        _layer=sprites_obj.sprites[i].layer;
        _uri=sprites_obj.sprites[i].name;
        _x=sprites_obj.sprites[i].x_pos;        
        _y=sprites_obj.sprites[i].y_pos;
        _container_name=_id+'_'+_state+'_mc';
        //assemble id
        if(_id!='none'&&_state!='none')
        {
            $library.editItem(_id);
            for(var j=0;j<$timeline.layers[0].frames.length;j++)
            {
                if($timeline.layers[0].frames[j].name==_state)
                {
                    $timeline.currentLayer=0;
                    $timeline.currentFrame=j;
                    if($timeline.layers[0].frames[j].elements[0]==undefined)
                    {
                        $library.addNewItem('movie clip',_container_name);
                        $library.addItemToDocument({x:0,y:0},_container_name);
                        if($timeline.layers[0].name!='states')
                        {
                            Create_control_mc(_id,_container_name,$timeline.layers[0].name);
                        }
                    }
                }
                $library.editItem(_container_name);
            }
        }else if(_id!='none'&&_state=='none'){
            trace('some functionality when: _id=="none"&&_state=="none"');
        }else if(_id=='none'&&_state=='none'){
            $library.editItem(main_item_name);
            _index_arr=$timeline.findLayerIndex(_layer);
            $timeline.currentLayer=_index_arr[0];
        }
        $library.addItemToDocument({x:0,y:0},_uri);
        Set_selection_position(_x,_y);
    }
}
//================================================================================== COMMON ==================================================================================
//Create CONTROL (button, checkbox)
function Create_control_struct(_lib_item, _states_arr, _control_type)
{
    for(var j=0;j<_states_arr.length;j++)
    {
        if(j==0)
        {
            _lib_item.timeline.layers[0].frames[j].name=_states_arr[j];
        }else{
            _lib_item.timeline.insertBlankKeyframe(j);
            _lib_item.timeline.layers[0].frames[j].name=_states_arr[j];
        }
    }
    _lib_item.timeline.addNewLayer("touch", "normal", true);
    _lib_item.timeline.reorderLayer(1,0);
    _lib_item.timeline.layers[1].frames[0].name='touch';
}
function Create_control_mc(_id, _container_name,_control_type)
{
    $library.editItem(_id);
    if(_control_type=='button')
    {
        for(var i=1;i<$timeline.layers[0].frames.length;i++)
        {
            $timeline.currentLayer=0;
            $timeline.currentFrame=i;
            _curr_frame=$timeline.layers[0].frames[i];
            
            switch(_curr_frame.name)
            {
                case 'down':
                    _curr_frame.actionScript='/*'+'\n'+'property:saturation,+50'+'\n'+'property:brightness,+70'+'\n'+'*/';
                    break;
                case 'dis':
                    $library.addItemToDocument({x:0,y:0},_container_name);
                    _curr_frame.actionScript='/*property:saturation,-90*/';
                    break;
                default:
                    break;
            }
        }
    }
}
function Correct_control_mc()
{
    for(var i=0;i<$library.items.length;i++)
    {
        if($library.items[i].itemType=='movie clip')
        {
            $library.editItem($library.items[i].name);
            $timeline.currentLayer=0;
            for(var j=0;j<$timeline.layers[0].frames.length;j++)
            {
                
                if($timeline.layers[0].frames[j].name=='down'&&$timeline.layers[0].name=='button')
                {
                    Duplicate_add_mc($library.items[i].name+'_up_mc',$library.items[i].name+'_down_mc',j);
                    Set_selection_position(0,7);
                    $library.editItem($library.items[i].name+'_down_mc');
                    $timeline.addNewLayer('sound','guide',true);
                    $timeline.layers[0].frames[0].actionScript=Set_mobile_property('sound');
                }else if($timeline.layers[0].frames[j].name=='up'&&$timeline.layers[0].name=='checkbox'){
                    Duplicate_add_mc($library.items[i].name+'_down_mc',$library.items[i].name+'_up_mc',j);
                    Set_selection_position(0,0);
                    $library.editItem($library.items[i].name+'_up_mc');
                    $timeline.currentFrame=0;
                    for(var z=0;z<$timeline.layers[0].frames[0].elements.length;z++)
                    {
                        if($timeline.layers[0].frames[0].elements[z].libraryItem.itemName=='check')
                        {
                            $timeline.layers[0].frames[0].elements[z].selected=true;
                            $dom.deleteSelection();
                        }
                    }
                }
            }
        }
    }
}
function Set_selection_position(_x,_y)
{
    $dom.selection[0].x=_x;
    $dom.selection[0].y=_y;
}
function Duplicate_add_mc(_mc_name,_new_mc_name,_curr_frame)
{
    $library.duplicateItem(_mc_name);
    $library.selectItem(_mc_name+' copy');
    $library.renameItem(_new_mc_name);
    $timeline.currentFrame=_curr_frame;
    $library.addItemToDocument({x:0,y:0},_new_mc_name);
}
function Set_mobile_property(_node)
{
    var _actionScript_str;
    switch(_node)
    {
        case 'sound':
            _actionScript_str='/*property:reset,true'+'\n'+'keepplaying:true*/';
            break;
        case 'text':
            _actionScript_str='/*'+'\n'+'property:VAlignment,Center'+'\n'+'property:DimensionSource, ProportionalSize'+'\n'+'property:DimensionSourceScale, DownOnly'+'\n'+'*/';
            break;
        default:
            _actionScript_str='stop();';
            break;
    }
    return _actionScript_str;
}
//================================================================================== COMMON end ==================================================================================
function Parse_psd()
{
    Remove_empty_layers();
    item_for_parse = Get_graphic_from_lib();
    var _root_layers=item_for_parse.timeline.layers;
    var _layers_for_parse;
    for(var i=0;i<item_for_parse.timeline.layers.length;i++)
    {
        var _root_layers_depth_0=_root_layers[i].frames[0].elements[0].libraryItem.timeline.layers;
        for(var j=0;j<popup_presets_OBJ[module_type].length;j++)
        {
            if(_root_layers[i].name==popup_presets_OBJ[module_type][j].id)
            {
                for(var k=0;k<_root_layers_depth_0.length;k++)
                {
                    actual_id=_root_layers[i].name;
                    switch(popup_presets_OBJ[module_type][j].type)
                    {
                        case 'states':
                            actual_label=_root_layers_depth_0[k].name;
                            to_layer='states';
                            break;
                        case 'button':
                            actual_label='up';
                            to_layer='button';
                            break;
                        case 'checkbox':
                            actual_label='down';
                            to_layer='checkbox';
                            break;
                        default:
                            break;
                    }
                    if(_root_layers_depth_0[k].frames[0].elements[0].libraryItem.timeline!=undefined)
                    {
                        Find_layers_bitmaps(_root_layers_depth_0[k].frames[0].elements[0].libraryItem.timeline.layers,_root_layers_depth_0[k].frames[0].elements[0].x+_root_layers[i].frames[0].elements[0].x,_root_layers_depth_0[k].frames[0].elements[0].y+_root_layers[i].frames[0].elements[0].y);
                    }else{
                        Find_layers_bitmaps(_root_layers_depth_0,_root_layers[i].frames[0].elements[0].x,_root_layers[i].frames[0].elements[0].y);
                    }
                    _root_layers[i].locked=true;
                }
            }
        }
        if(!_root_layers[i].locked)
        {
            actual_id='none';
            actual_label='none';
            to_layer=_root_layers[i].name;
            Find_layers_bitmaps(_root_layers_depth_0,_root_layers[i].frames[0].elements[0].x,_root_layers[i].frames[0].elements[0].y);
        }
    }
}
function Find_layers_bitmaps(_layers,_element_x,_element_y)//recursive search of bitmaps
{
    var _element;
    for(var i=0;i<_layers.length;i++)
    {
        if(_layers[i].frames[0].elements[0]==undefined)
        {
            break;
        }else{
            _element=_layers[i].frames[0].elements[0];
            if(_element.instanceType=='symbol')
            {
                Find_layers_bitmaps(_element.libraryItem.timeline.layers, _element_x+_element.x, _element_y+_element.y);
            }else if(_element.instanceType=='bitmap'){
                _sprite={'name':_element.libraryItem.name,'x_pos':_element_x+_element.x,'y_pos':_element_y+_element.y+y_pos_shift,'id':actual_id,'layer':to_layer,'state':actual_label};
                sprites_obj.sprites.push(_sprite);
            }
        }
    }
}
function Create_main_object(_name)
{
    $library.addNewItem('movie clip',_name);
    $timeline.layers[0].name=_name;
    if(module_type!='Paytable')
    {
        main_item_name='init_state_mc';
        $library.editItem(_name);
        $timeline.layers[0].name='states';
        $library.addNewItem('movie clip',main_item_name);
        switch(module_type)
        {
            case 'FS welcome':
                for(var i=0;i<_states_fs_welcome_arr.length;i++)
                {
                    if(i!=0)
                    {
                        $timeline.convertToBlankKeyframes(i);
                    }
                    $timeline.layers[0].frames[i].name=_states_fs_welcome_arr[i];
                }
                $timeline.currentFrame=0;
                $dom.addNewRectangle({left:0,top:0,right:1,bottom:1},0,false,true);
                $timeline.currentFrame=1;
                break;
            default:
                $timeline.layers[0].frames[0].name='init_state';
                $timeline.currentFrame=0;
                break;
        }
    }
    $library.addItemToDocument({x:0,y:0},main_item_name);
    $dom.exitEditMode();
    $library.addItemToDocument({x:0,y:0},_name);
    Set_selection_position(0,0);
    $library.editItem(main_item_name);
}
function Remove_empty_layers()
{
    for(var i=0;i<$library.items.length;i++)
    {
        if($library.items[i].itemType=='movie clip'||$library.items[i].itemType=='graphic')
        {
            for(var j=0;j<$library.items[i].timeline.layers.length;j++)
            {
                if($library.items[i].timeline.layers[j].frames[0].elements[0]==undefined)
                {
                    $library.items[i].timeline.deleteLayer(j);
                }
            }
        }
    }
}
function Get_graphic_from_lib()
{
    var _item_for_parse;
    for(var i=0;i<$library.items.length;i++)
    {
        if($library.items[i].itemType=='graphic')
        {
            _item_for_parse=$library.items[i];
            return _item_for_parse;
            break;
        }
    }
}
Main();