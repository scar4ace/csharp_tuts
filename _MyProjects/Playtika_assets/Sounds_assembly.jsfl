// INIT **************************************************************************************
xjsfl.init(this);
clear();

//VARS **************************************************************************************

//UI -----------------
var dynamicUI='';
var soundNamesArr=['no_sound'];
var folderURI='';
var uris=[];
var ui_dropdown_icons='';
var ui_dropdown_game='';
var ui_dropdown_anticipation='';
var idListArr;
    //hardcode
    idListArr=FLfile.read(xjsfl.uri + 'user/jsfl/snippets/_lib/Playtika_assets/idListTpl.json');
    idListArr=JSON.decode(idListArr);
//JSON -----------------
var soundsDataJson='';
var soundsDataObj={"icons":[],"game":[],"anticipation":[]};
//todo: Remove hardcode -----------------
var pathToJSON=xjsfl.uri + 'user/jsfl/snippets/_lib/Playtika_assets/Sounds_assembly_data.json';
var activeControls;
//work with JSON **************************************************************************************
function Create_soundsDataObj(_type_sound)
{
    switch (_type_sound)
    {
	case 'icons':
	    PushDataTo_soundsDataObj(idListArr.icons);
	    break;
	case 'game':
	    PushDataTo_soundsDataObj(idListArr.game);
	    break;
	case 'anticipation':
	    PushDataTo_soundsDataObj(idListArr.anticipation);
	    break;
	default:
	    trace('unsupported parametr in Create_soundsDataObj(_type_sound)');
    }
    JsonEncode();
}
function PushDataTo_soundsDataObj(_sounds_id)
{
	    var z=0;
	    for (var _key=_sounds_id[z]['id'] in activeControls)
	    {
		if(activeControls[_key]['value']!='no_sound'&&activeControls[_key]['value'])
		{
		    var _obj={"name":"","id":"","uri":""};
		    for(var i=0;i<uris.length;i++)
		    {
			_uri=new URI(uris[i]);
			if(_uri.name==activeControls[_key]['value'])
			{
			    _obj.uri=_uri.uri;
			}
		    }
		    _obj.name=activeControls[_key]['value'];
		    _obj.id=_sounds_id[z]['id'];
		    if(_sounds_id==idListArr.icons)
		    {
			soundsDataObj.icons.push(_obj);
		    }else if(_sounds_id==idListArr.game){
			soundsDataObj.game.push(_obj);
		    }else{
			soundsDataObj.anticipation.push(_obj);
		    }
		}
		z++;
	    }
}
function JsonEncode()
{
    soundsDataJson=JSON.encode(soundsDataObj);
    SaveJSON();
}
function SaveJSON()
{
    fileJson = new File(pathToJSON,'',true);;
    fileJson.write(soundsDataJson);
}
//XUL dynamic controls **************************************************************************************
//Create Dropdown Labels (sound ID) From JSON template -----------------
function CreateDropdownFromJSON()
{
    //todo:refactoring
    for(var i=0;i<idListArr.icons.length;i++)
    {
	ui_dropdown_icons+='dropdown:'+idListArr.icons[i]['id']+'=['+soundNamesArr+'],';
    }
    for(var i=0;i<idListArr.game.length;i++)
    {
	ui_dropdown_game+='dropdown:'+idListArr.game[i]['id']+'=['+soundNamesArr+'],';
    }
    for(var i=0;i<idListArr.anticipation.length;i++)
    {
	ui_dropdown_anticipation+='dropdown:'+idListArr.anticipation[i]['id']+'=['+soundNamesArr+'],';
    }
}
//XUL constructors **************************************************************************************
var xul_browse;
var xul_icons;
var xul_game;
var xul_anticipation;

function ConstructBrowseBtn()
{
    xul_browse = XUL
	.factory('title:Select folder with sounds')
	.setColumns([0,125])
	.setButtons('')
	.addButton('Browse','browse')
	.addEvent('browse','click', ClickBrowseHandler)
	.show();
}
function ConstructUiIcons()
{
    ui_buttons='button:Assembly Icons=assemblyicons';
    dynamicUI='title:Icons Sounds,'+ui_dropdown_icons+','+ui_buttons;
	
    xul_icons = XUL
	.factory(dynamicUI)	
	.setColumns([110,200])
	//disable base buttons
	.setButtons('')
	//events
	.addEvent('assemblyicons','click', ClickAssemblyIconsHandler)
	//.addEvent('initialize', onInitialize)
	.show();
}
function ConstructUiGame()
{
    ui_buttons='button:Assembly Game=assemblygame';
    dynamicUI='title:Game Sounds,'+ui_dropdown_game+','+ui_buttons;
	
    xul_game = XUL
	.factory(dynamicUI)	
	.setColumns([150,200])
	//disable base buttons
	.setButtons('')
	//events
	.addEvent('assemblygame', 'click', ClickAssemblyGameHandler)
	.show();
}
function ConstructUiAnticipation()
{
    ui_buttons='button:Assembly Anticipation=assemblyanticipation';
    dynamicUI='title:Anticipation Sounds,'+ui_dropdown_anticipation+','+ui_buttons;
	
    xul_anticipation = XUL
	.factory(dynamicUI)	
	.setColumns([150,200])
	//disable base buttons
	.setButtons('')
	//events
	.addEvent('assemblyanticipation', 'click', ClickAssemblyAnticipationHandler)
	.show();
}
//Event handlers **************************************************************************************
function onInitialize(event)
{
    trace('Initialize');
}
function ClickAssemblyIconsHandler(event)
{
    //DEBUG
    activeControls=this.controls;
    Create_soundsDataObj('icons');
    ConstructUiGame();
    this.close();
}
function ClickAssemblyGameHandler(event)
{
    //todo: ������ �� ������� uris + ������������ icons & game & anticipation to JSON
    activeControls=this.controls;
    Create_soundsDataObj('game');
    ConstructUiAnticipation();
    this.close();
}
function ClickAssemblyAnticipationHandler(event)
{
    //todo: ������ �� ������� uris + ������������ icons & game & anticipation to JSON
    //inspect(this.controls);
    activeControls=this.controls;
    Create_soundsDataObj('anticipation');
    
    //RUN IMPORT SCRIPT -----------------
    fl.runScript(xjsfl.uri + 'user/jsfl/snippets/_lib/Playtika_assets/ImportSounds.jsfl');
    this.close();
}
function ClickBrowseHandler(event)
{
    CreateFileList();
    CreateDropdownFromJSON();
    ConstructUiIcons();
    this.close();
}
//Recursive parser of folders -----------------
function CreateFileList()
{
    folderURI=fl.browseForFolderURL('Please select the folder with sounds')
    _rootFolder = new Folder (folderURI);
    ParseFoldersRecursion(_rootFolder.uris);
    for(var i=0;i<uris.length;i++)
    {
	_uri=new URI(uris[i]);
	soundNamesArr.push(_uri.name);
    }
}
function ParseFoldersRecursion(_folder)
{
    for(var i=0;i<_folder.length;i++)
    {
	var _resultFileList=[];
	if(_folder[i].lastIndexOf("/") == _folder[i].length-1)
	{
	    _subFolder=new Folder(_folder[i]);
	    ParseFoldersRecursion(_subFolder.uris);
	    trace[i];
	}else{
	    uris.push(_folder[i]);
	    trace[i];
	}
    }
}
//Start **************************************************************************************
ConstructBrowseBtn();


