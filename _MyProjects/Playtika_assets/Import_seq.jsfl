xjsfl.init(this);
clear();
var pathToJSON=xjsfl.uri + 'user/jsfl/snippets/_lib/Playtika_assets/Icons_assembly_data.json';
var decoded_json;
function Main()
{
    decoded_json=FLfile.read(pathToJSON);
    decoded_json=JSON.decode(decoded_json);
    $library.deleteItem($timeline.layers[$timeline.currentLayer].frames[$timeline.currentFrame].elements[0].libraryItem.itemName);
    $timeline.layers[$timeline.currentLayer].frames[$timeline.currentFrame].actionScript='/*videoquality:17*/';
    for(var i=0;i<decoded_json.icons.length;i++)
    {
        _img_uri = decoded_json.icons[i].uri;
        _seq_folder_resized = new Folder(_img_uri.substring(0,_img_uri.lastIndexOf('/'))+'/resized');
        $timeline.convertToKeyframes(0,_seq_folder_resized.uris.length);
        for(var j=0;j<_seq_folder_resized.uris.length;j++)
        {
            $timeline.currentFrame=j;
            $dom.importFile(_seq_folder_resized.uris[j]);
            $timeline.layers[0].frames[j].elements[0].x=decoded_json.icons[i].pos_x;
            $timeline.layers[0].frames[j].elements[0].y=decoded_json.icons[i].pos_y;
        }
    }
    $dom.selectNone();
}
Main();