xjsfl.init(this);
clear();
//VARS **************************************************************************************
var default_videoquality='17';
var anim_id_arr=[];
//XUL
var xul_assemble;
var xul_resize;
//JSON
var pathToJSON=xjsfl.uri + 'user/jsfl/snippets/_lib/Playtika_assets/Icons_assembly_data.json';
var decoded_json;
var encoded_json;
//JSON **************************************************************************************
function Decode_json()
{
    decoded_json=FLfile.read(pathToJSON);
    decoded_json=JSON.decode(decoded_json);
}
function Set_json(_json_index, _width, _height, _pos_x, _pos_y)
{
    var _icon_property = decoded_json.icons[_json_index];
    _icon_property.width=_width;
    _icon_property.height=_height;
    _icon_property.pos_x=_pos_x;
    _icon_property.pos_y=_pos_y;
}
function SaveJSON()
{
    encoded_json=JSON.encode(decoded_json);
    _fileJson = new File(pathToJSON,'',true);;
    _fileJson.write(encoded_json);
}
//Event handlers **************************************************************************************
function ClickResizeSequencesHandler(event)
{
    Decode_json();
    Get_icons_info();
    SaveJSON();
    fl.saveDocument(document);
    run_file = new File(xjsfl.uri + 'user/jsfl/snippets/_lib/Playtika_assets/resize_seq.jsx').open();
    alert('Wait until the resize in Photoshop is done, and then click Assemble');
    ConstructAssembleBtn();
    this.close();
}
function ClickAssembleHandler()
{
    $library.newFolder('_bitmaps');
    $library.newFolder('_atlas@8');
    $library.newFolder('_trash');
    Assemble_icons();
    Assemble_static();
    Decimate_sequences();
    Delete_unused();
    Sort_lib();
    $library.editItem('icons.object');
    fl.saveDocument(document);
    this.close();
 }
 
function Assemble_icons()
{
    for(var i=0;i<decoded_json.icons.length;i++)
    {
        _img_uri = decoded_json.icons[i].uri;
        _seq_folder_resized = new Folder(_img_uri.substring(0,_img_uri.lastIndexOf('/'))+'/resized');
        _anim_id =decoded_json.icons[i].id;
        _anim_id='anim_'+_anim_id.substring(5);
        $library.editItem(_anim_id);
        $timeline.convertToKeyframes(0,_seq_folder_resized.uris.length);
        
        for(var j=0;j<_seq_folder_resized.uris.length;j++)
        {
            $timeline.currentFrame=j;
            $dom.importFile(_seq_folder_resized.uris[j]);
            $timeline.layers[0].frames[j].elements[0].x=decoded_json.icons[i].pos_x;
            $timeline.layers[0].frames[j].elements[0].y=decoded_json.icons[i].pos_y;
            if(j==0)
            {
                $library.selectItem(decoded_json.icons[i].name);
                $library.moveToFolder('_bitmaps',decoded_json.icons[i].name);
                $dom.importFile(_seq_folder_resized.uris[j],true);
                $library.selectItem(decoded_json.icons[i].name);
                $library.renameItem(decoded_json.icons[i].name+'@8');
                $library.moveToFolder('_atlas@8',decoded_json.icons[i].name+'@8');
            }
        }
            $timeline.addNewLayer("stop","normal",true);
            $timeline.convertToKeyframes(_seq_folder_resized.uris.length-1);
            $timeline.layers[0].frames[_seq_folder_resized.uris.length-1].actionScript='stop();';
        }
}
function Decimate_sequences()
{
    for(var i=0;i<anim_id_arr.length;i++)
    {
        $library.editItem(anim_id_arr[i]);
        $timeline.currentLayer=1;
        _numLastFrame=$timeline.frameCount-1;
        for(var j=1;j<_numLastFrame;j+=2)
        {
            $timeline.clearKeyframes(j);
        }
    }
}
function Delete_unused()
{
    _unused_arr=$library.unusedItems;
    for(var i in _unused_arr)
    {
        $library.deleteItem(_unused_arr[i].name);
    }
}
function Sort_lib()
{
    for(var i=0;i<$library.items.length;i++)
    {
        var _item = $library.items[i];
        if(_item.itemType=='bitmap'&&_item.name.slice(-2)!='@8')
        {
            $library.moveToFolder('_bitmaps',_item.name);
        }
    }
    //HARDCODE
    $library.moveToFolder('_trash','slot.psd');
    $library.moveToFolder('_trash','slot.psd Asset');
}
function Assemble_static()
{
    for(var i=0;i<decoded_json.icons.length;i++)
    {
        _name=decoded_json.icons[i].name;
        _id=decoded_json.icons[i].id;
        $library.editItem(_id);
        $library.addItemToDocument({x:0,y:0},'_atlas@8/'+_name+'@8');
        $timeline.layers[0].frames[0].elements[0].x=decoded_json.icons[i].pos_x;
        $timeline.layers[0].frames[0].elements[0].y=decoded_json.icons[i].pos_y;
    }
}
function Get_icons_info()
{
    var j=0;
    for(var i=0;i<$library.items.length;i++)
    {
        if($library.items[i].itemType=='movie clip')
        {
            _type_mc=$library.items[i].name.substring(0,5);
            _name_mc=$library.items[i].name;
            
            switch(_type_mc)
            {
                case 'icon_':
                    _root_frame = Get_selected_item(_name_mc,0);
                    _root_frame.timeline.layers[0].name = 'static icon';
                    _width=Math.round(_root_frame.timeline.layers[0].frames[0].elements[0].width);
                    _height=Math.round(_root_frame.timeline.layers[0].frames[0].elements[0].height);
                    _pos_x=Math.round(_root_frame.timeline.layers[0].frames[0].elements[0].x);
                    _pos_y=Math.round(_root_frame.timeline.layers[0].frames[0].elements[0].y);
                    
                    Set_json([j], _width, _height, _pos_x, _pos_y);
                    $library.deleteItem(decoded_json.icons[j].name);
                    j++;
                    break;
                case 'anim_':
                    _root_frame = Get_selected_item(_name_mc,0);
                    _root_frame.timeline.layers[0].name = _name_mc+' video';
                    _root_frame.timeline.layers[0].frames[0].actionScript = '/*videoquality:'+default_videoquality+'*/';
                    anim_id_arr.push(_name_mc);
                    break;
                default:
                    break;
            }
        }
    }
}
function Get_selected_item(_lib_name, _frame)
{    
    _select_item=$library.selectItem(_lib_name);
    _selected_item=$library.getSelectedItems();
    _root_frame=_selected_item[_frame];
    return _root_frame;
}
//XUL constructors **************************************************************************************
function ConstructResizeBtn()
{
    xul_resize = XUL
	.factory()
        .setTitle('Resize sequences')
	.setColumns([0,150])
	.setButtons('')
	.addButton('Resize sequences','resizesequences')
	.addEvent('resizesequences','click', ClickResizeSequencesHandler)
	.show();
}
function ConstructAssembleBtn()
{
    xul_assemble = XUL
	.factory()
        .setTitle('Assemble icons.object')
	.setColumns([0,150])
	.setButtons('')
	.addButton('Assemble','assemble')
	.addEvent('assemble','click', ClickAssembleHandler)
	.show();
}
//Run **************************************************************************************
function Main()
{
    ConstructResizeBtn();
}
Main();