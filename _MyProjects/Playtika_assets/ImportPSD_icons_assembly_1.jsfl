xjsfl.init(this);
fl.createDocument("timeline");
$dom.width=3408;
$dom.height=1920;
clear();

//GUI
var xul_browse;
var xul_icons;

var psd_uri;
var seq_uri;
var select_item;
var selected_item;
var timeline_selected;
var selected_index;
var root_layers;
var uris=[];
var iconNamesArr=['no_video'];
var is_psd_loaded=false;
var ui_dropdown_icons='';
var slot_holder_info={'slot_start':[0,0,0,0],'slot_end':[0,0,0,0]};//x,y,w,h
var idListArr;
    //hardcode
    idListArr=FLfile.read(xjsfl.uri + 'user/jsfl/snippets/_lib/Playtika_assets/idListTpl_icons_obj.json');
    idListArr=JSON.decode(idListArr);
var activeControls;    
//JSON -----------------
var iconsDataJson='';
var iconsDataObj={"icons":[]};

var pathToJSON=xjsfl.uri + 'user/jsfl/snippets/_lib/Playtika_assets/Icons_assembly_data.json';


//Event handlers **************************************************************************************
function ClickBrowseHandler(event)
{
    if(is_psd_loaded)
    {
        seq_uri=fl.browseForFolderURL('Select icon sequences root folder');
        CreateFileList();
        CreateDropdownFromJSON();
        ConstructUiIcons();
        this.close();
    }else{
        psd_uri=fl.browseForFileURL("select", "Select *.psd", "PSD Document (*.psd)", "psd");
        $dom.importFile(psd_uri,true);
        is_psd_loaded=true;
        CreateObject();
    }
}
function ClickAssignVideoHandler(event)
{
    activeControls=this.controls;
    Create_iconsDataObj('icons');    
    Create_mc_from_iconsDataObj();
    this.close();
}
function Create_mc_from_iconsDataObj()
{
    for(var i=0;i<iconsDataObj.icons.length;i++)
    {
        var _icons=iconsDataObj.icons[i];
        
        anim_id='anim_'+_icons.id.slice(5);
        
        Create_movieclip("movie clip",_icons.id);
        $timeline.addNewLayer(_icons.id,"normal",true);
        Add_item_in_mc('icons.object',_icons.id);
        
        $library.editItem(_icons.id);
        $dom.importFile(_icons.uri);
        
        $timeline.layers[0].frames[0].elements[0].x=0;
        $timeline.layers[0].frames[0].elements[0].y=0;
        
        $library.editItem('icons.object');
        
        Create_movieclip("movie clip",anim_id);
        $timeline.addNewLayer(anim_id,"normal",true);
        Add_item_in_mc('icons.object',anim_id);
        
    }
    alert('Change the size and position of static icons, then run - Icons object assemble - Step 2');
    fl.saveDocument(document);
}
function Import_static_bitmap(_container,_content_uri)
{
    $library.editItem(_container);
    $dom.importFile(_content_uri);
}
function Add_anim_properties()
{
    
}
function Create_iconsDataObj(_type_sound)
{
    switch (_type_sound)
    {
	case 'icons':
	    PushDataTo_iconsDataObj(idListArr.icons);
	    break;
	default:
	    trace('unsupported parametr in Create_soundsDataObj(_type_sound)');
    }
    JsonEncode();
}
function PushDataTo_iconsDataObj(_icons_id)
{
	    var z=0;
	    for (var _key=_icons_id[z]['id'] in activeControls)
	    {
		if(activeControls[_key]['value']!='no_video'&&activeControls[_key]['value'])
		{
		    var _obj={"name":"","id":"","uri":"","width":0,"height":0,"pos_x":0,"pos_y":0};
		    for(var i=0;i<uris.length;i++)
		    {
			_uri=new URI(uris[i]);
			if(_uri.name==activeControls[_key]['value'])
			{
			    _obj.uri=_uri.uri;
			}
		    }
		    _obj.name=activeControls[_key]['value'];
		    _obj.id=_icons_id[z]['id'];
		    if(_icons_id==idListArr.icons)
		    {
			iconsDataObj.icons.push(_obj);
		    }else if(_icons_id==idListArr.game){
			iconsDataObj.game.push(_obj);
		    }else{
			iconsDataObj.anticipation.push(_obj);
		    }
		}
		z++;
	    }
    //inspect(iconsDataObj);
}
function JsonEncode()
{
    iconsDataJson=JSON.encode(iconsDataObj);
    SaveJSON();
}
function SaveJSON()
{
    fileJson = new File(pathToJSON,'',true);;
    fileJson.write(iconsDataJson);
}
//Work in flash **************************************************************************************
function Add_item_in_mc(_lib_name_container,_lib_name_content,_lib_name_content_uri)
{
    //enter to edit mode
    //if($library.editItem(_lib_name_container==false))
    //{
    //    $library.editItem(_lib_name_container);
    //}
    $library.editItem(_lib_name_container);
    //select item in lib
    lib_item_index = $library.findItemIndex(_lib_name_content);
    lib_item = $library.items[lib_item_index];
    //add selected item to active timeline
    $dom.addItem({
            x: 0,
            y: 0
    }, lib_item);
    //add instanse name for mc
    $timeline.layers[0].frames[0].elements[0].name = _lib_name_content;
}
function Create_movieclip(_type,_lib_name)
{
    //create MC in lib
    $library.addNewItem(_type,_lib_name);
}
//Recursive parser of folders -----------------
function CreateFileList()
{
    var _rootFolder = new Folder (seq_uri);
    ParseFoldersRecursion(_rootFolder.uris);
    for(var i=0;i<uris.length;i++)
    {
	_uri=new URI(uris[i]);
	iconNamesArr.push(_uri.name);
    }
    //inspect(iconNamesArr);
}
function ParseFoldersRecursion(_folder)
{
    for(var i=0;i<_folder.length;i++)
    {
	var _resultFileList=[];
	if(_folder[i].lastIndexOf("/") == _folder[i].length-1)
	{
	    _subFolder=new Folder(_folder[i]);
	    ParseFoldersRecursion(_subFolder.uris);
	    //trace[i];
	}else if(i==0){
	    uris.push(_folder[i]);
	}else{
            
        }
    }
    //inspect(uris);
}
function CreateObject() {
    
    $library.addNewItem("movie clip", "icons.object");
    $library.addItemToDocument({x:0,y:0}, "icons.object");        
    //choosing mc properties without edit mode
    root_frame = Get_selected_item('icons.object',0);
    root_frame.timeline.addNewLayer("slot_preview","guide",true);
    AddToStage();
}
function Get_selected_item(_lib_name, _frame)
{    
    select_item=$library.selectItem(_lib_name);
    selected_item=$library.getSelectedItems();
    root_frame=selected_item[_frame];
    return root_frame;
}
function AddToStage()
{
    FlattenBkgInPreview();
    $library.editItem("icons.object");
    selected_index=$library.findItemIndex('slot.psd');
    selected_item=$library.items[selected_index];
    $dom.addItem({x:$dom.width/2,y:$dom.height/2},selected_item);
    Move_preview_to_zero();    
}
function FlattenBkgInPreview()
{
    $library.editItem('slot.psd');
    
    root_frame=Get_selected_item('slot.psd',0);
    root_layers=root_frame.timeline.layers;
    //lock layers
    for(var i=0;i<root_layers.length;i++)
    {
        if(root_layers[i].name=='icons_holder')
        {
            root_layers[i].locked=true;
	    _layer_index=root_frame.timeline.findLayerIndex('icons_holder');
	    root_frame.timeline.reorderLayer(_layer_index[0],0);
        }
    }
    //convert all except icon holders layers to bitmap
    $timeline.selectAllFrames();
    $dom.convertSelectionToBitmap();
    //unlock layers
    for(var i=0;i<root_layers.length;i++)
    {
        root_layers[i].locked=false;
    }
    Set_icon_holder_info();
}
function Set_icon_holder_info()
{
    layer_index=selected_item[0].timeline.findLayerIndex("slot_start");
    layer_elements=selected_item[0].timeline.layers[layer_index].frames[0].elements;
    slot_holder_info.slot_start[0]=layer_elements[0].left;
    slot_holder_info.slot_start[1]=layer_elements[0].top;
    slot_holder_info.slot_start[2]=layer_elements[0].width;
    slot_holder_info.slot_start[3]=layer_elements[0].height;
    layer_index=selected_item[0].timeline.findLayerIndex("slot_end");
    layer_elements=selected_item[0].timeline.layers[layer_index].frames[0].elements;
    slot_holder_info.slot_end[0]=layer_elements[0].left;
    slot_holder_info.slot_end[1]=layer_elements[0].top;
    slot_holder_info.slot_end[2]=layer_elements[0].width;
    slot_holder_info.slot_end[3]=layer_elements[0].height;
    //inspect(slot_holder_info);    
}
function Move_preview_to_zero()
{
    $library.editItem('icons.object');
    root_frame=Get_selected_item('icons.object',0);
    layer_index=root_frame.timeline.findLayerIndex("slot_preview");
    layer_elements=root_frame.timeline.layers[layer_index].frames[0].elements;
    layer_elements[0].x=-slot_holder_info.slot_start[0];
    layer_elements[0].y=-slot_holder_info.slot_start[1];
    layer_elements[0].selected=false;
    //HARDCODE
    layer_index=root_frame.timeline.findLayerIndex("Layer 1");
    root_frame.timeline.deleteLayer(1);
}
//XUL constructors **************************************************************************************
function ConstructBrowseBtn()
{
    xul_browse = XUL
	.factory()
        .setTitle('select PSD, then SEQ root folder')
	.setColumns([0,150])
	.setButtons('')
	.addButton('Browse','browse')
	.addEvent('browse','click', ClickBrowseHandler)
	.show();
}
//Create Dropdown Labels (sound ID) From JSON template -----------------
function CreateDropdownFromJSON()
{
    for(var i=0;i<idListArr.icons.length;i++)
    {
	ui_dropdown_icons+='dropdown:'+idListArr.icons[i]['id']+'=['+iconNamesArr+'],';
    }
}
function ConstructUiIcons()
{
    ui_buttons='button:Assign video=assignvideo';
    dynamicUI='title:Icons Sequences,'+ui_dropdown_icons+','+ui_buttons;
	
    xul_icons = XUL
	.factory(dynamicUI)	
	.setColumns([110,200])
	//disable base buttons
	.setButtons('')
	//events
	.addEvent('assignvideo','click', ClickAssignVideoHandler)
	//.addEvent('initialize', onInitialize)
	.show();
}
//Run **************************************************************************************
function Main()
{
    ConstructBrowseBtn();
}
Main();