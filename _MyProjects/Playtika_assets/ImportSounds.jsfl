﻿xjsfl.init(this);
fl.outputPanel.clear();
fl.createDocument("timeline");

var dom = fl.getDocumentDOM();
var t = dom.getTimeline();
var l = t.layers;
var lib = dom.library;

function ImportJSON() {
	//Import json	
	fileURL = xjsfl.uri + "user/jsfl/snippets/_lib/Playtika_assets/Sounds_assembly_data.json";
	if (!fileURL) return;
	var jsonData = FLfile.read(fileURL);
	var o = JSON.decode(jsonData);
	fl.trace("Import json - Done");

	return o;
}

function CreateObject() {
	lib.addNewItem("movie clip", "sounds.object");
	lib.addItemToDocument({
		x: 0,
		y: 0
	}, "sounds.object");
}

function CreateMovieClips(nameIndex) {

	lib.addNewItem("movie clip", nameIndex);

	lib.editItem("sounds.object");
	var tl = dom.getTimeline();
	tl.addNewLayer(nameIndex, "normal", true);
	selectedIndex = lib.findItemIndex(nameIndex);
	selectedItem = lib.items[selectedIndex];
	dom.addItem({
		x: 0,
		y: 0
	}, selectedItem);

	tl.layers[0].frames[0].elements[0].name = nameIndex;

	fl.trace("object created " + nameIndex);
}

function CreateSounds(jsonData, nameIndex) {
	CreateMovieClips(nameIndex);
	for (var i in jsonData[nameIndex]) {
		// vars		
		var folderName = "_" + nameIndex;
		var itemId = jsonData[nameIndex][i].id;
		var itemName = jsonData[nameIndex][i].name;
		var itemURI = jsonData[nameIndex][i].uri;
		var itemLibPath = folderName + "/" + itemId;


		//import sound	
		dom.importFile(itemURI, true);
		fl.trace("Import sound - Done")

		//create folder and mc in it
		var folderIndex = lib.findItemIndex(folderName);
		if (!folderIndex)
			lib.newFolder(folderName);
		lib.addNewItem("movie clip", itemLibPath);
		fl.trace("Create folder and mc in it - Done")

		//add sound to movie clip
		lib.editItem(itemLibPath);
		var tl = dom.getTimeline();
		var sqIndex = lib.findItemIndex(itemName);
		tl.layers[0].frames[0].soundLibraryItem = lib.items[sqIndex];
		
		//add movie clip to timeline in mc
		lib.editItem(nameIndex);
		var tl = dom.getTimeline();
		tl.addNewLayer(itemId, "normal", true);
		selectedIndex = lib.findItemIndex(itemLibPath);
		selectedItem = lib.items[selectedIndex];
		dom.addItem({
			x: 0,
			y: 0
		}, selectedItem);

		////add instanceName	
		tl.layers[0].frames[0].elements[0].name = itemId;
	}
}

function Main() {

	var jsonData = ImportJSON();

	CreateObject();

	for (var nameIndex in jsonData)
		CreateSounds(jsonData, nameIndex);
}

Main();