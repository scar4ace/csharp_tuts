﻿package  {
	
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import ScreenManager;
	
	import Achievements;
	
	public class Achievements extends MovieClip {
		
		public var nameOwner:String;
		public var myShop:Shop;
		
		public var isEnemyOwner:Boolean = true;

		public function Achievements(_name:String, _shop:Shop) {
			nameOwner = _name;
			myShop = _shop;
			visible = false;
			
			//event listeners
			shopBtn.addEventListener(MouseEvent.CLICK, ShopBtnClickListener, false, 0, true);
			
			AddToStage();
			
		}
		private function AddToStage():void
		{
			Main._mainLink.addChildAt(this,0);
		}
		//обработчики событий
		private function ShopBtnClickListener(e:MouseEvent):void
		{
			ScreenManager._instance.ShowScreen(myShop);
		}
	}	
}
