﻿package {
	
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import flash.geom.Vector3D;
	
	import ScreenManager;
	import GameVars;
	import Main;
	
	
	public class Popup_enterName extends MovieClip {
		
		public function Popup_enterName() {
/*			//position
			x = Main._mainLink.stage.width/2;
			y = Main._mainLink.stage.height/2;
*/			
			okBtn.addEventListener(MouseEvent.CLICK, okBtnClickHandler, false, 0, true);
			visible = false;
			AddToStage();
		}
		private function AddToStage():void
		{
			Main._mainLink.addChild(this);
			ScreenManager._instance.ShowPopup(this);
			playerName.text = '';
			errorText.text = '';
		}
		private function okBtnClickHandler(e:MouseEvent):void
		{
			if (playerName.text)
			{
				GameVars._instance.playerName = playerName.text;
				ScreenManager._instance.HidePopup(this);
				Main._mainLink.InitGame();
				Destroy();
			}else {
				errorText.text = 'Please enter correct name';
			}
			
		}
		private function Destroy():void
		{
			Main._mainLink.removeChild(this);
		}
	}
	
}
