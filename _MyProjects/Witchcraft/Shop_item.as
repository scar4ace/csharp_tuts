﻿package
{
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import Shop;
	import GameVars;
	
	public class Shop_item extends MovieClip
	{
		
		public var item_type:String = 'no_type';
		public var item_name:String = 'empty';
		
		public var quantity:int = 0;
		private var price:int = 0;
		private var empty:Boolean = true;
		private var action_type:String = '';
		
		//айдишник парента на сцене
		private var item_id:int = int(name.charAt(name.length - 1));
		
		public function Shop_item()
		{
			//Events listener
			hit.addEventListener(MouseEvent.MOUSE_OVER, Shop_item_OVER_handler, false, 0, true);
			hit.addEventListener(MouseEvent.MOUSE_OUT, Shop_item_OUT_handler, false, 0, true);
			hit.addEventListener(MouseEvent.CLICK, Shop_item_CLICK_handler, false, 0, true);
			addEventListener(Event.ENTER_FRAME, Update, false, 0, true);
			
			if (MovieClip(MovieClip(parent).parent) is Shop)
			{
				action_type = 'sell';
			}
			else if (MovieClip(MovieClip(parent).parent) is Backpack)
			{
				action_type = 'buy';
			}
			Set_skin();
			Set_price();
			Init_array();
		}
		
		// shop item settings
		public function Set_shop_item(_item_type:String, _item_name:String, _quantity:int,_new_price:int)
		{
			if (empty)
			{
				empty = false;
			}
			quantity += _quantity;
			quantity_txt.text = String(quantity);
			Set_type(_item_type);
			Set_name(_item_name);
			Set_info();
			//GameVars._instance.Set_median_price(_item_name, _new_price);
			Set_price();
			Set_skin();
			//trace(quantity);
		}
		
		private function Set_type(_type:String)
		{
			if (empty)
			{
				item_type = 'no_type';
			}
			else
			{
				item_type = _type;
			}
		}
		
		private function Set_price()
		{
			price = GameVars._instance.Get_median_price(item_name);
/*			if (!empty)
			{
				price = GameVars._instance.Get_median_price(item_name);
			}
			else if (empty)
			{
				price = 0;
			}
*/			price_txt.text = String(price);
			//Event_channel._instance.Dispatch_Event(Event_channel.PRICE_CHANGED);
		}
		
		public function Set_quantity(_quantity:int)
		{
			quantity = _quantity;
			if (quantity == 0)
			{
				empty = true;
			}
			quantity_txt.text = String(quantity);
			Set_name(item_name);
			Set_price();
			Set_skin();
		}
		
		private function Set_name(_name:String)
		{
			if (empty)
			{
				item_name = 'empty';
			}
			else
			{
				item_name = _name;
			}
		}
		
		private function Set_skin()
		{
			if (!empty)
			{
				face_mc.gotoAndStop(item_name);
			}
			else
			{
				face_mc.gotoAndStop('empty');
			}
		}
		
		private function Set_info()
		{
			info_over.item_info.txt.text = item_name;
		}
		
		//инициализация массива shop item
		private function Init_array()
		{
			MovieClip(parent).arr_items[item_id] = this;
			if (MovieClip(parent).name == 'elements')
			{
				item_type = 'element';
			}
			else if (MovieClip(parent).name == 'poisons')
			{
				item_type = 'poison';
			}
			else
			{
				item_type = 'antidote';
			}
		}
		
		//Event handlers
		private function Shop_item_OVER_handler(e:MouseEvent)
		{
			if (!empty)
			{
				info_over.play();
			}
		
		}
		
		private function Shop_item_OUT_handler(e:MouseEvent)
		{
			if (info_over.currentFrame != 1)
			{
				info_over.gotoAndStop('hide');
				info_over.play();
			}
		
		}
		
		private function Shop_item_CLICK_handler(e:MouseEvent)
		{
			//trace(MovieClip(MovieClip(parent).parent));
			
			if (!empty)
			{
				if (GameVars._instance.is_player_buyer && GameVars._instance.is_player_seller)
				{
					ScreenManager._instance.popups_arr['popup_add_to_backpack'].Set_popup(item_name, item_type, quantity, action_type);
					ScreenManager._instance.ShowPopup(ScreenManager._instance.popups_arr['popup_add_to_backpack']);
				}
				else
				{
					ScreenManager._instance.popups_arr['popup_deal'].Set_popup(item_name, item_type, quantity, price, action_type);
					ScreenManager._instance.ShowPopup(ScreenManager._instance.popups_arr['popup_deal']);
				}
			}
		}
		
		private function Update(e:Event)
		{
		
		}
	
	}

}
