﻿package
{
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.utils.Dictionary;
	import Main;
	import ScreenManager;
	import GameVars;
	import Recruiting;
	import Backpack;
	import Elements;
	import Antidotes;
	import Poisons;
	
	public class Shop extends MovieClip
	{
		
		public var myKitchen:Kitchen;
		public var myMapItem:Map_item;
		public var myAchievements:Achievements;
		public var myRecruiting:Recruiting;
		
		public var myBackpack:Backpack;
		
		public var isEnemyOwner:Boolean = true;//флаг для лока лавки
		public var nameOwner:String;//заменить на тип игрока/врага
		
		public function Shop(name:String, _mapItem:Map_item)
		{
			nameOwner = name;
			nameBuyer_txt.text = nameOwner + " shop";//текстовое поле в мувике
			lock_kitchenBtn.visible = true;
			
			if (nameOwner == GameVars._instance.playerName)
			{
				isEnemyOwner = false;
				lock_kitchenBtn.visible = false;
			}
			visible = false;
			AddToStage();
			
			backpack_holder.visible = false;
			
			//event listeners
			//mapBtn.addEventListener(MouseEvent.CLICK, MapBtnClickListener, false, 0, true);
			kitchenBtn.addEventListener(MouseEvent.CLICK, KitchenBtnClickListener, false, 0, true);
			achievmentsBtn.addEventListener(MouseEvent.CLICK, AchievementsBtnHandler, false, 0, true);
			recruitingBtn.addEventListener(MouseEvent.CLICK, RecruitingBtnHandler, false, 0, true);
			addEventListener(Event.ENTER_FRAME, Update, false, 0, true);
			
			Event_channel._instance.addEventListener(Event_channel.PLAYER_BUYER, PLAYER_BUYER_handler, false, 0, true);
			
			AssignKitchen();
			AssigneAchievements();
			AssigneRecruiting();
			Create_backpack();
			nameSeller_txt.text = GameVars._instance.arrBackpack['Player'].nameOwner + ' backpack';
			AddToArrShops();
		
		}
		
		private function AddToStage():void
		{
			Main._mainLink.addChildAt(this, 0);
		}
		
		public function AssignKitchen():void
		{
			myKitchen = new Kitchen(nameOwner, this);
		}
		
		public function AssigneRecruiting():void
		{
			myRecruiting = new Recruiting(nameOwner, this);
		}
		
		public function AssigneAchievements():void
		{
			myAchievements = new Achievements(nameOwner, this);
		}
		
		//Event handlers
		private function KitchenBtnClickListener(e:MouseEvent):void
		{
			if (!isEnemyOwner)
			{
				ScreenManager._instance.ShowScreen(myKitchen);
			}
		}
		
		private function AchievementsBtnHandler(e:MouseEvent):void
		{
			ScreenManager._instance.ShowScreen(myAchievements);
		}
		
		private function RecruitingBtnHandler(e:MouseEvent):void
		{
			ScreenManager._instance.ShowScreen(myRecruiting);
		}
		
		//Backpack
		private function Create_backpack()
		{
			myBackpack = new Backpack(backpack_holder, this);
		}
		
		private function AddToArrShops()
		{
			if (isEnemyOwner)
			{
				Main._mainLink.map.arrShops[nameOwner] = this;
			}
			else
			{
				Main._mainLink.map.arrShops['Player'] = this;
			}
		}
		
		private function PLAYER_BUYER_handler(e:Event):void
		{
			if (visible)
			{
				if (isEnemyOwner && contains(GameVars._instance.arrBackpack['Player']))
				{
					myBackpack.visible = false;
					GameVars._instance.arrBackpack['Player'].visible = true;
				}
				addChild(GameVars._instance.arrBackpack['Player']);
			}
		}
		
		private function Update(e:Event)
		{
		
		}
	}

}
