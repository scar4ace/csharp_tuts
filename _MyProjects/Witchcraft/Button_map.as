﻿package  {
	
	import flash.display.SimpleButton;
	import flash.events.MouseEvent;
	import ScreenManager;
	
	
	public class Button_map extends SimpleButton {
		
		
		public function Button_map() {
			addEventListener(MouseEvent.CLICK, MapBtnClickListener, false, 0, true);
		}
		private function MapBtnClickListener(e:MouseEvent):void
		{
			ScreenManager._instance.ShowScreen(Main._mainLink.map);
		}
	}
	
}
