﻿package  {
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	
	public class MenuBottom extends MovieClip {
		
		public var money:int=GameVars._instance.default_money;
		public var daytime_left:int=24;
		
		public function MenuBottom(_x:int,_y:int) {
			x = _x;
			y = _y;
			cureBtn.addEventListener(MouseEvent.CLICK, CureBtn_CLICK_handler, false, 0, true);
			baneBtn.addEventListener(MouseEvent.CLICK, BaneBtn_CLICK_handler, false, 0, true);
			AddToStage();
			addEventListener(Event.ENTER_FRAME, Update, false, 0, true);
		}
		private function AddToStage():void
		{
			Main._mainLink.addChildAt(this,1);
		}
		
		private function CureBtn_CLICK_handler(e:MouseEvent):void
		{
			trace("click");
		}
		private function BaneBtn_CLICK_handler(e:MouseEvent):void
		{
			trace("click");
		}
		private function Update(e:Event):void
		{
			money_txt.text = String(money);
			daytime_txt.text = String(daytime_left);
			
		}
	}
	
}
