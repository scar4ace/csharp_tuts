﻿package  {
	
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import Main;
	import ScreenManager;
	import Kitchen;
	
	public class BuyRecipe extends MovieClip {
		
		public var nameOwner:String;
		public var myKitchen:Kitchen;
		
		public var isEnemyOwner:Boolean = true;

		public function BuyRecipe(_name:String, _kitchen:Kitchen) {
			
			nameOwner = _name;
			myKitchen = _kitchen;
			
			visible = false;
			AddToStage();
			
			//event listeners
			kitchenBtn.addEventListener(MouseEvent.CLICK, KitchenBtnClickHandler, false, 0, true);
		}
		private function AddToStage():void
		{
			Main._mainLink.addChildAt(this,0);
		}
		private function KitchenBtnClickHandler(e:MouseEvent):void
		{
			ScreenManager._instance.ShowScreen(myKitchen);
		}
	}	
}
