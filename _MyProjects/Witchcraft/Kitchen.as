﻿package  {
	
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import Main;
	import ScreenManager;
	import BuyRecipe;
	
	public class Kitchen extends MovieClip {
		
		public var nameOwner:String;
		public var myShop:Shop;
		public var myBuyRecipe:BuyRecipe;
		
		public var isEnemyOwner:Boolean = true;
		
		public function Kitchen(_name:String,_shop:Shop) 
		{
			nameOwner = _name;
			myShop = _shop;
			visible = false;
			AssignBuyRecipe();
			AddToStage();

			//event listeners
			shopBtn.addEventListener(MouseEvent.CLICK, ShopBtnClickHandler, false, 0, true);
			buyRecipeBtn.addEventListener(MouseEvent.CLICK, BuyRecipeBtnHandler, false, 0, true);

			//trace('kitchen of ' + nameOwner+' was created');
		}
		private function AddToStage():void
		{
			Main._mainLink.addChildAt(this,0);
		}
		private function AssignBuyRecipe():void
		{
			myBuyRecipe = new BuyRecipe(nameOwner, this);
		}
		
		//обработчики событий
		private function ShopBtnClickHandler(e:MouseEvent):void
		{
			ScreenManager._instance.ShowScreen(myShop);
		}
		private function BuyRecipeBtnHandler(e:MouseEvent):void
		{
			ScreenManager._instance.ShowScreen(myBuyRecipe);
		}
	}
}
