﻿package
{
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import Main;
	import ScreenManager;
	
	public class Map_item extends MovieClip
	{
		
		public var myShop:Shop;
		
		public function Map_item():void
		{
			hit.addEventListener(MouseEvent.CLICK, MouseClickListener, false, 0, true);
		}
		
		public function AssignShop(name:String):void
		{
			myShop = new Shop(name, this);
			//trace('shop was created');
		}
		
		//обработчики событий
		private function MouseClickListener(e:MouseEvent):void
		{
			ScreenManager._instance.ShowScreen(myShop);
			GameVars._instance.active_shop = myShop;
			GameVars._instance.active_backpack = Main._mainLink.map.arrShops['Player'].myBackpack;
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			trace('Shop-' + GameVars._instance.active_shop.nameOwner + ', ' + 'Backpack-' + GameVars._instance.active_backpack.nameOwner);
			if (MovieClip(this).name == 'item_0')
			{
				Event_channel._instance.Dispatch_Event(Event_channel.PLAYER_BUYER);
				Event_channel._instance.Dispatch_Event(Event_channel.PLAYER_SELLER);
			}else {
				Event_channel._instance.Dispatch_Event(Event_channel.PLAYER_BUYER);
				GameVars._instance.is_player_seller = false;
			}
		}
	
	}
}
