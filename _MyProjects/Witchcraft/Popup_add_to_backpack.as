﻿package
{
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import ScreenManager;
	
	//import Model;
	
	public class Popup_add_to_backpack extends MovieClip
	{
		
		private var max_quantity:int = 0;
		private var total_quantity:int = 0;
		private var item_type:String = '';
		private var item_name:String = '';
		private var action_type:String = '';
		private var check:Shop_item;
		
		public function Popup_add_to_backpack()
		{
			visible = false;
			addBtn.addEventListener(MouseEvent.CLICK, AddBtn_CLICK_handler, false, 0, true);
			cancelBtn.addEventListener(MouseEvent.CLICK, CancelBtn_CLICK_handler, false, 0, true);
			quantity_mc.increaseBtn.addEventListener(MouseEvent.CLICK, IncreaseQuantityClick_handler, false, 0, true);
			quantity_mc.decreaseBtn.addEventListener(MouseEvent.CLICK, DecreaseQuantityClick_handler, false, 0, true);
			
			//Error text
			errorText.text = 'No free slots!';
			errorText.visible = false;
			
			//quantity_mc.txt.text = '0';
			AddToStage();
			
		}
		
		public function Set_popup(_name:String, _type:String, _max_quantity:int, _action_type:String)
		{
			action_type = _action_type;
			max_quantity = _max_quantity;
			item_name = _name;
			itemName.text = 'Item: ' + _name;
			item_type = _type;
			Update_text();
			//trace(item_type);
		}
		
		public function AddToStage()
		{
			Main._mainLink.addChild(this);
		}
		
		public function Update_text()
		{
			quantity_mc.txt.text = total_quantity.toString();
		}
		
		private function Clear_data():void
		{
			max_quantity = 0;
			total_quantity = 0;
			errorText.visible = false;
		}
		
		//handlers
		private function AddBtn_CLICK_handler(e:MouseEvent):void
		{
			var backpack:MovieClip;
			var shop:MovieClip;
			if (total_quantity > 0)
			{
				switch (item_type)
				{
				case 'element': 
					backpack = GameVars._instance.active_backpack.elements;
					shop = GameVars._instance.active_shop.elements;
					break;
				case 'poison': 
					backpack = GameVars._instance.active_backpack.poisons;
					shop = GameVars._instance.active_shop.poisons;
					break;
				case 'antidote': 
					backpack = GameVars._instance.active_backpack.antidotes;
					shop = GameVars._instance.active_shop.antidotes;
					break;
				default: 
					trace('default case action');
				}
				
				//Найти ячейку в массиве содержащую объект на который кликнули
				if (action_type == 'sell')
				{
					for (var i:int = 0; i < shop.arr_items.length; i++)
					{
						if (shop.arr_items[i].item_name == item_name)
						{
							check = backpack.Check_array(item_name);
							if (check == null)
							{
								break;
							}
							else
							{
								//изменить source
								shop.arr_items[i].quantity -= total_quantity;
								shop.arr_items[i].Set_quantity(shop.arr_items[i].quantity)
								//изменить destination
								backpack.Change_item(item_name, total_quantity);
								trace('sell');
								break;
							}
						}
					}
				}
				if (action_type == 'buy')
				{
					for (var z:int = 0; z < backpack.arr_items.length; z++)
					{
						if (backpack.arr_items[z].item_name == item_name)
						{
							check = shop.Check_array(item_name);
							if (check == null)
							{
								break;
							}
							else
							{
								backpack.arr_items[z].quantity -= total_quantity;
								backpack.arr_items[z].Set_quantity(backpack.arr_items[z].quantity)
								shop.Change_item(item_name, total_quantity);
								trace('buy');
								break;
							}
						}
					}
				}
			}
			ScreenManager._instance.HidePopup(this);
			Clear_data();
		}
		
		private function CancelBtn_CLICK_handler(e:MouseEvent):void
		{
			ScreenManager._instance.HidePopup(this);
			Clear_data();
		}
		
		private function IncreaseQuantityClick_handler(e:MouseEvent):void
		{
			if (total_quantity < max_quantity)
			{
				total_quantity += 1;
				Update_text();
			}
		}
		
		private function DecreaseQuantityClick_handler(e:MouseEvent):void
		{
			if (total_quantity > 0)
			{
				
				total_quantity -= 1;
				Update_text();
			}
		}
	}
}
