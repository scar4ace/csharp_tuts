package
{
	import flash.display.MovieClip;
	
	public class Model
	{
		public static const _instance:Model = new Model();
		
		public function Model()
		{
			if (Model)
			{
				throw new Error('Class is singleton.');
			}
			else
			{
				
			}
		}
		
		public function Change_item(_item_name:String, _quantity:int)
		{
			var shop_item:Shop_item = Check_array(_item_name);
			
			shop_item.Set_shop_item(item_type, _item_name, _quantity);
		}
		
		public function Check_array(_arr_items:Array,_item_name:String,_item_type:String):Shop_item
		{
			var name_not_found:Boolean = true;
			var element:Shop_item;
			for (var i:int = 0; i < _arr_items.length; i++)
			{
				if (_arr_items[i].item_name == _item_name)
				{
					element = _arr_items[i];
					name_not_found = false;
					break;
				}
			}
			for (i = 0; i < _arr_items.length; i++)
			{
				if (_arr_items[i].item_name == 'empty' && name_not_found)
				{
					element = _arr_items[i];
					break;
				}
			}
			return element;
		}
	
	}

}