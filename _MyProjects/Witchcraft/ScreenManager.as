package
{
	import flash.display.MovieClip;
	import flash.events.*;
	import flash.utils.Dictionary;
	import Main;
	import Map;
	import Shop;
	import Kitchen;
		
	public class ScreenManager 
	{
		public static const _instance:ScreenManager = new ScreenManager();
		
		private var activeScreen:MovieClip;
		private var activePopup:MovieClip;
		public var popups_arr:Object=new Object;
		
		
		public function ScreenManager() 
		{
			if (ScreenManager)
			{
				throw new Error('Class is singleton.');
			}else {
			}
		}
		
		public function ShowScreen(_obj:MovieClip):void
		{
			HideScreen();
			activeScreen = _obj;
			activeScreen.visible = true;
			//trace(_obj.name);
		}
		private function HideScreen():void
		{
			if (activeScreen)
			{
				activeScreen.visible = false;
			}
		}
		public function ShowPopup(_obj:MovieClip)
		{
			activePopup = _obj;
			activePopup.visible = true;
		}
		public function HidePopup(_obj:MovieClip):void
		{
			if (_obj)
			{
				_obj.visible = false;
			}
			activePopup = null;
		}
		public function Create_popups()
		{
			popups_arr['popup_deal'] = new Popup_deal();
			popups_arr['popup_add_to_backpack'] = new Popup_add_to_backpack();
			popups_arr['popup_threat'] = new Popup_treat();
		}
	}

}