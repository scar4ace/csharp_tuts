﻿package
{
	
	import flash.events.Event;
	import flash.utils.Dictionary;
	import ScreenManager;
	import SoundManager;
	import flash.display.MovieClip;
	import flash.display.Stage;
	import flash.events.MouseEvent;
	import Map_item;
	import Popup_enterName;
	import GameVars;
	import MenuBottom;
	import Event_channel;
	
	public class Main extends MovieClip
	{
		
		public static var _mainLink:Main;
		public static var _stageLink:Stage;
		
		private var screenManager:ScreenManager;
		private var soundManager:SoundManager;
		private var gameVars:GameVars;
		
		public var menuBottom:MenuBottom;
		public var popup_EnterName:Popup_enterName;
		
		public function Main()
		{
			stage.scaleMode = 'noScale';
			
			_mainLink = this;
			//_stageLink = stage;
			
			screenManager = ScreenManager._instance;
			soundManager = SoundManager._instance;
			gameVars = GameVars._instance;
			
			menuBottom = new MenuBottom(120, 475);
			popup_EnterName = new Popup_enterName();
		
		}
		
		public function InitGame():void
		{
			screenManager.ShowScreen(map);
			Set_map_items();
			Set_shops();
			screenManager.Create_popups();
		}
		
		//ДЕБАЖНЫЕ ФУНКЦИИ
		public function Set_shops()
		{
			//дефолтные наборы магазинов
			map.arrShops['Player'].elements.Change_item('Goose grass', 7);
			map.arrShops['Player'].elements.Change_item('Chuck Norris tears', 3);
			map.arrShops['Player'].poisons.Change_item('Green poison', 3);
			map.arrShops['Player'].antidotes.Change_item('Red antidote', 1);
			map.arrShops['Player'].antidotes.Change_item('Green antidote', 1);
			map.arrShops['Player'].antidotes.Change_item('Blue antidote', 1);
			map.arrShops['Karkonta'].elements.Change_item('Dragon nipple', 3);
		}
		
		private function Set_map_items()
		{
			//Set Map Items
			map.item_0.name_txt.text = GameVars._instance.playerName;
			map.item_0.face_mc.gotoAndStop('face_0');
			map.item_0.AssignShop(map.item_0.name_txt.text);
			
			map.item_1.name_txt.text = 'Karkonta';
			map.item_1.face_mc.gotoAndStop('face_1');
			map.item_1.AssignShop(map.item_1.name_txt.text);
			
			map.item_2.name_txt.text = 'Ded Mazai';
			map.item_2.face_mc.gotoAndStop('face_2');
			map.item_2.AssignShop(map.item_2.name_txt.text);
			
			map.item_3.name_txt.text = 'Baba Yaga';
			map.item_3.face_mc.gotoAndStop('face_3');
			map.item_3.AssignShop(map.item_3.name_txt.text);
			
			map.item_4.name_txt.text = 'Ivan Ivanych';
			map.item_4.face_mc.gotoAndStop('face_4');
			map.item_4.AssignShop(map.item_4.name_txt.text);
		}
	}
}
