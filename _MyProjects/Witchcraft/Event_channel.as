package 
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	public class Event_channel extends EventDispatcher 
	{
		
		public static const _instance:Event_channel = new Event_channel();
		
		
		//События
		public static const PLAYER_BUYER:String = "player_buyer"; //Игрок покупатель (активен рюкзак игрока)
		public static const PLAYER_SELLER:String = "player_seller"; //Игрок дома
		public static const PRICE_CHANGED:String = "price_changed"; //изменение цены айтема (для median price)
		public static const END_DAY:String = "end_day";//конец дня
		public static const PROPOSE:String = "propose";//предложение врагу через попап
		public static const POISONED:String = 'poisoned';//отравлен
		public static const HEALED:String = 'healed';//вылечен
		public static const ROBBED:String = 'robbed';//ограблен
		public static const KILLED_A_ROBBER:String = 'killed_a_robber';//убил грабителя
		public static const KILLED:String = 'killed';//убит
		public static const ENEMY_ACTION:String = 'enemy_action';//действия врага (вызывать рандомно по таймеру)

		
		public function Event_channel() 
		{
			if (Event_channel)
			{
				throw new Error('Class is singleton!');
			}else {
				
			}
		}
		
		
		
		
		public function Dispatch_Event(_event:String):void
		{
			
			//trace('Event "'+_event+'" was sent');
			dispatchEvent(new Event(_event));
		}
		
		
		
	}
}