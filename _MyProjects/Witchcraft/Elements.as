﻿package
{
	
	import flash.display.MovieClip;
	import flash.utils.Dictionary;
	
	public class Elements extends MovieClip
	{
		
		public var arr_items:Array = new Array();
		public var item_type:String = 'element';
				
		public function Elements()
		{
		
		}
		
		//переместить в Model
		public function Change_item(_item_name:String, _quantity:int,_new_price:int=0)
		{
			var shop_item:Shop_item = Check_array(_item_name);
			
			shop_item.Set_shop_item(item_type, _item_name, _quantity,_new_price);
		}
		
		public function Check_array(_item_name:String):Shop_item
		{
			var name_not_found:Boolean = true;
			var element:Shop_item;
			for (var i:int = 0; i < arr_items.length; i++)
			{
				if (arr_items[i].item_name == _item_name)
				{
					element = arr_items[i];
					name_not_found = false;
					break;
				}
			}
			for (i = 0; i < arr_items.length; i++)
			{
				if (arr_items[i].item_name == 'empty' && name_not_found)
				{
					element = arr_items[i];
					break;
				}
			}
			return element;
		}
	}

}
