package
{
	import flash.display.MovieClip;
	import flash.events.*;
	import flash.geom.Vector3D;
	import flash.utils.Dictionary;
	import Event_channel;
	
	public class GameVars
	{
		public static const _instance:GameVars = new GameVars();
		
		public var playerName:String = '';
		//public var price_items:Dictionary = new Dictionary();
		public var arrBackpack:Object = new Object();
		
		public var arr_median_price:Object = new Object();
		private var arr_median_temp:Object = new Object();
		
		//для перебора массивов
		public var arr_item_names:Array = ['Goose grass', 'Chuck Norris tears', 'Mammoth shit', 'Dragon nipple', 'Chicken ass', 'Frog vomit', 'Cockroach penis', 'Fresh from mosquitoes', 'Green poison', 'Red poison', 'Blue poison', 'Yellow poison', 'Green antidote', 'Red antidote', 'Blue antidote', 'Yellow antidote'];
		//ссылки
		public var active_shop:Shop;
		public var active_backpack:Backpack;
		
		public var default_money = 10000;
		
		//триггеры
		public var is_player_seller:Boolean = false;
		public var is_player_buyer:Boolean = false;
		public var is_the_enemy_is_thinking:Boolean = false;
		public var is_next_free:Boolean = true;
		
		public function GameVars()
		{
			if (GameVars)
			{
				throw new Error('Class is singleton.');
			}
			else
			{
				Event_channel._instance.addEventListener(Event_channel.PLAYER_BUYER, PLAYER_BUYER_handler, false, 0, true);
				Event_channel._instance.addEventListener(Event_channel.PLAYER_SELLER, PLAYER_SELLER_handler, false, 0, true);
				//Event_channel._instance.addEventListener(Event_channel.PRICE_CHANGED, PRICE_CHANGED_handler, false, 0, true);
				
				//Средняя цена айтемов (изменяется в течении игры)
				arr_median_price['empty'] = 0;
				arr_median_price['Goose grass'] = 10;
				arr_median_price['Chuck Norris tears'] = 20;
				arr_median_price['Mammoth shit'] = 30;
				arr_median_price['Dragon nipple'] = 40;
				arr_median_price['Chicken ass'] = 50;
				arr_median_price['Frog vomit'] = 60;
				arr_median_price['Cockroach penis'] = 70;
				arr_median_price['Fresh from mosquitoes'] = 80;
				arr_median_price['Green poison'] = 100;
				arr_median_price['Red poison'] = 200;
				arr_median_price['Blue poison'] = 300;
				arr_median_price['Yellow poison'] = 400;
				arr_median_price['Green antidote'] = 500;
				arr_median_price['Red antidote'] = 600;
				arr_median_price['Blue antidote'] = 700;
				arr_median_price['Yellow antidote'] = 800;
				
			}
		}
		
		//изменение средней цены на айтем
		//НЕПРАВИЛЬНО!
		public function Set_median_price(_item_name:String,_new_price:int):void
		{
			if (_item_name != 'empty')
			{
				if (_new_price != 0)
				{
					arr_median_price[_item_name] = (arr_median_price[_item_name] * 4 + _new_price) / 5;
					trace('median price of '+_item_name+' was changed = '+arr_median_price[_item_name]);
				}
			}
		}
		public function Get_median_price(_item_name:String):int
		{
			return arr_median_price[_item_name];
		}
		
		//перенести в model
		//public function PRICE_CHANGED_handler(e:Event):void
		//{
			//
		//}
		
		private function PLAYER_BUYER_handler(e:Event):void
		{
			is_player_buyer = true;
		}
		
		private function PLAYER_SELLER_handler(e:Event):void
		{
			is_player_seller = true;
		}
	
	}

}