﻿package
{
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import ScreenManager;
	
	public class Popup_deal extends MovieClip
	{
		
		public var median_price:int = 0;
		private var actual_price:int = 0;
		
		private var max_quantity:int = 0;
		private var total_quantity:int = 0;
		private var total_price:int = 0;
		private var item_type:String = '';
		private var item_name:String = '';
		private var action_type:String = '';
		private var check:Shop_item;
		
		
		
		
		
		public function Popup_deal()
		{
			visible = false;
			//события кнопок
			price_mc.increaseBtn.addEventListener(MouseEvent.CLICK, IncreasePriceClick_handler, false, 0, true);
			price_mc.decreaseBtn.addEventListener(MouseEvent.CLICK, DecreasePriceClick_handler, false, 0, true);
			quantity_mc.increaseBtn.addEventListener(MouseEvent.CLICK, IncreaseQuantityClick_handler, false, 0, true);
			quantity_mc.decreaseBtn.addEventListener(MouseEvent.CLICK, DecreaseQuantityClick_handler, false, 0, true);
			dealBtn.addEventListener(MouseEvent.CLICK, DealBtnClick_handler, false, 0, true);
			proposeBtn.addEventListener(MouseEvent.CLICK, ProposeBtnClick_handler, false, 0, true);
			cancelBtn.addEventListener(MouseEvent.CLICK, CancelBtnClick_handler, false, 0, true);
			addEventListener(Event.ENTER_FRAME, Update, false, 0, true);
			//Error text
			errorText.text = 'No free slots!';
			errorText.visible = false;
			
			//quantity_mc.txt.text = '0';
			
			AddToStage();
		}
		
		private function AddToStage()
		{
			Main._mainLink.addChild(this);
		}
		public function Set_popup(_name:String, _type:String, _max_quantity:int, _price:int, _action_type:String)
		{
			action_type = _action_type;
			max_quantity = _max_quantity;
			item_name = _name;
			itemName.text = 'Item: ' + _name;
			item_type = _type;
			//при создании новой цены апдейтить цену в гейм варсах
			median_price = GameVars._instance.Get_median_price(item_name);
			actual_price = median_price;
			//Update_text();
			//trace(item_type);
		}
		
		//Обработчики событий
		private function IncreasePriceClick_handler(e:MouseEvent):void
		{
			if (actual_price < (5 * median_price))
			{
				actual_price += 1;
			}
		}
		
		private function DecreasePriceClick_handler(e:MouseEvent):void
		{
			if (actual_price > 0)
			{
				actual_price -= 1;
			}
		}
		
		private function IncreaseQuantityClick_handler(e:MouseEvent):void
		{
			if (total_quantity < max_quantity)
			{
				total_quantity += 1;
			}
		}
		
		private function DecreaseQuantityClick_handler(e:MouseEvent):void
		{
			if (total_quantity > 0)
			{
				total_quantity -= 1;
			}
		}
		
/*		public function Opponent_click_deal()
		{
			
		}
*/		private function DealBtnClick_handler(e:MouseEvent):void
		{
			var backpack:MovieClip;
			var shop:MovieClip;
			if (total_quantity > 0)
			{
				switch (item_type)
				{
				case 'element': 
					backpack = GameVars._instance.active_backpack.elements;
					shop = GameVars._instance.active_shop.elements;
					break;
				case 'poison': 
					backpack = GameVars._instance.active_backpack.poisons;
					shop = GameVars._instance.active_shop.poisons;
					break;
				case 'antidote': 
					backpack = GameVars._instance.active_backpack.antidotes;
					shop = GameVars._instance.active_shop.antidotes;
					break;
				default: 
					trace('default case action');
				}
				
				//обновить median price
				GameVars._instance.Set_median_price(item_name, actual_price);
				trace(actual_price);
				
				
				
				//Найти ячейку в массиве содержащую объект на который кликнули
				if (action_type == 'sell')
				{
					for (var i:int = 0; i < shop.arr_items.length; i++)
					{
						if (shop.arr_items[i].item_name == item_name)
						{
							check = backpack.Check_array(item_name);
							if (check == null)
							{
								break;
							}
							else
							{
								//изменить source
								shop.arr_items[i].quantity -= total_quantity;
								shop.arr_items[i].Set_quantity(shop.arr_items[i].quantity)
								//shop.Change_item(item_name, total_quantity,median_price);
								//изменить destination
								backpack.Change_item(item_name, total_quantity,median_price);
								trace('sell');
								break;
							}
						}
					}
				}
				if (action_type == 'buy')
				{
					for (var z:int = 0; z < backpack.arr_items.length; z++)
					{
						if (backpack.arr_items[z].item_name == item_name)
						{
							check = shop.Check_array(item_name);
							if (check == null)
							{
								break;
							}
							else
							{
								backpack.arr_items[z].quantity -= total_quantity;
								backpack.arr_items[z].Set_quantity(backpack.arr_items[z].quantity)
								//backpack.Change_item(item_name, total_quantity,median_price);
								shop.Change_item(item_name, total_quantity,median_price);
								trace('buy');
								break;
							}
						}
					}
				}
				
			}
			
			ScreenManager._instance.HidePopup(this);
			//Event_channel._instance.Dispatch_Event(Event_channel.PRICE_CHANGED);
			trace(item_type);
			Clear_data();
		}
		
		private function ProposeBtnClick_handler(e:MouseEvent):void
		{
			if (total_price > 0 && !GameVars._instance.is_the_enemy_is_thinking)
			{
				Event_channel._instance.Dispatch_Event(Event_channel.PROPOSE);
				/*Сделать в этом классе слушатель и лочить кнопки до диспатча события ответа от врага
				   + если попап от игрока, то лочить кнопку deal на старте и разлочивать после ответного предложения
				   или вообще закрывать попап если противник согласился купить или продать товар*/
				GameVars._instance.is_the_enemy_is_thinking = true;
			}
		}
		
		private function CancelBtnClick_handler(e:MouseEvent):void
		{
			ScreenManager._instance.HidePopup(this);
			Clear_data();
		}
		
		private function Clear_data()
		{
			max_quantity = 0;
			total_quantity = 0;
		}
		private function Update(e:Event):void
		{
			total_price = actual_price * total_quantity;
			median_txt.text = median_price.toString();
			price_mc.txt.text = actual_price.toString();
			quantity_mc.txt.text = total_quantity.toString();
			total_txt.text = total_price.toString();
		}
	}

}
