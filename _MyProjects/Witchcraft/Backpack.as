﻿package
{
	
	import flash.display.MovieClip;
	
	public class Backpack extends MovieClip
	{
		
		public var nameOwner:String;
		public var arr_items:Object = new Object();
		
		
		public function Backpack(_coord:MovieClip, _shop:Shop)
		{
			nameOwner = _shop.nameOwner;
			x = _coord.x;
			y = _coord.y;
			Add_to_array(_shop);
			_shop.addChild(this);
		}
		private function Add_to_array(_shop:Shop)
		{
			if (_shop.isEnemyOwner)
			{
				GameVars._instance.arrBackpack[_shop.nameOwner] = this;
			}else
			{
				GameVars._instance.arrBackpack['Player'] = this;
			}
		}
		public function Update_backpack(_items_array)
		{
			//загрузить массив текущего покупателя
		}
		public function Clear_backpack()
		{
			//обнулить массив после возвращения игрока домой
		}
	}

}
