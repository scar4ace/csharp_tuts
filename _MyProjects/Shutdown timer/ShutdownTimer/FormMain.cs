﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace ShutdownTimer
{
	public partial class FormMain : Form
	{
		private string appPath = @"C:\Windows\System32\shutdown.exe";
		public FormMain()
		{
			InitializeComponent();
		}
		private string CountTotalTimeInSeconds()
		{
			decimal totalTimeInSeconds;
			totalTimeInSeconds = (numericUpDownHours.Value * 3600) + (numericUpDownMinutes.Value*60);
			if (totalTimeInSeconds>0)
			{
				return totalTimeInSeconds.ToString();
			}else{
				return "";
			}
		}
		private void buttonStart_Click(object sender, EventArgs e)
		{
			MakeProcess("reset");
			MakeProcess("start");
		}
		private void buttonReset_Click(object sender, EventArgs e)
		{
			MakeProcess("reset");
		}
		private void MakeProcess(string _procCmd)
		{
			Process proc = new Process();
			proc.StartInfo.FileName = appPath;
			switch (_procCmd)
			{
				case "start":
					if (CountTotalTimeInSeconds() !="")
					{
						proc.StartInfo.Arguments = string.Format("-s -t {0}", CountTotalTimeInSeconds());
					}
					break;
				case "reset":
					proc.StartInfo.Arguments = "-a";
					break;
				default:
					break;
			}
			proc.Start();
			proc.Close();
		}
	}
}
