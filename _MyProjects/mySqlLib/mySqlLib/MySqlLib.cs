﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mySqlLib
{
    public class MySqlLib
    {
        // Возвращает 1 значение типа object
        public string SqlScalar(string _query, string _connectionString)
        {
            //object result;
            SqlConnection sqlConnection = new SqlConnection(_connectionString);
            SqlCommand sqlCommand = sqlConnection.CreateCommand();
            sqlCommand.CommandText = _query;
            sqlConnection.Open();

            // временный вывод данных
            string result = sqlCommand.ExecuteScalar().ToString();

            sqlConnection.Close();
            return result;
        }
        // Возвращает таблицу данных
        public DataTable SqlReturnDataset(string _query, string _connectionString, short tableIndex = 0)
        {
            //result;
            SqlConnection sqlConnection = new SqlConnection(_connectionString);
            SqlCommand sqlCommand = sqlConnection.CreateCommand();
            sqlCommand.CommandText = _query;
            sqlConnection.Open();

            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = sqlCommand;
            DataSet dataSet = new DataSet();
            adapter.Fill(dataSet);

            DataTable result = dataSet.Tables[tableIndex];

            sqlConnection.Close();
            return result;
        }
        // Не возвращает ничего, кроме "true" - успешно, "false" - неуспешно
        public void /*bool*/ SqlNoneQuery(string _query, string _connectionString)
        {
            SqlConnection sqlConnection = new SqlConnection(_connectionString);
            SqlCommand sqlCommand = sqlConnection.CreateCommand();
            sqlCommand.CommandText = _query;
            sqlConnection.Open();
            sqlCommand.ExecuteNonQuery();
            sqlConnection.Close();

            //return false;
        }
    }
}
